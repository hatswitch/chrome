import sys
import subprocess
import time

gclient_entries_file = sys.argv[1]


s = open(gclient_entries_file).read()

assert s.startswith('entries = ')

s = s[len('entries = '):]

entries = eval(s)

for dirpath, revision_url in entries.items():
    print( '')
    print( '==================================================')
    print( 'dirpath= {}'.format(dirpath))
    print( 'revision_url= {}'.format(revision_url))

    revision_parts = revision_url.split('@')

    if len(revision_parts) > 1:
        commit_hash = revision_parts[-1]
        assert len(commit_hash) == 40

        print('commit hash= {}'.format(commit_hash))
        print('commit details below:\n')

        sys.stdout.flush()
        child = subprocess.Popen('git show -q {}'.format(commit_hash),
                                 cwd=dirpath, shell=True)
        time.sleep(1)
        child.terminate()
        child.kill()
        pass
    pass

#!/usr/bin/python3

import pdb
import re
import sys
import getopt
import shutil
import os
import math
import argparse
import pickle as pickle
import bz2


import common

import logging

def convert_to_ms(s):
    parts = list(map(int, s.split(':')))
    assert (len(parts) == 3), 'must be in hr:min:sec format'
    second = (parts[0]*3600) + (parts[1]*60) + (parts[2]) # in second
    return second * 1000

def convert_ms_to_str(ms):
    hour = int(ms) / (3600 * 1000)
    ms = ms % (3600 * 1000)
    minute = int(ms) / (60 * 1000)
    ms = ms % (60 * 1000)
    second = int(ms) / (1000)
    ms = ms % (1000)
    return '{hour:02d}:{minute:02d}:{second:02d}:{ms:03d}'.format(
        hour=hour, minute=minute, second=second, ms=ms)

# http://stackoverflow.com/questions/1094841/reusable-library-to-get-human-readable-version-of-file-size
def sizeof_fmt(num):
    for x in ['bytes','KB','MB','GB','TB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        pass
        num /= 1024.0
        pass
    return

def genCDF(sortedList, toFilepath=None,
           bucketsize=0,
           commentLines=[],
           ):
    retval = None
    length = len(sortedList)
    if toFilepath:
        fil = open(toFilepath, 'w')
        pass
    else:
        retval = []
        pass
    ###

    for line in commentLines:
        fil.write('# ' + line + '\n')
        pass

    if bucketsize > 0:
        # this should be able to handle negative values as well.  (-0
        # == 0) is True

        # each bucket is represented by its upper bound.  for example,
        # if bucketsize is 10, then buckets are (-10, 0], (0, 10],
        # (10, 20], ... and the representatives of the groups are 0,
        # 10, and 20 respectively.

        # bucketUpperBound -> count
        bucketCountDict = {}

        # NOTE: we want the smallest value to get its own bucket if
        # applicable
        minvalue = min(sortedList)
        bucketCountDict[minvalue] = 0

        # this loop is not exploiting the fact that the sortedList
        # list is already sorted, but this is simpler.
        for val in sortedList:
            if val == minvalue:
                bucketCountDict[minvalue] += 1
                continue
            # which bucket should this go into?
            bucketUpperBound = math.ceil((float(val)) / bucketsize) * bucketsize
            if bucketUpperBound in bucketCountDict:
                # bucket exists already -> increment its count
                bucketCountDict[bucketUpperBound] += 1
                pass
            else:
                # create the bucket
                bucketCountDict[bucketUpperBound] = 1
                pass
            pass

#        print '***** buckets and their counts: ', bucketCountDict

        bucketUpperBounds = list(bucketCountDict.keys())
        bucketUpperBounds.sort()

        cumulativeCount = 0
        for bucketUpperBound in bucketUpperBounds:
            cumulativeCount += bucketCountDict[bucketUpperBound]
            fraction = float(cumulativeCount) / len(sortedList)
            if toFilepath:
                fil.write('%f    %f\n' % (bucketUpperBound, fraction))
                pass
            else:
                retval.append((bucketUpperBound, fraction))
                pass
            pass
        pass
    else:
        for idx, val in enumerate(sortedList):
            if toFilepath:
                fil.write('%f    %f\n' % (val, (idx + 1.0) / length))
                pass
            else:
                retval.append((val, (idx + 1.0) / length))
                pass
            pass
        pass
    ###

    if toFilepath:
        fil.close()
        pass
    return retval

#############################################

def updateTs(baseTime, ts, line):
    # baseTime is float, ts is string
    assert ts >= baseTime
    offset = ts - baseTime
    offsetStr = '{0:011.2f}'.format(offset)

    return re.sub(r'ts=',
                  r'',
                  line)

def shiftTimestamps(fileobj, outputfileobj):
    baseTime = None

    import common

    lines = fileobj.readlines()

    for line in lines:
        logging.debug('line: [%s]' % (line))

        matchWebkit = re.match(common.webkitLogLinePrefixPattern, line)
        matchChrome = re.match(common.chromeLogLinePrefixPattern, line)

        match = matchWebkit or matchChrome

        if match:
            for tsStr in re.findall('ts=(?P<ts>\d+\.\d{1,2})', line):
                if baseTime is None:
                    baseTime = float(tsStr)
                    assert baseTime > 0
                    offset = 0
                    pass
                else:
                    offset = float(tsStr) - baseTime
                    pass
                offsetStr = '{0:10.2f}'.format(offset)
                line = line.replace('ts={}'.format(tsStr),
                                    'ts={}'.format(offsetStr))
                pass
            pass
        # line = line.rstrip('\n')
        outputfileobj.write(line) # the line already contains ending newline
        pass

    return

#############################################

def shift_timestamps(logfile, outputfileobj):
    openfunc = bz2.open if logfile.endswith('.bz2') else open
    with openfunc(logfile, 'rt') as inputfp:
        shiftTimestamps(inputfp, outputfileobj)
        pass

    return


#############################################
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description='''
parse browser log file (presumably run with --extract-page-model) and extract the model of the webpage
        '''
        )
    parser.add_argument(
        "--log-level", dest='log_level', default='INFO',
        help="set log level")

    subparsers = parser.add_subparsers(help='sub-command help',
                                       dest='subparser_name')

    shift_timestamps_parser = subparsers.add_parser(
        'shift-timestamps', help='help')
    shift_timestamps_parser.add_argument(
        "infile",
        help="path to input log file (will detect .bz2 extension)")
    shift_timestamps_parser.add_argument(
        "outfile", nargs='?', default=sys.stdout,
        help="path to output log file (default is stdout)")

    extract_model_parser = subparsers.add_parser(
        'extract-model', help='help')
    extract_model_parser.add_argument(
        "--show-timing", action='store_true',
        help="show timing info when drawing")
    extract_model_parser.add_argument(
        "--show-elem-end-byte-offset", action='store_true',
        help="show elements' end byte offsets")
    extract_model_parser.add_argument(
        "--hide-edge-labels", action='store_true',
        help="do not show edge labels in graph")

    extract_model_parser.add_argument(
        "--group-peer-elems", action='store', dest='group_peer_elems',
        nargs='?', const='default', choices=['default', 'show-elem-instNums'],
        help="(for visualization purpose) group uninteresting 'peer' (e.g., created by same scope, belonging to same html, etc.) image and link elements into a single entity. uninteresting means the element is just fetched but nothing special is performed when finished")

    extract_model_parser.add_argument(
        "--group-peer-fetches", action='store', dest='group_peer_fetches',
        nargs='?', const='default', choices=['default', 'show-res-instNums'],
        help="(for visualization purpose) group uninteresting 'peer' (e.g., created by same scope) fetch requests into a single entity")

    extract_model_parser.add_argument(
        "infile",
        help="path to browser log file (will detect .bz2 extension and handle accordingly)")
    extract_model_parser.add_argument(
        "outfile", nargs='?', default='page_model.json',
        help="path to output model")


    change_hostnames_parser = subparsers.add_parser(
        'change-hostnames', help='change hostnames in the existing model to new hostnames')
    change_hostnames_parser.add_argument(
        "infile",
        help="path to page model json file that was generated by extract-model")
    change_hostnames_parser.add_argument(
        "outfile",
        help="path to output the updated json file")
    change_hostnames_parser.add_argument(
        "hostname", nargs='+',
        help="a new available hostname")

    args = parser.parse_args()

    # assuming loglevel is bound to the string value obtained from the
    # command line argument. Convert to upper case to allow the user to
    # specify --log=DEBUG or --log=debug
    numeric_level = getattr(logging, args.log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % args.log_level)
    logging.basicConfig(
        format="%(levelname) -10s %(module)s:%(lineno)s, %(funcName)s(): %(message)s",
        level=numeric_level,
    )

    if args.subparser_name == 'shift-timestamps':
        outputfileobj = args.outfile
        if outputfileobj is not sys.stdout:
            try:
                outputfileobj = open(outputfileobj, 'w')
                pass
            except Exception as e:
                print('cannot open output file for writing. error "{error}"'.format(
                    error=str(e)))
                sys.exit(1)
                pass
            pass
        shift_timestamps(args.infile, outputfileobj)
        pass

    elif args.subparser_name == 'extract-model':
        import extract_model
        extract_model.extract_model(args.infile,
                                    outfile=args.outfile,
                                    hide_edge_labels=args.hide_edge_labels,
                                    show_timing=args.show_timing,
                                    group_peer_elems=args.group_peer_elems,
                                    group_peer_fetches=args.group_peer_fetches,
                                    show_elem_end_byte_offset=args.show_elem_end_byte_offset)
        pass

    elif args.subparser_name == 'change-hostnames':
        import change_hostnames
        change_hostnames.change_hostnames(
            infile=args.infile, outfile=args.outfile, newhostnames=args.hostname)
        pass

    pass

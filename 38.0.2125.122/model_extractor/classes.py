from abc import ABC, abstractmethod
from collections import defaultdict
import io
import pdb
import re
import logging

import common
from dot_drawing import *

class DependencyEntity(ABC):
    @abstractmethod
    def __init__(self):
        self._in_edges = set()
        self._out_edges = set()

        # we don't know how to handle some parts of the model. for
        # example, sometimes, the main window handles a 'message'
        # event (scripts can fire events on event targets) that we
        # can't link to anything/script. and then from that event
        # handling, it creates a bunch of stuff, e.g., timers, xhrs,
        # etc. so basically that whole branch of the tree is not
        # handleable to us. so, for now, we exclude i.e., prune that
        # branch of the tree from the model. thus any scope/entity
        # created in this branch will be excluded from the model
        self._to_be_pruned = False
        pass

    def to_be_pruned(self):
        return self._to_be_pruned

    def set_to_be_pruned(self, value):
        if self._to_be_pruned:
            # cannot set from true to false
            assert value == self._to_be_pruned
            pass
        self._to_be_pruned = value
        pass

    def add_in_edge(self, edge):
        assert isinstance(edge, DependencyEdge) and edge.dst() is self
        assert edge not in self._in_edges
        self._in_edges.add(edge)
        pass

    def remove_in_edge(self, edge):
        # remove will raise key error as appropriate
        self._in_edges.remove(edge)
        pass

    def add_out_edge(self, edge):
        assert isinstance(edge, DependencyEdge) and edge.src() is self
        assert edge not in self._out_edges
        self._out_edges.add(edge)
        pass

    def remove_out_edge(self, edge):
        # remove will raise key error as appropriate
        self._out_edges.remove(edge)
        pass

    def in_edges(self, includeInnerScopes=False):
        # "includeInnerScopes" = true will get all edges of this scope
        # as well as its inner scopes
        edges = list(self._in_edges)
        if includeInnerScopes:
            for scope in self._inner_scopes:
                edges.extend(scope.in_edges(True))
                pass
            pass
        return edges

    def out_edges(self, includeInnerScopes=False):
        # "includeInnerScopes" = true will get all edges of this scope
        # as well as its inner scopes
        edges = list(self._out_edges)
        if includeInnerScopes:
            for scope in self._inner_scopes:
                edges.extend(scope.out_edges(True))
                pass
            pass
        return edges

    def drop_all_in_edges(self):
        edges = list(self._in_edges)
        for edge in edges:
            edge.drop()
            pass
        pass
        
    pass

# will be used to detect duplicates once the log processor is finished
g_all_edges = set()

class DependencyEdge(DotDrawingEdge):
    @abstractmethod    
    def __init__(self, src, dst):
        super().__init__()

        # "src" is optional because in our design, the edges will be
        # maintained by each node, so the src is implied. "dst" is
        # optional because of we won't have the actual scheduled
        # scopes until it runs, e.g., after timer expires

        self._src = None
        self._dst = None

        if src:
            self.set_src(src)
            pass

        if dst:
            self.set_dst(dst)
            pass

        assert self not in g_all_edges
        g_all_edges.add(self)

        pass

    def drop(self):
        src = self.src()
        if src:
            src.remove_out_edge(self)
            pass

        dst = self.dst()
        if dst:
            dst.remove_in_edge(self)
            pass

        g_all_edges.remove(self)

        pass

    def src(self):
        return self._src

    def dst(self):
        return self._dst

    def set_src(self, src):
        assert self._src is None and isinstance(src, DependencyEntity)
        if isinstance(src, ExecutionScope):
            assert isinstance(self, SchedulingEdgeWithTs), \
              'edge src is a scope ({scope}); should be used with a SchedulingEdgeWithTs'.format(scope=src)
            pass
        self._src = src
        self._src.add_out_edge(self)
        pass

    def adopt_new_src(self, newsrc):
        # use this when src is already currently set
        assert self._src and isinstance(newsrc, DependencyEntity)
        self._src.remove_out_edge(self)
        self._src = newsrc
        self._src.add_out_edge(self)
        pass

    def set_dst(self, dst):
        assert self._dst is None
        assert isinstance(dst, DependencyEntity), 'dst is of type {}'.format(type(dst))
        self._dst = dst
        self._dst.add_in_edge(self)
        pass

    def __str__(self):
        return '"{src}" -> "{dst}"'.format(src=self.src(), dst=self.dst())

    pass

class ReferencingEdge(DependencyEdge):
    # "A refers to B" relationship
    #
    # e.g., an element refers to a resource. we could have multiple
    # elements in the same document referring to the same resource

    def __init__(self, src, dst):
        super().__init__(src, dst)
        assert isinstance(self.src(), EventTarget) and isinstance(self.dst(), Resource)
        pass

    def draw_get_label(self):
        return 'refs'

    def draw_get_style(self):
        return 'setlinewidth(2)'

    pass

class ContainingEdge(DependencyEdge):
    # "A contains B" relationship
    #
    # e.g., in webkit sense, Document is a ContainterNode, that
    # contains nodes of types Elements

    def __init__(self, src, dst):
        super().__init__(src, dst)
        assert isinstance(self.src(), Document) and isinstance(self.dst(), Element)
        pass

    def draw_get_label(self):
        return 'has'

    def draw_get_style(self):
        return 'dashed'

    pass

class SchedulingEdge(DependencyEdge):
    # "A schedules B" relationship
    #
    # for things like: a timer is scheduled, a render tree update is
    # scheduled, a microtask is enqueued, etc.

    # @abstractmethod
    def __init__(self, src, dst):
        super().__init__(src, dst)
        pass

    def draw_get_label(self):
        return 'sched'

    def draw_get_style(self):
        return 'dotted, setlinewidth(2)'

    pass

class SchedulingEdgeWithTs(SchedulingEdge):
    def __init__(self, *args, ts, **kwargs):
        super().__init__(*args, **kwargs)

        self._ts = ts
        pass

    def ts(self):
        return self._ts

    def _draw_get_label_action(self):
        return 'sched'

    def draw_get_label(self):
        action = self._draw_get_label_action()
        if isinstance(self.src(), ExecutionScope):
            time_offset = ''
            if DotDrawing.show_timing:
                time_offset = '@{offset:.1f} '.format(offset=self.ts() - self.src().open_ts())
                pass
            return '{time_offset}{action}'.format(time_offset=time_offset, action=action)
        else:
            return action
        pass

    pass

# many of the scheduling edge subclasses are just for convenience so
# we can use type(edge) to know what it is instead of looking at the
# destination. also, for drawing purposes, it's nice to have the edge
# be able to use custom properties

class FetchSchedulingEdge(SchedulingEdgeWithTs):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert isinstance(self.dst(), FetchRequest)
        # the src can be several different things, like Resource, or
        # FetchFinishScope
        pass

    def __str__(self):
        return 'fetch res:{}'.format(self.dst().resInstNum())

    pass

class FetchMicrotaskSchedulingEdge(SchedulingEdgeWithTs):
    # schedules a (dom)microtask that will start the fetch. currently
    # we only see this for images
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert isinstance(self.dst(), FetchRequest)
        assert isinstance(self.src(), ImageElement)
        pass

    def __str__(self):
        return 'fetch res:{}'.format(self.dst().resInstNum())

    def draw_get_label(self):
        return 'sched(utask)'

    pass

class FetchFinishEdge(SchedulingEdge):
    def __init__(self, src, dst):
        super().__init__(src, dst)
        assert isinstance(self.src(), FetchRequest) and isinstance(self.dst(), FetchFinishScope)
        pass

    def draw_get_label(self):
        return ''

    pass

class CreateElementActionEdge(SchedulingEdgeWithTs):
    def __init__(self, src, dst, ts):
        assert isinstance(dst, Element)
        super().__init__(src, dst, ts=ts)
        pass

    def _draw_get_label_action(self):
        return 'creat'

    pass

# class CreateResourceEdge(SchedulingEdgeWithTs):
#     def __init__(self, src, dst, ts):
#         assert isinstance(dst, Resource)
#         super().__init__(src, dst, ts=ts)
#         pass

#     def _draw_get_label_action(self):
#         return 'creat'

#     pass

class RenderUpdateSchedulingEdge(SchedulingEdgeWithTs):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert type(self.dst()) is RenderTreeUpdateScope or self.dst() is None
        # the src can be several different things, like Resource, or
        # FetchFinishScope
        pass

    def set_dst(self, dst):
        assert type(dst) is RenderTreeUpdateScope
        return super().set_dst(dst)

    pass

class DOMEventSchedulingEdge(SchedulingEdge):
    def __init__(self, src, dst, eventName):
        assert isinstance(dst, EventTargetEventScopeBase)
        super().__init__(src, dst)
        self._eventName = eventName
        pass

    def eventName(self):
        return self._eventName

    def draw_get_label(self):
        return 'on \\"{eventName}\\"'.format(eventName=self.eventName())

    pass

# class DOMReadyStateChangeEventSchedulingEdge(DOMEventSchedulingEdge):
#     def __init__(self, *args, readyState, **kwargs):
#         super().__init__(*args, eventName='readyStateChange', **kwargs)
#         assert type(self.dst()) is DOMReadyStateChangeEventScope
#         self.readyState = readyState
#         pass

#     def draw_get_label(self):
#         return ''
#         # return 'on \\"{eventName}\\"'.format(eventName=self.eventName())

#     pass

class CreateDOMTimerEdge(SchedulingEdgeWithTs):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert type(self.dst()) == DOMTimer
        pass

    def _draw_get_label_action(self):
        return 'creat'

    pass

class ClearDOMTimerEdge(SchedulingEdgeWithTs):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert type(self.dst()) == DOMTimer
        pass

    def _draw_get_label_action(self):
        return 'del'

    pass

class SetResourceReferenceEdge(SchedulingEdgeWithTs):
    # we need this because, for example, javascript can change an
    # image element's src attribute, causing it to point to another
    # resource and potentially initiates a fetch (thus element causes
    # multiple fetches, and will fire multiple "load" events)
    #
    # the edge src should be the current important scope, the dst
    # should be an element, and the url is the complete url being set
    # to

    #
    # OR: maybe can just use the FetchSchedulingEdge and dont have to
    # worry about which element the fetch is for.
    def __init__(self, *args, completeURL, **kwargs):
        super().__init__(*args, **kwargs)
        self.completeURL = completeURL
        pass

    def _draw_get_label_action(self):
        return 'setsAttr'

    def draw_get_label(self):
        action = self._draw_get_label_action()
        if isinstance(self.src(), ExecutionScope):
            time_offset = ''
            if DotDrawing.show_timing:
                time_offset = '@{offset:.1f} '.format(offset=self.ts() - self.src().open_ts())
                pass
            return '{time_offset}{action}'.format(time_offset=time_offset, action=action)
        else:
            return action
        pass

    pass


class FetchRequest(DotDrawingNodeEntity, DependencyEntity):
    def __init__(self, resource, method, url, priority, ts, starter_scope):
        DependencyEntity.__init__(self)
        DotDrawingNodeEntity.__init__(self)

        self._resource = resource # just for reference

        self._method = method
        self._url = url
        self._priority = priority
        self._ts = ts
        self._starter_scope = starter_scope

        # the stuff that happens when this fetch request finishes
        self._finish_edge = None
        # or optionally, if nothing interesting happens, then all we
        # care about is its duration
        self._finish_scope_duration = None

        # # number of bytes, including headers and body if any
        # self._upstream_size = None
        # self._downstream_size = None
        pass

    def method(self):
        return self._method

    def url(self):
        return self._url

    def priority(self):
        return self._priority

    def ts(self):
        return self._ts

    def starter_scope(self):
        return self._starter_scope

    def resInstNum(self):
        return self._resource.instNum

    # def set_upstream_size(self, s):
    #     assert self._upstream_size is None
    #     assert s is not None
    #     self._upstream_size = s
    #     pass

    # def upstream_size(self):
    #     return self._upstream_size

    # def set_downstream_size(self, s):
    #     assert self._downstream_size is None
    #     assert s is not None
    #     self._downstream_size = s
    #     pass

    # def downstream_size(self):
    #     return self._downstream_size

    def set_finish_scope(self, scope):
        # assert self._finish_scope is None and isinstance(scope, FetchFinishScope)
        # self._finish_scope = scope
        assert self._finish_edge is None and isinstance(scope, FetchFinishScope)
        self._finish_edge = FetchFinishEdge(src=self, dst=scope)
        pass

    def maybe_simplify_finish_scope(self):
        # return True if something was done; return False if nothing
        # was done
        if not self._finish_edge:
            return False

        scope = self._finish_edge.dst()
        assert scope and (self._finish_scope_duration is None)
        assert len(self.out_edges()) == 1
        if scope.count_in_edges(True) > 1:
            return False

        if not scope.count_out_edges(True):
            self._finish_scope_duration = scope.duration()

            before = len(g_all_edges)
            self._finish_edge.drop()
            self._finish_edge = None
            after = len(g_all_edges)
            assert (after + 1) == before
            assert not self.out_edges() and not scope.count_in_edges(True) and not scope.count_out_edges(True)
            return True
        else:
            ret = scope.maybe_simplify_entirely()
            if ret:
                self._finish_edge = None
                pass
            return ret
        raise Exception('not reached')

    def toString(self, succinct=True):
        assert succinct
        return 'fetchreq for res:{}'.format(self.resInstNum())

    def __str__(self):
        return self.toString()

    # draw api

    def draw_get_shape(self):
        return 'box'

    def draw_get_node_name(self):
        return 'fetch_res_{}'.format(self._resource.instNum)

    def draw_get_label(self):
        s = 'fetch res:{resInstNum}'.format(
            resInstNum=self.resInstNum(), type=resource_type_short_names[self._resource.type])
        if self._finish_scope_duration is not None:
            # we have simplied our finish scope!
            assert self._finish_edge is None
            if DotDrawing.show_timing:
                s += '\\nfinScopeDur: {:.1f}'.format(self._finish_scope_duration)
                pass
            pass

        return s

    def draw_get_fillcolor(self):
        return resource_type_colors[self._resource.type]

    def draw_get_style(self):
        return 'rounded, filled'

    pass

class MetaFetchRequest(FetchRequest):
    # for grouping a bunch of peer entities into one, just for
    # visualization purposes, to keep graph size for clarity

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.resInstNums = set()
        self.label_mode = 'default'
        pass

    def draw_get_label(self):
        label = 'METAFETCH'
        label += '\\n{} RESs'.format(len(self.resInstNums))
        if self.label_mode == 'show-res-instNums':
            label += ': '
            numPerLine = 5 # 5 on same line with the "RESs" header
            i = 1
            for instNum in sorted(self.resInstNums):
                label += str(instNum) + ','
                if (i % numPerLine) == 0:
                    label += '\\n'
                    if numPerLine == 5:
                        numPerLine = 7 # 7 on subsequent lines
                        i = 0 # reset i
                        pass
                    pass
                i += 1
                pass
            pass
        return label

    pass


# a resource is some object/resource that has a URI and MIGHT need to
# be fetched over the network. (MIGHT because for example, some font
# resources are not fetched if they are not needed for the css style
# selected; also they might not be fetched if they are encoded as data
# URI).
#
# a resource is NOT the same as a network request, e.g., it might take
# multiple network requests to obtain a resource (due to redirects,
# cors validation for xmlhttprequests, etc.)
#
# also, multiple elements (e.g., <img>) might refer to the same
# resource, i.e., the same URI. if those elements are part of the same
# html (either static or dynamic), then for our purposes, i.e.,
# generating of network requests, we only need to know about the
# earliest element.  however, if one is from static html and the other
# is dynamic (e.g., element created by script or parsed from html
# inserted by script), then we need to know about both. for example,
# consider the following example:
#
# html:
#
#   .
#   .
#   <script src="foo.js"></script>
#   .
#   .
#   <img src="bar.jpg">
#   .
#   .
#
# where "foo.js" inserts an image also referencing "bar.jpg" (either
# by inserting html (e.g., doc.write()) or by using DOM API), then
# there will be a race where either can be the first to cause the
# network request.
#
# for example, in a likely scenario, "foo.js" will take some time to
# be fetched; during that time, parser is blocked, waiting. BUT the
# preload scanner will see the subsequent "bar.jpg" and will preload
# it. then when "foo.js" gets to run, its dynamically inserted img
# will not generate network request.
#
# in the less likely scenario, it could be that the html content after
# the script arrives slowly while the "foo.js" arrives quickly, so the
# image inserted by the script will win the race and generate the
# network request. another possible scenario is that the preload
# scanner does see the image before the script gets to run, but due to
# preload limits, it doesn't issue the network request, and then the
# script still gets to win.

class ResourceType(common.AutoNumber):
    MainResource = ()
    Image = ()
    CSSStyleSheet = ()
    Script = ()
    Font = ()
    Raw = ()

    Unknown = () # for fetches of resources that we/webkit don't know
                 #about, e.g., favicon ones
    pass

resource_type_short_names = {
    ResourceType.MainResource: "main",
    ResourceType.Image: "img",
    ResourceType.CSSStyleSheet: "css",
    ResourceType.Script: "script",
    ResourceType.Font: "font",
    ResourceType.Raw: "raw",
    }

resource_type_shortest_names = {
    ResourceType.MainResource: "M",
    ResourceType.Image: "I",
    ResourceType.CSSStyleSheet: "C",
    ResourceType.Script: "S",
    ResourceType.Font: "F",
    ResourceType.Raw: "R",
    }

resource_type_colors = {
    ResourceType.MainResource: "magenta",
    ResourceType.Image: "none",
    ResourceType.CSSStyleSheet: "yellow",
    ResourceType.Script: "red",
    ResourceType.Font: "skyblue1",
    ResourceType.Raw: "lawngreen",
    }

class Resource(DotDrawingNodeEntity, DependencyEntity):
    def __init__(self, instNum, url, type_, isDataURL, create_ts, creator_scope):
        DependencyEntity.__init__(self)
        DotDrawingNodeEntity.__init__(self)

        self.instNum = instNum
        self.url = url
        assert type(type_) == ResourceType
        self.type = type_
        self.isDataURL = isDataURL
        self.creator_scope = creator_scope
        # CreateResourceEdge(src=creator_scope, dst=self, ts=create_ts)

        self.create_ts = create_ts # when it is created by webkit

        # a resource can require multiple fetches due to redirects. we
        # use an array (/sequence/chain) of sizes for upstream and
        # downstream
        self._outboundSizes = []
        self._inboundSizes = []
        # a separate list for the requests info like method and url
        # because the size info sometimes is available after seeing
        # the 'follow-redirect' message due to being logged from
        # different processes
        self._request_chain = []

        self._initial_fetch = None


        # how much does this resource increment fetcher's request
        # count by? this is only to make sure the font stuff is sane
        # (sometimes each font increments req count a few times and
        # then decrements to the proper number). therefore, this value
        # should be either 0 or 1 once everything is said and done.
        self.inc_req_count_by = 0

        # this should be set to true once when we first increment the
        # inc_req_count_by. and this should not be cleared to False
        # after having beeen set to true
        self.contributes_to_req_count = False
        pass

    def get_type_shortname(self):
        return resource_type_short_names[self.type]

    def elements(self):
        '''return the set of elements that reference this resource'''

        elements = [edge.src() for edge in self.in_edges() if type(edge) == ReferencingEdge]
        if len(elements) > 1:
            # currently we expect only image resources can have
            # multiple elements point to it
            assert self.type == ResourceType.Image
            pass
        assert all(map(lambda elem: isinstance(elem, Element), elements))
        return set(elements)

    # append a total outbound size (size of request headers + request
    # body). 
    def append_outboundSize(self, s):
        assert len(self._request_chain) == (len(self._outboundSizes) + 1)
        assert len(self._inboundSizes) == len(self._outboundSizes)
        self._outboundSizes.append(s)
        pass

    def increment_current_outboundSize(self, incAmount):
        assert len(self._request_chain) == len(self._outboundSizes)
        assert len(self._inboundSizes) == (len(self._outboundSizes) - 1)
        self._outboundSizes[-1] += incAmount
        assert incAmount >= 0
        pass

    def outboundSizes(self):
        return self._outboundSizes

    # append a total inbound size (size of response headers + response
    # body).
    def append_inboundSize(self, s):
        assert len(self._request_chain) == (len(self._inboundSizes) + 1)
        assert len(self._outboundSizes) == (len(self._inboundSizes) + 1), \
          'res:{} has an inbound size without an outbound size numOutboundSizes= {} numInboundSizes= {}'.format(
              self.instNum, len(self._outboundSizes), len(self._inboundSizes))
        self._inboundSizes.append(s)
        assert len(self._inboundSizes) == len(self._outboundSizes)
        pass

    def inboundSizes(self):
        return self._inboundSizes

    def starts_fetching(self, ts, starter_scope, method, priority):
        # "starter_scope" must be valid if this is not the main
        # resource

        assert self._initial_fetch is None
        self._initial_fetch = FetchRequest(
            resource=self, method=method, url=self.url,
            priority=priority, ts=ts, starter_scope=starter_scope)

        if starter_scope:
            assert isinstance(starter_scope, ExecutionScope)
            FetchSchedulingEdge(src=starter_scope, dst=self._initial_fetch, ts=ts)
            pass

        return self._initial_fetch

    def finishes_fetching(self, ts, scope):
        assert self._initial_fetch is not None
        self._initial_fetch.set_finish_scope(scope)
        # logging.debug('======== {}'.format(self._request_chain))
        # logging.debug('======== {}'.format(self._outboundSizes))
        pass

    def initial_fetch_ts(self):
        return self._initial_fetch.ts()

    def initial_fetch_scope(self):
        return self._initial_fetch.starter_scope()

    def initial_fetch(self):
        return self._initial_fetch

    def toString(self, succinct=True):
        s = 'res:{instNum}'.format(instNum=self.instNum)
        if not succinct:
            s += ' type= [{type}] url= [{url}]'.format(type=self.type.name, url=self.url)
            pass
        return s

    def __str__(self):
        return self.toString()

    # _DrawDot api

    def draw_get_node_name(self):
        return 'res_{resInstNum}'.format(resInstNum=self.instNum)

    def draw_get_label(self):
        s = 'res:{instNum}'.format(instNum=self.instNum,
                                          type=resource_type_short_names[self.type])
        if self.isDataURL:
            s += '\\nisDataURL: {}'.format(self.isDataURL)
            pass
        return s

    def draw_get_fillcolor(self):
        return resource_type_colors[self.type]

    def draw_get_style(self):
        return 'filled'

    def draw_get_shape(self):
        return 'box'

    def draw(self, *args, **kwargs):
        if not self.in_edges() and not self.out_edges():
            # if we don't have any out edges or in edges, we are not
            # very interesting, let's just not draw us
            return False
        else:
            return super().draw(*args, **kwargs)
        pass

    pass

class MainResource(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        pass

    pass

class StyleSheetResource(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # while parsing css sheet, interesting things can happen,
        # e.g., "@import url(https://fonts.googleapis.com/css...)"
        # will cause a fetch. so, we use a scope in that case.
        #
        # exactly one of parse_duration and parse_scope is not None
        self.parse_duration = None
        self.parse_scope = None
        pass

    def draw_get_label(self):
        label = super().draw_get_label()
        assert self.parse_duration is not None
        if DotDrawing.show_timing:
            label += '\\nparseTime: {:.1f}'.format(self.parse_duration)
            pass
        assert len(self.elements()) == 1
        return label

    def __str__(self):
        return super().__str__() + ' parseTime= {parseDuration:.1f}'.format(
            parseDuration=self.parse_duration)

    pass

class ScriptResource(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    pass

class EventTarget(DotDrawingNodeEntity, DependencyEntity):
    def __init__(self, instNum, creator_scope, create_ts):
        DependencyEntity.__init__(self)
        DotDrawingNodeEntity.__init__(self)

        self.instNum = instNum

        # the current scope when this element is created
        self.creator_scope = creator_scope
        assert creator_scope is None or isinstance(creator_scope, ExecutionScope)
        if self.creator_scope:
            self.set_to_be_pruned(self.creator_scope.to_be_pruned())
            pass

        self.create_ts = create_ts

        self._create_edge = None

        # map from event name, e.g., "load", "DOMContentLoaded",
        # "readystatechange", etc. to scope
        # self.dom_event_handling_scopes = defaultdict(list)
        pass

    def add_dom_event_handling_scope(self, eventName, scope):
        # some event might be fired multiple times, e.g.,
        # "readystatechange"

        DOMEventSchedulingEdge(self, scope, eventName)

        scope.set_to_be_pruned(self.to_be_pruned())
        pass

    # return list of (event name, scope) pairs
    def dom_event_handling_scopes(self):
        retval = [(edge.eventName(), edge.dst()) for edge in self.out_edges() if type(edge) == DOMEventSchedulingEdge]
        return retval

    def toString(self, succinct=True):
        assert succinct
        s = self.__class__.__name__ + ' et:{instNum}'.format(instNum=self.instNum)
        return s

    def __str__(self):
        return self.toString()

    # draw api

    def draw_get_node_name(self):
        raise NotImplementedError

    pass

class Document(EventTarget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.uncompressed_html_size = None

        # # when the document has finished parsing (it will fire the
        # # DOMContentLoaded event)
        # self.finished_parsing_scope = None
        pass

    def draw_get_node_name(self):
        return 'doc_{instNum}'.format(instNum=self.instNum)

    def draw_get_label(self):
        return 'doc:{instNum}'.format(instNum=self.instNum)

    pass


class DOMTimer(DotDrawingNodeEntity, DependencyEntity):
    # timer IDs are unique per ExecutionContext, of which Document is
    # a subclass, and also this id space is circular. so that means we
    # might run into duplicate timer IDs. also, these IDs are not the
    # same as our other instNum's
    def __init__(self, timerID, interval, singleShot, creator_scope, create_ts):
        DependencyEntity.__init__(self)
        DotDrawingNodeEntity.__init__(self)

        self.timerID = timerID
        self.interval = interval
        # assert singleShot
        self.singleShot = singleShot
        assert creator_scope
        self.creator_scope = creator_scope
        self.set_to_be_pruned(self.creator_scope.to_be_pruned())

        # who clears us
        self.clearer_scope = None

        CreateDOMTimerEdge(creator_scope, self, ts=create_ts)

        # just for single shot case, when the scope doesn't do
        # anything interesting
        self._single_shot_fire_scope_duration = None

        self._already_simplified_entirely = False
        pass

    def timer_fired_scopes(self):
        return list(edge.dst() for edge in self.out_edges() if isinstance(edge.dst(), DOMTimerFiredScope))

    def add_timer_fired_scope(self, scope):
        if self.singleShot:
            # cannot have more than one fired scope for single shot
            # timers
            assert not self.timer_fired_scopes()
            pass

        assert isinstance(scope, DOMTimerFiredScope)

        SchedulingEdge(src=self, dst=scope)

        scope.set_to_be_pruned(self.to_be_pruned())
        pass

    def clear_timer(self, scope, ts):
        # the timer is cleared/removed by this clearer

        # it's ok to clear a timer more than once. it will just have
        # no effect
        if not self.clearer_scope:
            self.clearer_scope = scope
            ClearDOMTimerEdge(src=scope, dst=self, ts=ts)
            pass
        pass

    def toString(self, succinct=True):
        return 'DOMTimer:{timerID}'.format(timerID=self.timerID)

    def creator_entity(self):
        createedges = list(filter(lambda edge: type(edge) is CreateDOMTimerEdge, self.in_edges()))
        assert len(createedges) <= 1
        return createedges[0].src() if createedges else None

    def maybe_simplify_timer_fired_scopes(self):
        didmodify = False
        scopes = self.timer_fired_scopes()
        for scope in scopes:
            didmodify |= scope.maybe_simplify_entirely()
            pass
        return didmodify

    def maybe_simplify_entirely(self):
        if self._already_simplified_entirely:
            return False
        if len(self.out_edges()) == 0:
            # if there's NO timer fired scope, we can remove ourselves
            # entirely
            assert not self.timer_fired_scopes()
            self._validate_in_edges()
            num_inedges = len(self.in_edges())

            before = len(g_all_edges)
            self.drop_all_in_edges()
            after = len(g_all_edges)
            assert (after + num_inedges) == before
            assert not self.in_edges()
            self._already_simplified_entirely = True
            pass
        elif len(self.out_edges()) == 1:
            # if there's ONE timer fired scope, we can also remove
            # ourselves entirely but have to connect our creator to
            # the timer fired scope
            assert len(self.timer_fired_scopes()) == 1
            self._validate_in_edges()

            ourcreator = self.creator_entity()
            assert ourcreator

            num_inedges = len(self.in_edges())

            before = len(g_all_edges)
            self.drop_all_in_edges()
            after = len(g_all_edges)
            assert (after + num_inedges) == before
            assert not self.in_edges()

            # connect the creator and the timerfired scope
            outedge = list(self.out_edges())[0]
            outedge.adopt_new_src(ourcreator)
            assert len(g_all_edges) == after

            # we should not have out edges anymore
            assert len(self.out_edges()) == 0
            self._already_simplified_entirely = True
            pass

        return self._already_simplified_entirely

    def _validate_in_edges(self):
        # exactly one create edge, and at most one clear edge
        inedges = self.in_edges()
        assert len(inedges) <= 2
        if len(inedges) > 0:
            createedges = list(filter(lambda edge: type(edge) is CreateDOMTimerEdge, inedges))
            clearedges = list(filter(lambda edge: type(edge) is ClearDOMTimerEdge, inedges))
            assert len(createedges) == 1
            assert len(clearedges) <= 1
            pass
        return

    def __str__(self):
        return 'timer:{}'.format(self.timerID)

    # draw api

    def draw_get_shape(self):
        return 'box'

    def draw_get_fillcolor(self):
        return 'lightpink'

    def draw_get_style(self):
        return 'filled'

    def draw_get_node_name(self):
        return 'domtimer_{timerID}'.format(timerID=self.timerID)

    def draw_get_label(self):
        s = '#{timerID}'.format(timerID=self.timerID)
        if DotDrawing.show_timing:
            s += '\\nI:{interval}, Rep:{isRepeating}'.format(
                interval=self.interval, isRepeating=int(not self.singleShot))
            pass
        if self._single_shot_fire_scope_duration is not None:
            if DotDrawing.show_timing:
                s += '\\nsingleShotScopeDur: {duration:.1f}'.format(
                    duration=self._single_shot_fire_scope_duration)
                pass
            pass
        return s

    pass

class CreateXHREdge(SchedulingEdgeWithTs):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert type(self.dst()) is XMLHttpRequest
        pass

    def _draw_get_label_action(self):
        return 'creat'

    pass

class XMLHttpRequest(EventTarget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # don't add the create edge here; we'll add the edge when/if
        # the xhr actually loads some resource

        self._create_edge = CreateXHREdge(
            src=self.creator_scope, dst=self, ts=self.create_ts)

        self.is_async = None

        # the sequence of resources that are sent by this xhr, e.g.,
        # it can send a preflight request ("OPTIONS" http request),
        # when that succeeds, it send a POST request for the same url,
        # etc.
        self.resource_chain = []

        self._already_simplified_entirely = False

        pass

    # sometimes we don't care about original creator, e.g., the actual
    # scope that starts loading an xhr is only thing we care about,
    # not the scope that creates the xhr
    def override_creation_info(self, creator_scope, create_ts):
        assert isinstance(self, XMLHttpRequest)

        assert self._create_edge is not None
        self._create_edge.drop()
        self.creator_scope = creator_scope
        self.create_ts = create_ts

        # make the NEW creator scope create us
        self._create_edge = CreateXHREdge(
            src=self.creator_scope, dst=self, ts=self.create_ts)
        pass

    def maybe_simplify_entirely(self):
        if self._already_simplified_entirely:
            return False

        if len(self.out_edges()) == 0:
            assert len(self.in_edges()) == 1
            inedge = list(self.in_edges())[0]
            assert type(inedge) is CreateXHREdge
            inedge.drop()

            assert len(self.in_edges()) == 0
            self._already_simplified_entirely = True
            pass

        return self._already_simplified_entirely

    def draw_get_node_name(self):
        return 'xhr_{instNum}'.format(instNum=self.instNum)

    def draw_get_label(self):
        # return 'xhr:{instNum}'.format(instNum=self.instNum)
        return ' '

    def draw_get_shape(self):
        return 'octagon'

    def draw_get_fillcolor(self):
        return 'blueviolet'

    def draw_get_style(self):
        return 'filled'

    pass

class Element(EventTarget):
    def __init__(self, *args, tag, **kwargs):
        super().__init__(*args, **kwargs)

        self.byte_range_start = None
        self.byte_range_end = None

        self.tag = tag
        # assert tag in ('body', 'img', 'link', 'script', 'iframe')

        # the url that this element points to when it's part of main
        # page html, e.g., for an img elem, the "src" attribute is
        # part of main page html, and not set by some script or part
        # of an html fragment created by script. this could be none,
        # e.g., an <img> element without a "src" attribute, and its
        # src attribute is later set by script, possibly multiple
        # times
        self.initial_resource_url_from_html = None

        # for simplifying drawn graphs
        self._already_simplified_entirely = False
        pass

    def resource(self):
        '''return the resource that this element references. might be None'''

        resources = [edge.dst() for edge in self.out_edges() if type(edge) == ReferencingEdge]
        if len(resources) > 1:
            logging.warning('element {elem} (tag: {tag}) referencing multiple ({num}) resources: {resources}'.format(
                elem=self.instNum, num=len(resources), tag=self.tag,
                resources=str(list(map(lambda r: str(r), resources)))))
            pass
        resource = resources[0] if resources else None
        assert isinstance(resource, Resource) or resource is None
        return resource

    def __str__(self):
        res = self.resource()
        s = 'elem:{instNum} byte range [{start}, {end}], res:{resInstNum}'.format(
            instNum=self.instNum, start=self.byte_range_start, end=self.byte_range_end,
            resInstNum=res.instNum if res else 0)
        return s

    def maybe_simplify_entirely(self):
        if self._already_simplified_entirely:
            return False

        # # if we have zero out edges, then we can simplify ourselves,
        # # i.e., go ahead and remove the one or two incoming edges if
        # # any
        # if len(self.out_edges()) == 0:
        #     inedges = self.in_edges()
        #     assert len(inedges) <= 2
        #     if len(inedges) == 1:
        #         inedge = list(inedges)[0]
        #         inedge.drop()
        #         assert len(inedges) == 0
        #         pass
        #     elif len(inedges) == 2:
        #         assert all(
        #         pass
        #     self._already_simplified_entirely = True
        #     pass

        return self._already_simplified_entirely

    # draw api

    def draw_get_shape(self):
        return 'box'

    def draw_get_label(self):
        assert self.tag
        s = 'elem:{type}'.format(type=self.tag)
        if self.byte_range_end is not None and DotDrawing.show_elem_end_byte_offset:
            s += '\\nend@{end}B'.format(end=self.byte_range_end)
            pass
        return s

    def draw_get_node_name(self):
        return 'elem_{elemInstNum}'.format(elemInstNum=self.instNum)

    pass

class LinkElement(Element):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, tag='link', **kwargs)

        self.rel = None
        self.href = None
        self.completeURL = None

        self.is_blocking_stylesheet = False

        pass

    def toString(self, succinct=True):
        if succinct:
            return super().toString()
        else:
            return 'link ' + super().__str__() + ' rel= [{rel}] href= [{href}] url= [{url}]'.format(
                rel=self.rel, href=self.href, url=self.completeURL)
        pass

    def __str__(self):
        return self.toString()

    def draw_get_label(self):
        label = super().draw_get_label()
        label += '\\nrel:{}'.format(self.rel)
        if self.rel == 'stylesheet':
            label += '\\nblocking:{}'.format(int(self.is_blocking_stylesheet))
            pass
        return label

    pass

class IFrameElement(Element):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, tag='iframe', **kwargs)
        self.src = None
        pass

    def toString(self, succinct=True):
        if succinct:
            return super().toString()
        else:
            return 'iframe ' + super().__str__() + ' src= [{src}]'.format(
                src=self.src)
        pass

    __str__ = toString

    pass

class HtmlElement(Element):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, tag='html', **kwargs)
        pass

    def resource(self):
        return None

    def draw_get_label(self):
        return 'html\\nend offset: {end}'.format(
            start=self.byte_range_start, end=self.byte_range_end)

    pass

class BodyElement(Element):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, tag='body', **kwargs)
        pass

    def resource(self):
        return None

    def draw_get_label(self):
        return 'body\\nend offset: {end}'.format(
            start=self.byte_range_start, end=self.byte_range_end)

    pass

class ImageElement(Element):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, tag='img', **kwargs)

        self.src = None
        self.completeURL = None

        pass

    def __str__(self):
        res = self.resource()
        return 'img elem:{instNum} byte range [{start}, {end}], res:{resInstNum}, ' \
            'url= [{url}]'.format(
                instNum=self.instNum, start=self.byte_range_start, end=self.byte_range_end,
                resInstNum=res.instNum if res else 0,
                url=self.completeURL)

    pass

class MetaEdge(DependencyEdge):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    def set_src(self, src):
        # won't insist on the use of SchedulingEdgeWithTs if src is a
        # scope
        assert self._src is None and isinstance(src, DependencyEntity)
        self._src = src
        self._src.add_out_edge(self)
        pass

    pass

class MetaElement(Element):
    # for grouping a bunch of peer entities into one, just for display
    # purposes, to keep graph size small for visual clarity
    #
    # currently handle only link and image elements

    def __init__(self, *args, **kwargs):
        super().__init__(*args, tag='METAELEM', **kwargs)
        self.img_instNums = set()
        self.link_instNums = set()

        # either 'default' to show only number of elems, or
        # 'show-elem-instNums' to also list the element instNum's
        self.label_mode = 'default'
        pass

    def draw_get_label(self):
        label = self.tag
        for header, instNumsSet in (('IMGs', self.img_instNums),
                                    ('LINKs', self.link_instNums),
                                    ):
            if instNumsSet:
                label += '\\n{numElems} {header}'.format(numElems=len(instNumsSet), header=header)
                if self.label_mode == 'show-elem-instNums':
                    label += ': '
                    numPerLine = 5 # 5 on same line with the header
                    i = 1
                    for instNum in sorted(instNumsSet):
                        label += str(instNum) + ','
                        if (i % numPerLine) == 0:
                            label += '\\n'
                            if numPerLine == 5:
                                numPerLine = 7 # 7 on subsequent lines
                                i = 0 # reset i
                                pass
                            pass
                        i += 1
                        pass
                    pass
                pass
            pass
        return label

    pass

class ScriptElement(Element):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, tag='script', **kwargs)

        self.completeURL = None

        self.is_parser_blocking = False

        # self.compile_begin_ts = None
        # self.compile_done_ts = None

        # how long it takes to compile / run the script
        self.compile_duration = None

        self.willBeParserExecuted = None
        self.willExecuteWhenDocumentFinishedParsing = None
        self.readyToBeParserExecuted = None

        # async exec means doesn't depend on css/parser. just execute
        # when finished downloading the resource.... but if this
        # script has willExecuteInOrder=true, then has to be exec'ed
        # in the same order it was encountered as other
        # willExecuteInOrder=true scripts
        self.queueForAsyncExecution = None
        self.willExecuteInOrder = None

        self.parserInserted = None
        self._hasSrcAttr = None
        # init to false
        self._shouldExecImmediately = False
        self.defer = None
        self.async = None

        self.alreadyStarted = None

        # run_edge points to ScriptControllerRunScriptScope. if it's
        # simplified then we will remember its duration
        self._run_edge = None
        self._run_scope_duration = None

        self.script_prepare_scope = None
        pass

    def hasSrcAttr(self):
        # assert self._hasSrcAttr is not None
        return self._hasSrcAttr

    def set_hasSrcAttr(self, val):
        assert self._hasSrcAttr is None
        assert type(val) is bool
        self._hasSrcAttr = val
        pass

    def shouldExecImmediately(self):
        return self._shouldExecImmediately

    def set_shouldExecImmediately(self, val):
        assert type(val) is bool
        self._shouldExecImmediately = val
        pass

    def set_script_prepare_scope(self, scope):
        assert self.script_prepare_scope is None
        self.script_prepare_scope = scope
        pass

    def set_script_execution_scope(self, scope):
        # assert self.instNum != 40
        assert self._run_edge is None
        self._run_edge = SchedulingEdge(src=self, dst=scope)
        pass

    def script_execution_scope(self):
        return self._run_edge.dst() if self._run_edge else None

    def maybe_simplify_run_scope(self):
        # return True if something was done; return False if nothing
        # was done
        if not self._run_edge:
            return False

        scope = self._run_edge.dst()
        assert scope and (self._run_scope_duration is None)
        assert len(scope.out_edges()) <= 1, (scope.out_edges())
        if scope.count_in_edges(True) > 1:
            return False

        if not scope.count_out_edges(True):
            self._run_scope_duration = scope.duration()

            before = len(g_all_edges)
            self._run_edge.drop()
            self._run_edge = None
            after = len(g_all_edges)
            assert (after + 1) == before
            # assert not self.out_edges()
            assert not scope.count_in_edges(True)
            assert not scope.count_out_edges(True)
            return True
        else:
            ret = scope.maybe_simplify_entirely()
            if ret:
                self._run_edge = None
                pass
            return ret
        raise Exception('not reached')

    def draw_get_label(self):
        label = super().draw_get_label()
        if self._run_scope_duration is not None:
            # we have simplied our run scope!
            assert self._run_edge is None
            if DotDrawing.show_timing:
                label += '\\nrunScopeDur: {:.1f}'.format(self._run_scope_duration)
                pass
            pass
        return label

    def __str__(self):
        res = self.resource()
        return 'script elem:{instNum} byte range [{start}, {end}], res:{resInstNum}, ' \
            'url= [{url}]'.format(
                instNum=self.instNum, start=self.byte_range_start, end=self.byte_range_end,
                resInstNum=res.instNum if res else 0,
                url=self.completeURL)

    pass

class ShadowHostElement(Element):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, tag='_shadowhost_', **kwargs)
        pass

    def resource(self):
        return None

    pass

class ExecutionScope(DotDrawingScopeEntity, DependencyEntity):
    # this id is created/maintained here in python code, not parsed
    # from log
    next_scope_id = 0

    @abstractmethod
    def __init__(self, open_ts, open_event_seq_no, outer_scope,
                 scopeStart=None, recursionLevel=None):
        '''

        outer_scope is the current active scope when this scope opens.

        optionally, scopeStart is (assumed to be) the time the scope
        opens (potentially different than open_ts) (or it can be any
        other value the user wants to use). if specified at
        construction, then the same value must be passed to close().

        '''

        DependencyEntity.__init__(self)
        DotDrawingScopeEntity.__init__(self)

        self._scope_id = ExecutionScope.next_scope_id
        ExecutionScope.next_scope_id += 1

        self._open_ts = open_ts
        self._close_ts = None
        self._recursionLevel = recursionLevel

        # terminology: outer and inner scopes have "contains"
        # relationships: outer scope contains inner scope, and inner
        # scope is contained in outer scope. (so,
        # "self._outer_scope" can be renamed "self._contained_in")

        self._outer_scope = None

        assert outer_scope is None or isinstance(outer_scope, ExecutionScope)
        if outer_scope:
            # same importantness
            assert self.is_important() == outer_scope.is_important()
            self._outer_scope = outer_scope
            self._outer_scope._add_inner_scope(self)

            self.set_to_be_pruned(self._outer_scope.to_be_pruned())
            pass

        # contains only direct inner scopes, i.e., each inner code
        # might have its own inner scopes, but this scope will not
        # directly link to those descendants
        self._inner_scopes = []

        # event sequence numbers at which this scope opens and closes
        self._open_event_seq_no = open_event_seq_no
        self._close_event_seq_no = None

        self._scopeStart = scopeStart

        self._already_simplified_entirely = False
        pass

    @abstractmethod
    def is_important(self):
        raise NotImplementedError

    def count_in_edges(self, includeInnerScopes=False):
        # "includeInnerScopes" = true will get all edges of this scope
        # as well as its inner scopes
        numedges = len(self.in_edges())
        if includeInnerScopes:
            numedges += sum([scope.count_in_edges(True) for scope in self._inner_scopes], 0)
            pass
        return numedges

    def count_out_edges(self, includeInnerScopes=False):
        # "includeInnerScopes" = true will get all edges of this scope
        # as well as its inner scopes
        numedges = len(self.out_edges())
        if includeInnerScopes:
            numedges += sum([scope.count_out_edges(True) for scope in self._inner_scopes], 0)
            pass
        return numedges

    def inner_scopes(self):
        return self._inner_scopes

    def scope_id(self):
        return self._scope_id

    def close(self, ts, event_seq_no, scopeStart=None):
        # close this scope at time ts
        assert not self.has_closed()
        self._close_ts = ts
        assert self._close_ts >= self._open_ts
        self._close_event_seq_no = event_seq_no
        assert self._close_event_seq_no > self._open_event_seq_no

        assert self._scopeStart == scopeStart, \
          'scopes created with "scopeStart" must be closed with same "scopeStart" value'
        pass

    def _add_inner_scope(self, scope):
        self._check_inner_scope(scope)
        assert self.is_important() == scope.is_important()
        if self._inner_scopes:
            assert self._inner_scopes[-1].has_closed()
            pass
        self._inner_scopes.append(scope)
        pass

    def _check_inner_scope(self, scope):
        # derived classes should overload
        pass

    def scopeStart(self):
        return self._scopeStart

    def recursion_level(self):
        return self._recursionLevel

    def outer_scope(self):
        return self._outer_scope

    def outer_scopes_to_root(self):
        # return the list/path of OUTER scopes -- not including myself
        # -- all the way to the root/outermost scope

        # retlist = [self]
        retlist = []
        outer = self.outer_scope()
        while outer:
            retlist.append(outer)
            outer = outer.outer_scope()
            pass
        return retlist

    def has_closed(self):
        return self._close_ts != None

    def open_ts(self):
        return self._open_ts
    def open_event_seq_no(self):
        return self._open_event_seq_no

    def close_ts(self):
        return self._close_ts
    def close_event_seq_no(self):
        return self._close_event_seq_no

    def duration(self):
        assert self.has_closed()
        return self._close_ts - self._open_ts

    def drop(self):
        if self._outer_scope:
            self._outer_scope.remove_inner_scope(self)
            pass
        pass

    def remove_inner_scope(self, scope):
        numBefore = len(self._inner_scopes)
        self._inner_scopes.remove(scope)
        assert len(self._inner_scopes) == (numBefore - 1)
        pass

    def __str__(self):
        return self.toString()

    def toString(self, succinct=True):
        s = 'scope #{id}:'.format(id=self._scope_id) + '  ' + self.__class__.__name__ + ' ({})'.format(self.open_ts())
        if not succinct:
            s += ' (dur= {duration:.1f})'.format(duration=self.duration())
            pass
        return s

    def _draw_get_subgraph_style(self):
        return 'filled'

    def _draw_get_scope_short_name(self):
        return None

    def maybe_simplify_entirely(self):
        if self._already_simplified_entirely:
            return False

        if self.count_out_edges(True) == 0 and len(self.in_edges()) == 1:
            inedge = list(self.in_edges())[0]
            assert inedge

            before = len(g_all_edges)
            inedge.drop()
            after = len(g_all_edges)
            assert (after + 1) == before
            assert not self.in_edges()

            # we should not have out edges anymore
            assert not self.out_edges()
            self._already_simplified_entirely = True
            pass

        return self._already_simplified_entirely

    # drawing api

    def draw_get_subgraph_name(self):
        return 'cluster_{scopeID}_'.format(scopeID=self._scope_id)

    def draw_get_node_name(self):
        return 'c_{scopeID}_node'.format(scopeID=self._scope_id)

    def draw_get_label(self):
        # label = '{name} #{scopeId}\n{dur:.1f} ms'.format(
        #     name=self.__class__.__name__, scopeId=self._scope_id, dur=self.duration())
        # label += ' [{open_ts:.2f}, {close_ts:.2f}]'.format(
        #     open_ts=self._open_ts, close_ts=self._close_ts)
        name = self._draw_get_scope_short_name()
        if not name:
            name = re.sub('Scope$', '', self.__class__.__name__)
            pass
        label = '{name}'.format(name=name)
        if DotDrawing.show_timing:
            label += '\\n{dur:.1f}ms'.format(dur=self.duration())
            pass
        return label


    # static helper vars
    _scope_subgraph_specs = {}

    def draw(self, nodes_specs, edges_specs,
             indentAmount, scope_verbosity=DotDrawingEntity.ScopeVerbosity.NORMAL):
        assert not self._is_visited, '{}'.format(self)
        self._is_visited = True

        indent = ' '*indentAmount

        logging.debug(indent + 'begin drawing {}'.format(self))

        mysubgraphspec = {
            'name': None,
            'attrs': {},
            'comment': None,
            'subgraphs': [],
            }

        ###
        ### first, open cluster subgraph for us, and also add the
        ### dummy node
        draw_myself = len(self.in_edges()) > 0 or len(self.out_edges()) > 0
        if draw_myself:
            myclusterName = self.draw_get_subgraph_name()
            mysubgraphspec['name'] = myclusterName
            mysubgraphspec['comment'] = '{classname} [{start}, {end}]'.format(
                classname=self.__class__.__name__, start=self.open_ts(), end=self.close_ts())
            mysubgraphspec['attrs']['label'] = self.draw_get_label()
            mysubgraphspec['attrs']['style'] = self._draw_get_subgraph_style()
            mysubgraphspec['nodes'] = {
                self.draw_get_node_name(): {
                    'name': self.draw_get_node_name(),
                    'attrs': {'style': 'invis', 'shape': 'point'},
                    },
            }
            pass

        ###
        ### maybe draw inner scopes
        ###
        logging.debug(indent + 'maybe drawing inner scopes')
        did_draw_inner_scopes = False
        for scope in self._inner_scopes:
            logging.debug(indent + 'inner scope {}...'.format(scope))
            if scope.draw_is_visited() or scope.draw(
                    nodes_specs=nodes_specs, edges_specs=edges_specs,
                    indentAmount=indentAmount+4, scope_verbosity=scope_verbosity):
                logging.debug(indent + 'did draw or already visited')

                # did the inner scope draw itself?
                inner_scope_subgraph_spec = ExecutionScope._scope_subgraph_specs.get(scope, None)
                if inner_scope_subgraph_spec:
                    # yes, so we absorb its drawn buf into ours
                    # assert inner_scope_subgraph_spec['name'] not in mysubgraphspec['subgraphs'], '{} {}'.format(mysubgraphspec['name'], inner_scope_subgraph_spec)
                    mysubgraphspec['subgraphs'].append(inner_scope_subgraph_spec)
                    del ExecutionScope._scope_subgraph_specs[scope]
                    did_draw_inner_scopes = True
                    pass
                pass
            else:
                logging.debug(indent + 'DID NOT draw')
                pass
            pass

        ###
        ### ok, now draw out edges if any
        ###
        did_draw_edges = self._draw_out_edges(
            nodes_specs=nodes_specs, edges_specs=edges_specs,
            src_node_name=self.draw_get_node_name(),
            indentAmount=indentAmount, out_edges=self.out_edges(),
            scope_verbosity=scope_verbosity)

        ###
        ### save our scope for later use by an outer scope or for
        ### outputting
        ###
        interesting = draw_myself or did_draw_inner_scopes or did_draw_edges
        logging.debug(indent + '{draw_myself}, {did_draw_inner_scopes}, {did_draw_edges}'.format(
            draw_myself=draw_myself, did_draw_inner_scopes=did_draw_inner_scopes, did_draw_edges=did_draw_edges))
        if interesting:
            logging.debug(indent + 'save my drawn buf')
            assert self not in ExecutionScope._scope_subgraph_specs
            ExecutionScope._scope_subgraph_specs[self] = mysubgraphspec
            pass

        logging.debug(indent + 'done drawing {}... returning {interesting}\n'.format(
            self, interesting=interesting))

        return interesting

    @staticmethod
    def check_non_overlapping(scopes):
        # make sure the scopes do not overlap
        prev_close_ts = None
        for scope in scopes:
            assert type(scope.open_ts()) == type(scope.close_ts()) == float, \
              '{} {}'.format(scope.open_ts(), scope.close_ts())
            assert 0 < scope.open_ts() <= scope.close_ts()
            if prev_close_ts is not None:
                assert prev_close_ts <= scope.open_ts()
                pass
            prev_close_ts = scope.close_ts()
            pass
        pass

    @staticmethod
    def get_active_scope(scopes):
        # return the current scope that has not been closed/ended, or
        # None otherwise
        if scopes and not scopes[-1].has_closed():
            return scopes[-1]
        return None

    pass


# an execution scope can be either important or
# unimportant. "important" generally means high-level ideas that is
# intrinsic about a page model, regardless of the specific load
# instance. for examples, V8Scope and RenderTreeUpdateScope.
#
# "unimportant" scopes include implementation-details like pumping the
# html parser and the various Promise-related scopes (they allow the
# log processor to establish relationships between related events,
# validate certain assumptions, etc.), and load-instance-specific
# scopes like preload scanning. for example, in the case of preload
# scanning, the preload scanner runs only if the parser has unparsed
# html that it cannot parse due to being blocked waiting to fetch a
# script; if in some load instance, the script downloads
# instantaneously (e.g., cached), then the parser will immediately
# execute it and not run the preload scanner, then that load instance
# will not have the preload scanning scope.
#
# execution scopes can have inner and outer scopes, but only of the
# same "importantness": e.g., an important scope can only have
# important outer scope/inner scopes.
#

class ImportantExecutionScope(ExecutionScope):
    # this is abstract to force subclassing
    @abstractmethod
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    def is_important(self):
        return True

    pass

class UnimportantExecutionScope(ExecutionScope):
    # this is not abstract to make it easy to create quick-and-dirty
    # scopes
    @abstractmethod
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    def is_important(self):
        return False

    pass

class EventTargetEventScopeBase(ExecutionScope, ABC):
    '''abstract base class for all scopes that deal with some event for an
    event target
    '''

    @abstractmethod
    def __init__(self, *args, eventName, eventTargetInstNum, **kwargs):
        # set these before calling parent class's constructor because
        # the we will try to add ourselves to the outer scope, which
        # will call _check_inner_scope(), and we want to have our
        # eventName and eventTargetInstNum already set to perform the
        # check
        self.eventName = eventName
        self.eventTargetInstNum = eventTargetInstNum

        ExecutionScope.__init__(self, *args, **kwargs)
        pass

    def _check_inner_scope(self, inner_scope):
        if isinstance(inner_scope, EventTargetEventScopeBase):
            assert self.eventName == inner_scope.eventName, \
              '{} != {}'.format(self.eventName, inner_scope.eventName)
            assert self.eventTargetInstNum == inner_scope.eventTargetInstNum, \
              '{} != {}'.format(self.eventTargetInstNum, inner_scope.eventTargetInstNum)
            pass
        pass

    pass

# for now we don't support nested v8 scopes, but for sure we will have
# to; it's common for scripts to use dynamic html insertions (e.g.,
# "doc.write(...)", "innerHtml = ...", etc.) to inject script elements
# that the parser will then execute, thus entering a nested v8 scope
class V8Scope(ImportantExecutionScope):
    # represents a scope of v8 execution: a time interval during which
    # the v8 script engine is running, e.g., executing a script
    # element, or executing an event handler, etc.

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    pass

class DocOnLoadEventScope(EventTargetEventScopeBase, ImportantExecutionScope):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    pass

class ScriptControllerRunScriptScope(ImportantExecutionScope):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.scriptElement = None
        pass

    def toString(self, *args, **kwargs):
        return super().toString(*args, **kwargs) + \
          ' elem:{elemInstNum}'.format(elemInstNum=self.scriptElement.instNum)

    def _draw_get_scope_short_name(self):
        return 'RunScript'

    pass


class RenderTreeUpdateScope(ImportantExecutionScope):
    #
    # "is_initial" means it's the first render tree update scope of
    # the page load; it seems that this scope is never scheduled by
    # any specific scope, and is scheduled when the document sees the
    # <body> element:
    #
    # t=1, ts=    236.49, HTMLBodyElement.cpp :181, insertedInto(): ___ doc:7: doc:7 now has body elem:204
    # t=1, ts=    236.50, Document.h :1408, scheduleRenderTreeUpdateIfNeeded(): ___ scopeStart:1474708877747.767822: doc:7: maybe schedule render tree update: begin
    # t=1, ts=    236.51, Document.cpp :1829, scheduleRenderTreeUpdate(): ___ doc:7: render tree update is scheduled
    #

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._is_initial = False
        self._already_simplified_entirely = False
        pass

    def is_initial(self):
        return self._is_initial

    def set_is_initial(self):
        assert not self._is_initial
        self._is_initial = True
        pass

    def draw_get_label(self):
        s = super().draw_get_label()
        if self._is_initial:
            s += '\\ninitialUpdate'
            pass
        return s

    def _draw_get_scope_short_name(self):
        return 'RenderUpdate'

    pass

class RenderViewLayoutScope(ImportantExecutionScope):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._is_initial = False
        self._already_simplified_entirely = False
        pass

    def set_is_initial(self):
        assert not self._is_initial
        self._is_initial = True
        pass

    def draw_get_label(self):
        s = super().draw_get_label()
        if self._is_initial:
            s += '\\ninitialLayout'
            pass
        return s

    def _draw_get_scope_short_name(self):
        return 'Layout'

    pass

class LoadPendingFontsTimerFiredScope(ImportantExecutionScope):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    def _draw_get_scope_short_name(self):
        return 'LoadFontsTimer'

    pass

# when resource loader/fetcher finishes loading/fetching a resource,
# webkit might perform actions that we want to know about, e.g., run
# onload handler, parse the stylesheet contents, etc.
class FetchFinishScope(ImportantExecutionScope):
    def __init__(self, *args, resInstNum, **kwargs):
        super().__init__(*args, **kwargs)
        self.resInstNum = resInstNum
        pass

    def maybe_simplify_entirely(self):
        if self._already_simplified_entirely:
            return False
        assert len(self.in_edges()) == 1
        if len(self.out_edges()) == 1:
            # if there's ONE out edge, we can also remove ourselves
            # entirely but have to connect our in edge and out edge
            inedge = list(self.in_edges())[0]
            assert inedge
            fetchreq = inedge.src()

            before = len(g_all_edges)
            inedge.drop()
            after = len(g_all_edges)
            assert (after + 1) == before
            assert not self.in_edges()

            # connect the fetchreq and the out edge dst
            outedge = list(self.out_edges())[0]
            outedge.adopt_new_src(fetchreq)
            assert len(g_all_edges) == after

            # we should not have out edges anymore
            assert not self.out_edges()
            self._already_simplified_entirely = True
            pass

        return self._already_simplified_entirely

    def toString(self, succinct=True):
        s = super().toString(succinct)
        s += ' res:{instNum}'.format(instNum=self.resInstNum)
        return s

    def _draw_get_scope_short_name(self):
        return 'FetchFin'

    pass


class EventTargetInvokingListenersScope(EventTargetEventScopeBase, ImportantExecutionScope):
    def __init__(self, *args, **kwargs):
        EventTargetEventScopeBase.__init__(self, *args, **kwargs)
        # UnimportantExecutionScope.__init__(self, *args, **kwargs)
        pass

    def toString(self, succinct=True):
        s = super().toString(succinct)
        s += ' event "{eventName}", et:{target}'.format(
            eventName=self.eventName, target=self.eventTargetInstNum)
        return s

    def _draw_get_scope_short_name(self):
        return 'eventhandling'

    pass

class EventTargetInvokingOneListenerScope(EventTargetEventScopeBase, UnimportantExecutionScope):
    def __init__(self, *args, **kwargs):
        EventTargetEventScopeBase.__init__(self, *args, **kwargs)
        # UnimportantExecutionScope.__init__(self, *args, **kwargs)
        pass

    def __str__(self):
        s = super().__str__() + ' event "{eventName}", et:{target}'.format(
            eventName=self.eventName, target=self.eventTargetInstNum)
        return s

    pass


class DOMTimerFiredScope(ImportantExecutionScope):
    def __init__(self, *args, timerID, **kwargs):
        super().__init__(*args, **kwargs)
        self.timerID = timerID
        pass

    def toString(self, *args, **kwargs):
        s = super().toString(*args, **kwargs)
        s += ' timerID:{timerID}'.format(timerID=self.timerID)
        return s

    def draw_get_label(self):
        return 'TimerFired'

    pass

class ExecuteScriptsWaitingForStyleResourcesScope(ImportantExecutionScope):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    def draw_get_label(self):
        return 'ExecBlockedScripts'

    pass

class ExecuteParserBlockingScriptScope(ImportantExecutionScope):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    pass


class SettlePromiseEdge(SchedulingEdgeWithTs):
    # some scope settles a promise

    @abstractmethod
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert isinstance(self.dst(), Promise), self.dst()
        assert isinstance(self.src(), ExecutionScope), self.src()
        pass

    pass

class ResolvePromiseEdge(SettlePromiseEdge):
    # some scope resolves a promise
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    def draw_get_label(self):
        return 'reso'

    pass

class RejectPromiseEdge(SettlePromiseEdge):
    # some scope rejects a promise
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    def draw_get_label(self):
        return 'rej'

    pass

class ExecutePromiseEdge(SchedulingEdge):
    def __init__(self, src, dst):
        assert type(src) is Promise, src
        assert isinstance(dst, ExecutePromiseScope)
        super().__init__(src=src, dst=dst)
        pass

    def draw_get_label(self):
        return ''

    pass

class PromiseChainEdge(SchedulingEdge):
    def __init__(self, src, dst):
        assert isinstance(src, Promise)
        assert type(dst) is Promise, dst
        super().__init__(src=src, dst=dst)
        pass

    def draw_get_label(self):
        return 'chain'

    pass

class OnResolvePromiseEdge(SchedulingEdge):
    # from a promise to its resolve microtask run scope
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert isinstance(self.src(), Promise)
        assert type(self.dst()) is RunMicrotaskScope
        pass

    def draw_get_label(self):
        return 'onReso'

    pass

class OnRejectPromiseEdge(SchedulingEdge):
    # from a promise to its reject microtask run scope
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        assert isinstance(self.src(), Promise)
        assert type(self.dst()) is RunMicrotaskScope
        pass

    def draw_get_label(self):
        return 'onRej'

    pass

# "A promise is said to be settled if it is either fulfilled or
# rejected"
class Promise(DotDrawingNodeEntity, DependencyEntity):
    def __init__(self, instNum, create_ts, creator_scope):
        ## "creator_scope" should be the OUTER scope during which the
        ## promise is created, NOT the CreatePromiseScope
        DependencyEntity.__init__(self)
        DotDrawingNodeEntity.__init__(self)

        self.instNum = instNum
        self.create_ts = create_ts

        self.creator_scope = creator_scope

        # prevent mistake of giving the scope of creating myself as my
        # creator scope
        if isinstance(self.creator_scope, CreatePromiseScope):
            assert self.creator_scope.promiseInstNum != self.instNum
            pass

        # these two are just for quick error checking; we can get to
        # them through out_edges()
        self._execute_edge = None

        # src of this edge is who settles us
        self._settle_edge = None # there can be only one: either
                                 # resolve or reject

        # maybe_simplify_onSettle_microtask_scope(): sets
        # _onSettle_microtask_scope_duration and clears _settle_edge
        self._onSettle_edge = None
        self._onSettle_microtask_scope_duration = None

        CreatePromiseActionEdge(src=creator_scope, dst=self, ts=create_ts)

        pass

    def set_execute_scope(self, scope):
        assert not self._execute_edge
        self._execute_edge = ExecutePromiseEdge(src=self, dst=scope)
        pass

    def add_chain_promise(self, promise):
        PromiseChainEdge(src=self, dst=promise)
        pass

    def set_settler_scope(self, scope, type, ts):
        # "scope" is the scope that settles this promise; "type" is
        # the settling type, which must be "resolve" or "reject"
        assert not self._settle_edge

        if type == 'resolve':
            edgeclass = ResolvePromiseEdge
            pass
        elif type == 'reject':
            edgeclass = RejectPromiseEdge
            pass
        else:
            raise Exception('bad')
        self._settle_edge = edgeclass(src=scope, dst=self, ts=ts)
        pass

    def set_onSettle_microtask_scope(self, scope):
        # when the promise settles, it schedules a microtask to do
        # work; that is the "scope" we're setting here
        assert self._settle_edge # must have been settled
        assert not self._onSettle_edge
        if type(self._settle_edge) is ResolvePromiseEdge:
            edgeclass = OnResolvePromiseEdge
            pass
        else:
            edgeclass = OnRejectPromiseEdge
            pass
        self._onSettle_edge = edgeclass(src=self, dst=scope)
        pass

    def maybe_simplify_onSettle_microtask_scope(self):
        if not self._onSettle_edge:
            return False

        scope = self._onSettle_edge.dst()
        assert scope and (self._onSettle_microtask_scope_duration is None)
        assert scope.count_in_edges(True) == 1

        if scope.count_out_edges(True) <= 1:
            self._onSettle_microtask_scope_duration = scope.duration()

            # drop my edge to the scope
            before = len(g_all_edges)
            self._onSettle_edge.drop()
            self._onSettle_edge = None
            after = len(g_all_edges)
            assert (after + 1) == before
            assert not scope.count_in_edges(True)

            # if the scope has an out edge, then make myself the src
            # of that edge
            if scope.count_out_edges(True) == 1:
                scopesedge = list(scope.out_edges())[0]
                scopesedge.adopt_new_src(self)
                assert len(g_all_edges) == after
                pass
            return True

        return False

    def maybe_simplify_entirely(self):
        # some promises, especially the "deferred" ones created for
        # thening/chaining, don't do anything interesting. this will
        # completely remove itself if it's not interesting

        # condition: has only 3 in edges: create, chaining, and settle
        # (resolve/reject), and no out going edges

        if not self.out_edges() and len(self.in_edges()) == 3:
            inedges = list(self.in_edges())
            if any(map(lambda edge: isinstance(edge, PromiseChainEdge), inedges)) and \
              any(map(lambda edge: isinstance(edge, SettlePromiseEdge), inedges)) and \
              any(map(lambda edge: isinstance(edge, CreatePromiseActionEdge), inedges)):
                # ok! let's simplify ourselves
                before = len(g_all_edges)
                inedges[0].drop()
                inedges[1].drop()
                inedges[2].drop()
                after = len(g_all_edges)
                assert (after + 3) == before
                assert not self.out_edges() and not self.in_edges()
                return True
            pass

        return False

    def maybe_simplify_execute_scope(self):
        if not self._execute_edge:
            return False

        scope = self._execute_edge.dst()
        assert scope

        # we should be the only one pointing to the scope
        assert scope.count_in_edges() == 1

        ## it's ok for the execute scope to have no out edges, e.g.,
        ## it might have created a timer that never fired and thus has
        ## been simplified entirely

        # assert scope.count_out_edges() >= 1

        if scope.count_out_edges() == 1:
            scopesedge = list(scope.out_edges())[0]

            # drop my execute edge
            before = len(g_all_edges)
            self._execute_edge.drop()
            self._execute_edge = None
            after = len(g_all_edges)
            assert (after + 1) == before
            assert not scope.count_in_edges(True)

            # make myself as the execute scope's src
            scopesedge.adopt_new_src(self)
            assert len(g_all_edges) == after
            return True
        elif scope.count_out_edges() == 0:
            # drop my execute edge
            before = len(g_all_edges)
            self._execute_edge.drop()
            self._execute_edge = None
            after = len(g_all_edges)
            assert (after + 1) == before
            assert not scope.count_in_edges(True)

            return True            

        return False

    def __str__(self):
        return 'prom:{}'.format(self.instNum)

    def _promise_get_type(self):
        return None

    # draw api

    def draw_get_label(self):
        s = '#{instNum}'.format(instNum=self.instNum)
        type = self._promise_get_type()
        if type:
            s += '({type})'.format(type=type)
            pass
        if self._onSettle_microtask_scope_duration is not None:
            assert self._onSettle_edge is None
            if DotDrawing.show_timing:
                s += '\\nreslvDur:{:.1f}'.format(self._onSettle_microtask_scope_duration)
                pass
            pass

        return s

    def draw_get_shape(self):
        return 'box'

    def draw_get_node_name(self):
        return 'promise_{instNum}_'.format(instNum=self.instNum)

    def draw_get_style(self):
        return 'filled'

    def draw_get_fillcolor(self):
        return 'orange'

    def toString(self, succinct=True):
        assert succinct
        return 'promise:{instNum}'.format(instNum=self.instNum)

    pass

class RacerEdge(DependencyEdge):
    def __init__(self, src, dst):
        super().__init__(src, dst)
        assert type(self.src()) is RacePromise and type(self.dst()) is Promise
        pass

    def draw_get_label(self):
        return 'racer'

    # def draw_get_style(self):
    #     return 'dashed'

    pass

class RacePromise(Promise):
    def __init__(self, *args, **kwargs):
        Promise.__init__(self, *args, **kwargs)

        # prevent mistake of giving the scope of creating myself as my
        # creator scope
        if isinstance(self.creator_scope, CreateRacePromiseScope):
            assert self.creator_scope.promiseInstNum != self.instNum
            pass

        # list of racer promises
        self.racers = []
        pass

    def add_racer(self, promise):
        assert promise not in self.racers
        self.racers.append(promise)
        RacerEdge(src=self, dst=promise)
        pass

    def _promise_get_type(self):
        return 'race'

    pass

class CreatePromiseActionEdge(SchedulingEdgeWithTs):
    def __init__(self, src, dst, ts):
        assert isinstance(dst, Promise)
        super().__init__(src, dst, ts=ts)
        pass

    def _draw_get_label_action(self):
        return 'creat'

    pass

class CreatePromiseScope(ImportantExecutionScope):
    def __init__(self, *args, promiseInstNum, createdWithOobExecutor, **kwargs):
        super().__init__(*args, **kwargs)
        self.promiseInstNum = promiseInstNum
        self.createdWithOobExecutor = createdWithOobExecutor
        pass

    pass

class ExecutePromiseScope(ImportantExecutionScope):
    def __init__(self, *args, promiseInstNum, **kwargs):
        super().__init__(*args, **kwargs)

        self.promiseInstNum = promiseInstNum
        pass

    def toString(self, succinct=True):
        return 'exeScope: prom:{instNum}'.format(instNum=self.promiseInstNum)

    def _draw_get_scope_short_name(self):
        return 'exec'

    pass

class FontFaceSetLoadPromiseExecutorScope(ExecutePromiseScope):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass
    pass

class CreateRacePromiseScope(UnimportantExecutionScope):
    def __init__(self, *args, promiseInstNum, **kwargs):
        super().__init__(*args, **kwargs)
        self.promiseInstNum = promiseInstNum
        pass
    pass

class RunMicrotaskScope(ImportantExecutionScope):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    def _draw_get_scope_short_name(self):
        return 'RunUTask'

    pass

# class Microtask(DotDrawingNodeEntity, DependencyEntity):
class Microtask(ABC):
    # v8/webkit doesn't really have a microtask structure; microtasks
    # are just tasks, which can be given as a js function or a js
    # object, etc. we modified v8/webkit to identify dommicrotask and
    # promisemicrotask, which have separate ID spaces, etc.
    #
    # so, in here, we'll make this Microtask class abstract, and will
    # have PromiseMicrotask and DOMMicrotask child classes
    @abstractmethod
    def __init__(self, schedule_ts, scheduler_scope):
        # DependencyEntity.__init__(self)
        # DotDrawingNodeEntity.__init__(self)

        self._schedule_ts = schedule_ts
        # the scope during which the microtask is enqueued/scheduled
        self._scheduler_scope = scheduler_scope

        # SchedulingEdgeWithTs(src=scheduler_scope, dst=self, ts=schedule_ts)
        pass

    def schedule_ts(self):
        return self._schedule_ts

    def scheduler_scope(self):
        return self._scheduler_scope

    pass

class PromiseMicrotask(Microtask):
    def __init__(self, *args, taskID, promiseInstNum, **kwargs):
        super().__init__(*args, **kwargs)
        self._taskID = taskID
        self._promiseInstNum = promiseInstNum
        pass

    def taskID(self):
        return self._taskID

    def promiseInstNum(self):
        return self._promiseInstNum

    def toString(self, succinct=True):
        return 'promiseMicrotask:{taskID}'.format(taskID=self._taskID)

    # # draw api

    # def draw_get_node_name(self):
    #     return 'promise_microtask_{taskID}'.format(taskID=self._taskID)

    # def draw_get_fillcolor(self):
    #     return 'lightslateblue'

    # def draw_get_style(self):
    #     return 'filled'

    # def draw_get_shape(self):
    #     return 'octagon'

    # def draw_get_label(self):
    #     return '#{taskID}'.format(taskID=self._taskID)

    pass

class DOMMicrotask(Microtask):
    def __init__(self, *args, instNum, **kwargs):
        super().__init__(*args, **kwargs)
        self.instNum = instNum

        SchedulingEdgeWithTs(src=self.scheduler_scope(), dst=self, ts=self.schedule_ts())
        pass

    def instNum(self):
        return self.instNum

    def toString(self, succinct=True):
        return 'DomMicrotask#{instNum}'.format(instNum=self.instNum)

    # # draw api

    # def draw_get_node_name(self):
    #     return 'dom_microtask_{instNum}'.format(instNum=self.instNum)

    # def draw_get_fillcolor(self):
    #     return 'lightgreen'

    # def draw_get_style(self):
    #     return 'filled'

    # def draw_get_shape(self):
    #     return 'octagon'

    # def draw_get_label(self):
    #     return '#{instNum}'.format(instNum=self.instNum)


    pass

class HTMLScriptRunner(object):
    def __init__(self):
        self._scriptNestingLevel = 0
        pass

    def scriptNestingLevel(self):
        return self._scriptNestingLevel

    def inc_scriptNestingLevel(self):
        self._scriptNestingLevel += 1
        pass

    def dec_scriptNestingLevel(self):
        self._scriptNestingLevel -= 1
        assert self._scriptNestingLevel >= 0
        pass

    pass

class HTMLDocumentParser(object):
    def __init__(self, instNum, isParsingFragment):
        self._instNum = instNum
        self._isParsingFragment = isParsingFragment
        self._fragmentInstNum = None
        pass

    @property
    def instNum(self):
        return self._instNum

    @property
    def isParsingFragment(self):
        return self._isParsingFragment

    @property
    def fragmentInstNum(self):
        assert self._isParsingFragment
        return self._fragmentInstNum

    @fragmentInstNum.setter
    def fragmentInstNum(self, value):
        assert self._isParsingFragment, 'this parser is not parsing a fragment'
        assert self._fragmentInstNum is None, \
          'fragment inst num already set (to {})'.format(self._fragmentInstNum)
        assert type(value) is int
        self._fragmentInstNum = value
        pass

    pass

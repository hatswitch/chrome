from abc import ABC, abstractmethod
import io
import logging

import common

class DotDrawing(ABC):
    # just for some static vars

    show_timing = False
    show_elem_end_byte_offset = False
    hide_edge_labels = False

    @abstractmethod
    def __init__(self):
        pass

    pass

class DotDrawingEntity(DotDrawing):
    class ScopeVerbosity(common.OrderedEnum):
        MINIMAL = 1
        NO_INNER = 2
        NORMAL = 3
        HIGH = 5
        pass

    @abstractmethod
    def __init__(self):
        self._is_visited = False
        pass

    def draw_is_visited(self):
        return self._is_visited

    # because of DOT ordering requirements (e.g., end points of an
    # edge generally should already be defined) and our desire to draw
    # nodes outside of scopes, we use three separate buffers to write
    # respective drawing data, then at the end will output the three
    # bufs in the "nodes, then scopes, then edges" order
    #
    @abstractmethod
    def draw(self, nodes_specs, edges_specs,
             indentAmount, scope_verbosity=ScopeVerbosity.NORMAL):
        # scope_verbosity:
        #
        # * 'minimal': to draw only one level of scope, i.e., will not
        # draw any *inner* scope.
        #
        # * 'normal': if a scope doesn't have any out edges or any
        # * inner scope, then it will not be drawn
        #
        # * 'high': would force drawing in cases where it would be
        # better not to draw (e.g., scopes that do not schedule/fetch
        # anything, and thus the only information is the run time)
        raise NotImplementedError

    @abstractmethod
    def draw_get_node_name(self):
        raise NotImplementedError

    def draw_get_shape(self):
        return ''

    def draw_get_label(self):
        return ''

    def draw_get_color(self):
        return ''

    def draw_get_fillcolor(self):
        return ''

    def draw_get_style(self):
        return ''

    def draw(self, nodes_specs, edges_specs,
             indentAmount, scope_verbosity=ScopeVerbosity.NORMAL):
        assert not self._is_visited
        self._is_visited = True

        indent = ' '*indentAmount

        logging.debug(indent + 'begin drawing {}'.format(self.toString()))

        # first, draw our node
        mynodename = self.draw_get_node_name()
        mynodeattrs = {
            'label': self.draw_get_label(),
            'color': self.draw_get_color(),
            'fillcolor': self.draw_get_fillcolor(),
            'style': self.draw_get_style(),
            'shape': self.draw_get_shape(),
            }
        assert mynodename not in nodes_specs
        nodes_specs[mynodename] = {
            'name': mynodename,
            'attrs': mynodeattrs,
            }

        # now draw our out edges
        self._draw_out_edges(
            nodes_specs=nodes_specs, edges_specs=edges_specs,
            src_node_name=mynodename, indentAmount=indentAmount,
            out_edges=self.out_edges(), scope_verbosity=scope_verbosity)

        logging.debug(indent + 'done drawing {}'.format(self))
        logging.debug(indent + 'returning true')

        return True

    def _draw_out_edges(self, nodes_specs, edges_specs,
                        src_node_name, out_edges, indentAmount, scope_verbosity):
        # recursively draw edges destinations and then edges

        indent = ' '*indentAmount
        did_draw = False
        for edge in out_edges:
            logging.debug(indent + 'attempt to draw edge... {edge}'.format(edge=edge))
            dst = edge.dst()
            if dst.draw_is_visited() or dst.draw(
                    nodes_specs=nodes_specs, edges_specs=edges_specs,
                    indentAmount=indentAmount+4, scope_verbosity=scope_verbosity):
                logging.debug(indent + 'edge dst did draw (or already visited)')
                dstnodename = dst.draw_get_node_name()
                edgelabel = edge.draw_get_label() if not DotDrawing.hide_edge_labels else ''
                edgestyle = edge.draw_get_style()
                edgecolor = dst.draw_get_fillcolor() # borrow the color of the dst
                if not edgecolor or edgecolor == 'none': edgecolor = 'black'
                lhead = '{}'.format(dst.draw_get_subgraph_name()) if isinstance(dst, DotDrawingScopeEntity) else ''
                ltail = '{}'.format(self.draw_get_subgraph_name()) if isinstance(self, DotDrawingScopeEntity) else ''

                # now we can no longer use "(scopeName, elemName)"
                # pairs as unique edge names, because a scope can have
                # multiple edges to the same element, e.g., if the
                # scope makes multiple changes to the element's src
                # attribute, each such change resulting in a
                # SetResourceReferenceEdge
                assert (src_node_name, dstnodename) not in edges_specs, "{} -> {}".format(src_node_name, dstnodename)
                edges_specs[(src_node_name, dstnodename, edge.edge_id())] = {
                    'attrs': {
                        'label': edgelabel,
                        'style': edgestyle,
                        'lhead': lhead,
                        'ltail': ltail,
                        'color': edgecolor,
                        }
                    }

                did_draw = True
                pass
            else:
                logging.debug(indent + 'did NOT draw out edge {edge}'.format(edge=edge))
                pass
            pass
        return did_draw

    pass

class DotDrawingNodeEntity(DotDrawingEntity):
    @abstractmethod
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    pass

# a scope is a way to group multiple related nodes; in our
# application, it will be used for representing ExecutionScope's
class DotDrawingScopeEntity(DotDrawingEntity):
    @abstractmethod
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    @abstractmethod
    def draw_get_subgraph_name(self):
        pass

    pass

class DotDrawingEdge(DotDrawing):
    # this id is created/maintained here in python code, not parsed
    # from log
    next_edge_id = 0

    @abstractmethod
    def __init__(self):
        self._edge_id = DotDrawingEdge.next_edge_id
        DotDrawingEdge.next_edge_id += 1
        pass

    def edge_id(self):
        return self._edge_id

    def draw_get_label(self):
        return ''

    def draw_get_style(self):
        return ''

    def draw_get_arrowhead(self):
        return ''

    def draw_get_arrowtail(self):
        return ''
        
    pass

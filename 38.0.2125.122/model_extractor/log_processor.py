import re
import logging

import common

# inspired by http://stackoverflow.com/a/6635029
class LogProcessor(object):
    def __init__(self):
        self.handlers = [] # List of pairs (regexp, handler)
        self.handler_func_names = set()

    def register(self, regexp, fileName=None, functionName=None):
        """Declare a function as handler for a regular expression.
        
        optionally, fileName and functionName will act as pre-filters,
        i.e., if specified, they have to match with the corresponding
        values from the log event before the regex pattern is matched
        against the content of the log message

        """
        def gethandler(func):
            self.handlers.append((re.compile(regexp), fileName, functionName, func))
            # print (func.__name__)
            assert func.__name__ not in self.handler_func_names
            self.handler_func_names.add(func.__name__)
            return func
        return gethandler

    def _handle_log_event(self, log_event_info, msg):
        for regex, fileName, functionName, handler in self.handlers:
            if fileName != None and fileName != log_event_info.fileName:
                continue
            if functionName != None and functionName != log_event_info.functionName:
                continue
            match = regex.search(msg)
            if match:
                handler(log_event_info, **match.groupdict())
                pass
            pass
        pass

    def process(self, fileobj):
        """Process a file line by line and execute all handlers by registered regular expressions"""

        @common.static_vars(seqNo=0)
        def get_next_webkit_log_event_No():
            retval = get_next_webkit_log_event_No.seqNo
            get_next_webkit_log_event_No.seqNo += 1
            return retval

        @common.static_vars(seqNo=0)
        def get_next_chrome_log_event_No():
            retval = get_next_chrome_log_event_No.seqNo
            get_next_chrome_log_event_No.seqNo += 1
            return retval

        logLineNumber = 0
        for logLine in fileobj:
            logLineNumber += 1
            logLine = logLine.strip()
            if not logLine:
                # empty line
                continue

            log_event_info, msg = common.getWebkitLogEventMetaInfo(logLine)
            if log_event_info:
                logging.debug('line {lineNumber: >5}: [{line}]'.format(
                    lineNumber=logLineNumber, line=logLine))
                # for now log event No is same as array index
                log_event_info.log_event_No = get_next_webkit_log_event_No()
                self._handle_log_event(log_event_info, msg)
                continue

            log_event_info, msg = common.getChromeLogEventMetaInfo(logLine)
            if log_event_info:
                logging.debug('line {lineNumber: >5}: [{line}]'.format(
                    lineNumber=logLineNumber, line=logLine))

                # for now log event No is same as array index
                log_event_info.log_event_No = get_next_chrome_log_event_No()
                self._handle_log_event(log_event_info, msg)
                continue

            pass
        return

    pass

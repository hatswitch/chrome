## not to be run directly. use main.py

import sys
import json
import random
import pdb
import bz2

# 1. from command line, take a json page model file that is generated
# by `main.py extract-model`, and a lis of (host)names that are
# available for use ---> have set of NEW_HOSTS
#
# 2. go through all the requests, collect all the hostnames used by
# requests that are made to download resources (WILL NOT use the
# "orig_used_hostnames" field, because this model might have gone
# through multiple transformations) ----> have set of CURRENT_HOSTS
#
# 3. if len(NEW_HOSTS) >= len(CURRENT_HOSTS), then each current host
# will map to a unique randomly selected new host. otherwise, some
# current hosts will have to share new hosts
#
# 4. with the randomly selected mapping from step 3, we update all the
# requests and write to new json
#
#
# example:
#
# "resources": {
#   "1": {
#     "req_chain": [
#       {
#         "host": "www.cnn.com",
#         "method": "GET",
#         ...
#       }
#     ],
#   },
#   "2": {
#     "req_chain": [
#       {
#         "host": "www.i.cdn.cnn.com",
#         "method": "GET",
#         ...
#       }
#     ],
#     "type": "css"
#   },


def try_to_convert_key_to_int(obj):
    # using this because extract model use integers for keys (e.g.,
    # scope id and resinstnum are ints), but json.dump() has to
    # convert them to strings, and the output json contains the keys
    # sorted by numerical values, i.e., "1" preceeds "2" preceeds "10"
    #
    # if we didn't use this function, then OUR output would be sorted
    # by string values, so "1" would preceed "10" would preceed "2",
    # making it impossible to do a "diff -u" to compare the two models
    # to verify our hostname changes
    #
    newobj = {}
    for k, v in obj.items():
        try:
            int_k = int(k)
            newobj[int_k] = v
            pass
        except:
            newobj[k] = v
            pass
        pass

    return newobj

def change_hostnames(*, infile, outfile, newhostnames):

    openfunc = bz2.open if infile.endswith('.bz2') else open
    with openfunc(infile, 'rt') as fp:
        model = json.load(fp,
                          object_hook=try_to_convert_key_to_int)
        pass

    NEW_HOSTS = sorted(list(set(newhostnames)))
    assert len(NEW_HOSTS) >= 1

    CURRENT_HOSTS = set()

    for resource in model['resources'].values():
        for req in resource['req_chain']:
            CURRENT_HOSTS.add(req['host'])
            pass
        pass

    # seed based on sorted list of ORIGINAL hostnames
    orig_used_hostnames = set(model['orig_used_hostnames'])

    import hashlib
    md5 = hashlib.md5(str(tuple(sorted(orig_used_hostnames))).encode('utf-8'))
    # can't use the tuple directly as the seed because that would use
    # its __hash__() as seed, and python does not guarantee the
    # __hash__() value would be consistent across runs
    random.seed(md5.digest())

    print('number current hosts: {}\nnum new hosts: {}'.format(
        len(CURRENT_HOSTS), len(NEW_HOSTS)))

    if len(NEW_HOSTS) >= len(CURRENT_HOSTS):
        # we can sample the new hosts without replacement
        selected_new_hosts = random.sample(NEW_HOSTS, len(CURRENT_HOSTS))

        # must be all unique
        assert len(selected_new_hosts) == len(set(selected_new_hosts))
        pass
    else:
        # shuffle new hosts
        random.shuffle(NEW_HOSTS)
        # select one by one in sequence
        selected_new_hosts = []
        for i in range(len(CURRENT_HOSTS)):
            selected_new_hosts.append(NEW_HOSTS[i % len(NEW_HOSTS)])
            pass

        # must use all of new hosts
        assert len(set(selected_new_hosts)) == len(set(NEW_HOSTS))
        pass

    assert len(selected_new_hosts) == len(CURRENT_HOSTS)

    print('\nmapping will be:')

    CURRENT_HOSTS = sorted(list(CURRENT_HOSTS))
    mapping = {}
    for i in range(len(CURRENT_HOSTS)):
        current = CURRENT_HOSTS[i]
        new = selected_new_hosts[i]
        print('  {current}     ->    {new}'.format(current=current, new=new))
        mapping[current] = new
        pass

    # now update the json model

    for resource in model['resources'].values():
        for req in resource['req_chain']:
            current = req['host']
            new = mapping[current]
            req['host'] = new
            pass
        pass

    # now write new model to file

    with open(outfile, 'w') as fp:
        json.dump(model, fp, indent=2, sort_keys=True)
        pass

    pass

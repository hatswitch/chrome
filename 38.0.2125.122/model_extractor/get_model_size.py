#!/usr/bin/python3

# just output the total upload and download size of the given json
# page model

import json
import sys

model_fpath = sys.argv[1]

with open(model_fpath) as fp:
    model = json.load(fp)

resources = model['resources']

upbytes = 0
downbytes = 0

for resource in resources.values():
    for req in resource['req_chain']:
        upbytes += req['req_total_size']
        downbytes += req['resp_body_size'] + req['resp_meta_size']

print("upbytes= {}".format(upbytes))
print("downbytes= {}".format(downbytes))

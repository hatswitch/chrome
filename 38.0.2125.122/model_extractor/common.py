import re

# "t=1, ts=1459804774848.31, LocalDOMWindow.cpp :1665, dispatchEvent(): ___ "
webkitLogLinePrefixPattern = re.compile(r'^t=(?P<tid>\d+), ts=[ ]*(?P<ts>\d+\.\d{1,2}), (?P<fileName>.+) :(?P<lineNumber>\d+), (?P<functionName>.+): ___ ')

# "[ts=1459804774637.26 fooo.cc :156] ~bar(): ,,,, "
chromeLogLinePrefixPattern = re.compile(r'^\[ts=[ ]*(?P<ts>\d+\.\d{1,2}) (?P<fileName>.+) :(?P<lineNumber>\d+)\] (?P<functionName>.+): ,,,, ')


class WebkitLogEventMetaInfo:
    def __init__(self, tid, ts, fileName, lineNumber, functionName):
        '''NOTE: fileName, lineNumber, and functionName are the location of the
        logging call
        '''
        self.tid = tid
        self.ts = ts
        self.fileName = fileName
        self.lineNumber = lineNumber
        self.functionName = functionName

        # will be filled in later
        self.log_event_No = None
        pass

    def __str__(self):
        return 'ts= {ts}, fileName= {fileName}, lineNumber= {lineNumber}, ' \
          'functionName= {functionName}'.format(
              ts=self.ts, fileName=self.fileName, functionName=self.functionName,
              lineNumber=self.lineNumber)

    pass

class ChromeLogEventMetaInfo:
    def __init__(self, ts, fileName, lineNumber, functionName):
        '''NOTE: fileName, lineNumber, and functionName are the location of the
        logging call
        '''
        self.ts = ts
        self.fileName = fileName
        self.lineNumber = lineNumber
        self.functionName = functionName

        # will be filled in later
        self.log_event_No = None
        pass

    pass


def getWebkitLogEventMetaInfo(line, returnLogMessage=True):
    '''

    "returnLogMessage": whether to return the meat, i.e., message, of
    the log line

    '''
    info = None
    match = re.match(webkitLogLinePrefixPattern, line)
    if match:
        info = WebkitLogEventMetaInfo(tid=int(match.group('tid')),
                                       ts=float(match.group('ts')),
                                       fileName=match.group('fileName'),
                                       lineNumber=int(match.group('lineNumber')),
                                       functionName=match.group('functionName'))
        pass
    if returnLogMessage:
        return info, (line[match.end():] if match else None)
    else:
        return info

def getChromeLogEventMetaInfo(line, returnLogMessage=True):
    '''

    "returnLogMessage": whether to return the meat, i.e., message, of
    the log line

    '''
    info = None
    match = re.match(chromeLogLinePrefixPattern, line)
    if match:
        info = ChromeLogEventMetaInfo(ts=float(match.group('ts')),
                                       fileName=match.group('fileName'),
                                       lineNumber=int(match.group('lineNumber')),
                                       functionName=match.group('functionName'))
        pass
    if returnLogMessage:
        return info, (line[match.end():] if match else None)
    else:
        return info

# copy from
# http://stackoverflow.com/questions/279561/what-is-the-python-equivalent-of-static-variables-inside-a-function
def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
            pass
        return func
    return decorate


import enum
@enum.unique
class AutoNumber(enum.Enum):
    # from https://docs.python.org/3/library/enum.html#autonumber
    def __new__(cls):
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj

    pass

class OrderedEnum(enum.Enum):
    # https://docs.python.org/3/library/enum.html#orderedenum
    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self.value >= other.value
        return NotImplemented
    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self.value > other.value
        return NotImplemented
    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self.value <= other.value
        return NotImplemented
    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented

    pass

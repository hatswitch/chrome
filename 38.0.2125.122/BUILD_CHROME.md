this page contains instructions collected from across multiple pages to check out the src and build `chromimum` on ubuntu 14.04

## create a directory for the source

```
cd
mkdir chromium-src
cd chromium-src
```

## obtain the depot_tools 
(instructions from https://commondatastorage.googleapis.com/chrome-infra-docs/flat/depot_tools/docs/html/depot_tools_tutorial.html#_setting_up)
```
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git

# add this line to your ~/.bashrc file
export PATH=$PATH:$HOME/chromium-src/depot_tools
```

## check out the chromium source code
(instructions from https://www.chromium.org/developers/how-tos/get-the-code)

```
fetch --no-history chromium    # this can take 10-20 minutes on univ connection

# go into src dir: the root of the chromium src; everything will be
# done inside here

cd src

# install build dependencies

sudo bash ./build/install-build-deps.sh
```

## checkout the 38.0.2125.122 release
(instructions from https://www.chromium.org/developers/how-tos/get-the-code/working-with-release-branches)

we're going with 38.0.2125.122 because that's what my modified chrome is based off

```
git fetch --tags   # can take a while, e.g., 20-30 minutes
git checkout -b mybranch-38.0.2125.122 tags/38.0.2125.122
```

normally we should just need to run `gclient sync --with_branch_heads`, but our branch is pretty old, so things have kind of gone out of whack a little. still luckily we can still make it work (as of sept 29, '16):

(if you run into any issue with `gclient sync` etc. and want to just try again from clean slate, it seems sufficient to just switch back to master i.e., `git checkout master`, delete our branch, and check it out again i.e., `git checkout -b mybranch-38.0.2125.122 tags/38.0.2125.122`


**WARNING: the "gclient sync" step can be a frustrating trial-and-error process... **

```
# remove a few problematic dirs, which gclient sync MIGHT or
# MIGHT NOT restore --- some of these might no longer be needed,
# e.g., either gclient sync itself, or gyp, will say something
# like "WARNING: <dir> is no longer part of this client.  It is
# recommended that you manually remove it" where <dir> is one
# of the third-party dirs

# typically when gclient sync errors on a dir, we see that the
# dir contains an odd repository where `git log` shows only one
# commit (typically a very recent one). after we remove the dir,
# rerun `gclient sync`, then we will see the dir show the right
# `git log`, with multiple commits, with the top one being
# something in 2014 or 2015

export DEPOT_TOOLS_UPDATE=0 # see the note below to see why we need this

rm -rf buildtools \
   testing/gmock \
   third_party/flac \
   third_party/WebKit/LayoutTests/w3c/web-platform-tests \
   third_party/WebKit/LayoutTests/w3c/csswg-test \
   third_party/hunspell_dictionaries \
   third_party/libjpeg_turbo \
   third_party/libsrtp \
   third_party/mesa/src \
   third_party/libaddressinput/src

# you might also need to remove the following dirs if `gclient sync`
# complains about them. DON'T remove them unless `gclient sync`
# complains; i'd gone ahead and just removed them pre-emptively before
# running `gclient sync`, and `gclient sync` would fail when it attempted
# to rebase `src` to 480fc5d14778f9987917d73040c8b5f3604eaace (though
# `src` is ALREADY at that commit, so it should be smart and not
# attempt to rebase at all)
   third_party/openmax_dl \
   third_party/webpagereplay \
   third_party/yasm/source/patched-yasm \
   tools/gyp \
   third_party/swiftshader

# (third_party/swiftshader can cause BUILD error later on if
# not correctly sync'ed)

# need to specify a few specific revisions (* see below)

gclient sync --with_branch_heads \
    --revision src@480fc5d14778f9987917d73040c8b5f3604eaace \
    --revision src/third_party/leveldatabase/src@e353fbc7ea81f12a5694991b708f8f45343594b1 \
    --revision src/third_party/libaddressinput/src@1218e871d3a5a48d2dd2271012bb6fb0afadc10e \
    --revision src/third_party/trace-viewer@234a216e7679e78fb490c5711a022b39e767aae9 \
    --revision src/third_party/WebKit/LayoutTests/w3c/csswg-test@44d6dfb50e58bb79f8615540ccc403adca9bd4b2 \
    --revision src/third_party/WebKit/LayoutTests/w3c/web-platform-tests@118f4bab2a877f9a836944eea90d2a059768cc5d

```

oct 18, 2016: i start getting error at `for d in self.custom_deps:` in `depot_tools/gclient.py` (line 611): the exception is something like `NameError: name 'File' is not defined` when it's dealing with `third_party/libyuv/`:
```
src/third_party/libyuv (ERROR)
----------------------------------------
[0:00:07] Started.
[0:00:07]

________ running 'git -c core.deltaBaseCacheLimit=2g clone --no-checkout --progress https://chromium.googlesource.com/external/libyuv.git /home/crawleruser/chromium-src/src/third_party/_gclient_libyuv_VgyD8c' in '/home/crawleruser/chromium-src'
[0:00:07] Cloning into '/home/crawleruser/chromium-src/src/third_party/_gclient_libyuv_VgyD8c'...
[0:00:08] Receiving objects:   0% (1/12622)
[0:00:08] Receiving objects:  10% (1263/12622)
[0:00:09] remote: Total 12622 (delta 8016), reused 12622 (delta 8016)
[0:00:09] Receiving objects: 100% (12622/12622), 5.23 MiB | 10.43 MiB/s
[0:00:09] Resolving deltas:   0% (0/8016)
[0:00:09] Resolving deltas:  10% (818/8016)
[0:00:09] Resolving deltas: 100% (8016/8016)
[0:00:09] Checking connectivity... done.
[0:00:10] From https://chromium.googlesource.com/external/libyuv
[0:00:10]  * [new ref]         refs/branch-heads/m32 -> branch-heads/m32
[0:00:10]  * [new ref]         refs/branch-heads/m34 -> branch-heads/m34
[0:00:10]  * [new ref]         refs/branch-heads/m39 -> branch-heads/m39
[0:00:10] Checked out 455c66b4375d72984b79249616d0a708ad568894 to a detached HEAD. Before making any commits
in this repo, you should use 'git checkout <branch>' to switch to
an existing branch or use 'git checkout origin -b <branch>' to
create a new branch for your work.
[0:00:10]
----------------------------------------
Traceback (most recent call last):
  File "/home/crawleruser/chromium-src/depot_tools/gclient.py", line 2184, in <module>

  File "/home/crawleruser/chromium-src/depot_tools/gclient.py", line 2170, in main

  File "/home/crawleruser/chromium-src/depot_tools/subcommand.py", line 252, in execute

  File "/home/crawleruser/chromium-src/depot_tools/gclient.py", line 1932, in CMDsync
    parser.add_option('-a', '--actual', action='store_true',
  File "/home/crawleruser/chromium-src/depot_tools/gclient.py", line 1395, in RunOnDeps
    else:
  File "/home/crawleruser/chromium-src/depot_tools/gclient_utils.py", line 1037, in run
    editor = os.environ.get('VISUAL')
  File "/home/crawleruser/chromium-src/depot_tools/gclient.py", line 795, in run
    return '%s%s' % (branch, modified_path)
  File "/home/crawleruser/chromium-src/depot_tools/gclient.py", line 611, in ParseDepsFile
    for d in self.custom_deps:
  File "<string>", line 24, in <module>
NameError: name 'File' is not defined
```

problem is `gclient` automatically updates `gclient.py` which has removed support for `File()` in the DEPS file, so, in short do `export DEPOT_TOOLS_UPDATE=0` before running `gclient`
(https://groups.google.com/a/chromium.org/forum/#!searchin/chromium-dev/1.5$20year$20old/chromium-dev/yfOLAnk5YAk/-Ump4CNnAwAJ)


### (*) how to find out which revision to force `gclient sync` to use?

in `chromium-src/.gclient_entries` you will find something like this:

```
entries = {
  'src': 'https://chromium.googlesource.com/chromium/src.git',
  'src/breakpad/src': 'https://chromium.googlesource.com/breakpad/breakpad/src.git@c85196f3c42b2eb8559464892a4b1749620a64d4',
.
.
.
  'src/third_party/libaddressinput/src': 'https://chromium.googlesource.com/external/libaddressinput.git@5eeeb797e79fa01503fcdcbebdc50036fac023ef',
.
.
.
}
```

for example, that means `gclient` wants to checkout revision `5eeeb797e79fa01503fcdcbebdc50036fac023ef` from an external/third-party repository for `src/third_party/libaddressinput/src`. but this was true back in late 2014 for release 38.0.2125.122, and due to reasons unknown to me (perhaps repository migrations), that commit hash `5eeeb797e79fa01503fcdcbebdc50036fac023ef` no longer exists in that repository. so, what i do is look in my working checkout, and look for the commit message associated with that commit:
```
commit 5e0f1290b83e59c0b3b0b3d23c060f1b134a1364
Author: rouslan@chromium.org <rouslan@chromium.org@38ededc0-08b8-5190-f2ac-b31f878777ad>
Date:   Sun Aug 10 00:05:08 2014 +0000

    Fix signed/unsigned warning when building in Chromium on Windows.
    
    TBR=roubert@google.com
.
.
.
```
and then search for that message in the latest repository, and use the new commit hash associated with that message when running `gclient sync` with the `--revision` option.

do the same for other repositories that run into errors.

these commit messages are in `misc/working-third-party-commit-messages.txt`


## apply our patches
we have patches to chrome, v8, and webkit.

```
export OUR_PATCHES_DIR=/path/to/checkout_of_this_repo/38.0.2125.122/patches
```

   * in the root `src` dir (i.e., `chromium-src/src`):
```
git am $OUR_PATCHES_DIR/chrome/*.patch
```

   * in the `src/v8` dir:
```
git am $OUR_PATCHES_DIR/v8/*.patch
```

   * in the `src/third_party/WebKit` dir:
```
git am $OUR_PATCHES_DIR/WebKit/*.patch
```

## set up and configure the build
```
# on ubuntu 14.04.5 i see this command complain about missing libexif.
# i fixed this by just `apt-get install libexif-dev`

gclient runhooks




# build a static chrome binary, probably go for this option if will use chrome inside docker
./build/gyp_chromium


# for local development, can try this instead:
# the "-D" options try to reduce build time, but if run into issues,
# rerun this command without them. see other config params at
# https://www.chromium.org/developers/gyp-environment-variables

./build/gyp_chromium -Dcomponent=shared_library -Dremove_webcore_debug_symbols=1 -Dfastbuild=1
```

## build and run the programs

for release build of `chrome` (first build can take an hour)
```
# build it. if see error with `ssl3con.c`, see the note at bottom
ninja -C out/Release chrome chrome_sandbox

# install the chrome sandbox (from https://chromium.googlesource.com/chromium/src/+/master/docs/linux_suid_sandbox_development.md)
sudo cp out/Release/chrome_sandbox /usr/local/sbin/chrome-devel-sandbox
sudo chown root:root /usr/local/sbin/chrome-devel-sandbox
sudo chmod 4755 /usr/local/sbin/chrome-devel-sandbox

# put in .bashrc:
export CHROME_DEVEL_SANDBOX=/usr/local/sbin/chrome-devel-sandbox

# run it
./out/Release/chrome
```

if inside docker, i guess by default it does not have X server installed. then we need `xvfb`:

`sudo apt-get install Xvfb`

then can run the basic chrome with

`xvfb-run ./out/Release/chrome http://illinois.edu`

for a run to extract page model, run this (kill after, say, 10 seconds if run manually. if run as part of our crawler, then enable the chrome-debugging-port accordingly):

```
rm -rf disk-cache-dir user-data-dir

xvfb-run ./out/Release/chrome --incognito --safebrowsing-disable-download-protection \
    --disable-client-side-phishing-detection \
    --disk-cache-dir=disk-cache-dir --user-data-dir=user-data-dir \
    --disable-component-extensions-with-background-pages --disable-default-apps \
    --disable-background-networking --disable-preconnect \
    --enable-logging=stderr \
    --vmodule=http_stream_parser=2,chrome_browser_main_posix=2,render_process_host_impl=2,http_network_transaction=2 \
    --disable-background-parser --extract-page-model --blink-platform-log-channels=PageModeling \
    http://illinois.edu/
```



---
Note: on Ubuntu 14.04(.5) I'm seeing this error:
```
../../net/third_party/nss/ssl/ssl3con.c:2091:16: error: no member named 'pIv' in 'struct CK_NSS_AEAD_PARAMS'
    aeadParams.pIv = (unsigned char *) additionalData;
    ~~~~~~~~~~ ^
../../net/third_party/nss/ssl/ssl3con.c:2092:16: error: no member named 'ulIvLen' in 'struct CK_NSS_AEAD_PARAMS'
    aeadParams.ulIvLen = 8;
    ~~~~~~~~~~ ^

```

CK_NSS_AEAD_PARAMS defined as such so it can be compiled with older nss headers, but it should be defined differently. Not sure why this is yet, but this allows me to build chrome for now.
```
-    aeadParams.pIv = (unsigned char *) additionalData;
-    aeadParams.ulIvLen = 8;
+    aeadParams.pNonce = (unsigned char *) additionalData;
+    aeadParams.ulNonceLen = 8;
```
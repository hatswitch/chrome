From f4b8bf185967558ff619210e53cccf334f952db5 Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Tue, 24 May 2016 19:51:47 -0500
Subject: [PATCH 135/135] some logging changes surrounding script executions

---
 Source/core/html/parser/HTMLDocumentParser.cpp |  5 +-
 Source/core/html/parser/HTMLScriptRunner.cpp   | 76 ++++++++++++++++++++++++--
 Source/core/html/parser/HTMLScriptRunner.h     |  2 +-
 3 files changed, 74 insertions(+), 9 deletions(-)

diff --git a/Source/core/html/parser/HTMLDocumentParser.cpp b/Source/core/html/parser/HTMLDocumentParser.cpp
index b66651f..26d3b57 100644
--- a/Source/core/html/parser/HTMLDocumentParser.cpp
+++ b/Source/core/html/parser/HTMLDocumentParser.cpp
@@ -1224,18 +1224,17 @@ void HTMLDocumentParser::executeScriptsWaitingForResources()
     const double scopeStart = currentTimeMS();
     if (m_isExtractingPageModel) {
         MYWTF_LOG_2_WITH_DOC_AND_PARSER(PageModeling, scopeStart, document(), this,
-                                      "begin calling runner's executeScriptsWaitingForResources()");
+                                      "begin calling scriptrunner executeScriptsWaitingForResources()");
     }
 
     m_scriptRunner->executeScriptsWaitingForResources();
 
     if (m_isExtractingPageModel) {
         MYWTF_LOG_2_WITH_DOC_AND_PARSER(PageModeling, scopeStart, document(), this,
-                                      "done calling runner's executeScriptsWaitingForResources()");
+                                      "done calling scriptrunner executeScriptsWaitingForResources()");
     }
 
     if (!isWaitingForScripts()) {
-        const double scopeStart = currentTimeMS();
         if (m_isExtractingPageModel) {
             MYWTF_LOG_2_WITH_DOC_AND_PARSER(PageModeling, scopeStart, document(), this,
                                           "not waiting for script, so begin resume parsing");
diff --git a/Source/core/html/parser/HTMLScriptRunner.cpp b/Source/core/html/parser/HTMLScriptRunner.cpp
index 2dfedf1..216bb339 100644
--- a/Source/core/html/parser/HTMLScriptRunner.cpp
+++ b/Source/core/html/parser/HTMLScriptRunner.cpp
@@ -121,13 +121,27 @@ ScriptSourceCode HTMLScriptRunner::sourceFromPendingScript(const PendingScript&
     return ScriptSourceCode(script.element()->textContent(), documentURLForScriptExecution(m_document), script.startingPosition());
 }
 
-bool HTMLScriptRunner::isPendingScriptReady(const PendingScript& script)
+bool HTMLScriptRunner::isPendingScriptReady(const PendingScript& script, const bool doLog)
 {
     m_hasScriptsWaitingForResources = !m_document->isScriptExecutionReady();
-    if (m_hasScriptsWaitingForResources)
+    if (m_hasScriptsWaitingForResources) {
+        if (doLog && m_document->isExtractingPageModel()) {
+            MYWTF_LOG_WITH_DOC(PageModeling, m_document.get(),
+                               "nope (script execution is not ready)");
+        }
         return false;
-    if (script.resource() && !script.resource()->isLoaded())
+    }
+    if (script.resource() && !script.resource()->isLoaded()) {
+        if (doLog && m_document->isExtractingPageModel()) {
+            MYWTF_LOG_WITH_DOC(PageModeling, m_document.get(),
+                               "nope (script resource not loaded");
+        }
         return false;
+    }
+    if (doLog && m_document->isExtractingPageModel()) {
+        MYWTF_LOG_WITH_DOC(PageModeling, m_document.get(),
+                           "yup");
+    }
     return true;
 }
 
@@ -254,6 +268,13 @@ void HTMLScriptRunner::execute(PassRefPtrWillBeRawPtr<Element> scriptElement, co
 
     bool hadPreloadScanner = m_host->hasPreloadScanner();
 
+    const double scopeStart = currentTimeMS();
+    const uint32_t elemInstNum = scriptElement->instNum();
+    if (m_document->isExtractingPageModel()) {
+        MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, m_document.get(),
+                             "begin execute elem:%u", elemInstNum);
+    }
+
     // Try to execute the script given to us.
     runScript(scriptElement.get(), scriptStartPosition);
 
@@ -263,7 +284,19 @@ void HTMLScriptRunner::execute(PassRefPtrWillBeRawPtr<Element> scriptElement, co
         // If preload scanner got created, it is missing the source after the current insertion point. Append it and scan.
         if (!hadPreloadScanner && m_host->hasPreloadScanner())
             m_host->appendCurrentInputStreamToPreloadScannerAndScan();
+        if (m_document->isExtractingPageModel()) {
+            MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, m_document.get(),
+                                 "has parser blocking script and not executing script -> begin exec them now");
+        }
         executeParsingBlockingScripts();
+        if (m_document->isExtractingPageModel()) {
+            MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, m_document.get(),
+                                 "has parser blocking script and not executing script -> done exec them now");
+        }
+    }
+    if (m_document->isExtractingPageModel()) {
+        MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, m_document.get(),
+                             "done execute elem:%u", elemInstNum);
     }
 }
 
@@ -274,8 +307,17 @@ bool HTMLScriptRunner::hasParserBlockingScript() const
 
 void HTMLScriptRunner::executeParsingBlockingScripts()
 {
-    while (hasParserBlockingScript() && isPendingScriptReady(m_parserBlockingScript))
+    const double scopeStart = currentTimeMS();
+    if (m_document->isExtractingPageModel()) {
+        MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, m_document.get(),
+                             "executeParsingBlockingScripts begin");
+    }
+    while (hasParserBlockingScript() && isPendingScriptReady(m_parserBlockingScript, true))
         executeParsingBlockingScript();
+    if (m_document->isExtractingPageModel()) {
+        MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, m_document.get(),
+                             "executeParsingBlockingScripts done");
+    }
 }
 
 void HTMLScriptRunner::executeScriptsWaitingForLoad(Resource* resource)
@@ -284,7 +326,18 @@ void HTMLScriptRunner::executeScriptsWaitingForLoad(Resource* resource)
     ASSERT(hasParserBlockingScript());
     ASSERT_UNUSED(resource, m_parserBlockingScript.resource() == resource);
     ASSERT(m_parserBlockingScript.resource()->isLoaded());
+
+    const double scopeStart = currentTimeMS();
+    const uint32_t resInstNum = resource->instNum();
+    if (m_document->isExtractingPageModel()) {
+        MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, m_document.get(),
+                             "executeScriptsWaitingForLoad begin, res:%u", resInstNum);
+    }
     executeParsingBlockingScripts();
+    if (m_document->isExtractingPageModel()) {
+        MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, m_document.get(),
+                             "executeScriptsWaitingForLoad done, res:%u", resInstNum);
+    }
 }
 
 void HTMLScriptRunner::executeScriptsWaitingForResources()
@@ -305,6 +358,12 @@ void HTMLScriptRunner::executeScriptsWaitingForResources()
 
 bool HTMLScriptRunner::executeScriptsWaitingForParsing()
 {
+    const double scopeStart = currentTimeMS();
+    uint32_t numLoops = 0;
+    if (m_document->isExtractingPageModel()) {
+        MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, m_document.get(),
+                             "executeScriptsWaitingForParsing begin");
+    }
     while (!m_scriptsToExecuteAfterParsing.isEmpty()) {
         ASSERT(!isExecutingScript());
         ASSERT(!hasParserBlockingScript());
@@ -317,12 +376,18 @@ bool HTMLScriptRunner::executeScriptsWaitingForParsing()
         }
         PendingScript first = m_scriptsToExecuteAfterParsing.takeFirst();
         executePendingScriptAndDispatchEvent(first, PendingScriptDeferred);
+        ++numLoops;
         // FIXME: What is this m_document check for?
         if (!m_document) {
             ASSERT_NOT_REACHED();
             return false;
         }
     }
+    if (m_document->isExtractingPageModel()) {
+        MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, m_document.get(),
+                             "executeScriptsWaitingForParsing done");
+        ASSERT(numLoops == 0);
+    }
     return true;
 }
 
@@ -439,12 +504,13 @@ void HTMLScriptRunner::runScript(Element* script, const TextPosition& scriptStar
             if (m_scriptNestingLevel == 1) {
                 if (isExtractingPageModel) {
                     MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, m_document.get(),
-                                       "script elem:%u, not executing script because nesting level",
+                                       "script elem:%u, not executing script; instead set it as parserBlockingScript",
                                        elemInstNum);
                 }
                 m_parserBlockingScript.setElement(script);
                 m_parserBlockingScript.setStartingPosition(scriptStartPosition);
             } else {
+                ASSERT(m_scriptNestingLevel > 1);
                 ScriptSourceCode sourceCode(script->textContent(), documentURLForScriptExecution(m_document), scriptStartPosition);
                 if (isExtractingPageModel) {
                     MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, m_document.get(),
diff --git a/Source/core/html/parser/HTMLScriptRunner.h b/Source/core/html/parser/HTMLScriptRunner.h
index 6c1a1f6..b592fbf 100644
--- a/Source/core/html/parser/HTMLScriptRunner.h
+++ b/Source/core/html/parser/HTMLScriptRunner.h
@@ -91,7 +91,7 @@ private:
     // Helpers for dealing with HTMLScriptRunnerHost
     void watchForLoad(PendingScript&);
     void stopWatchingForLoad(PendingScript&);
-    bool isPendingScriptReady(const PendingScript&);
+    bool isPendingScriptReady(const PendingScript&, const bool doLog=false);
     ScriptSourceCode sourceFromPendingScript(const PendingScript&, bool& errorOccurred) const;
 
     RawPtrWillBeMember<Document> m_document;
-- 
2.1.4


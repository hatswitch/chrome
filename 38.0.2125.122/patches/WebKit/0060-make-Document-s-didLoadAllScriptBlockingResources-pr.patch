From 58637258a587d06e1e2f76d24f26a9d93dc49dc6 Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Mon, 22 Feb 2016 11:17:44 -0600
Subject: [PATCH 60/74] make Document's didLoadAllScriptBlockingResources()
 private add isSuppressingScriptExecution() to Document add/update some
 logging in Document

---
 Source/core/dom/Document.cpp | 137 +++++++++++++++++++++++++++++++------------
 Source/core/dom/Document.h   |   7 ++-
 2 files changed, 105 insertions(+), 39 deletions(-)

diff --git a/Source/core/dom/Document.cpp b/Source/core/dom/Document.cpp
index 491e283..2300496 100644
--- a/Source/core/dom/Document.cpp
+++ b/Source/core/dom/Document.cpp
@@ -207,7 +207,7 @@
 #include "platform/Logging.h"
 
 
-#define DO_EXTRACT_PAGE_MODEL
+// #define DO_EXTRACT_PAGE_MODEL
 
 using namespace WTF;
 using namespace Unicode;
@@ -512,6 +512,7 @@ Document::Document(const DocumentInit& initializer, DocumentClassFlags documentC
     , m_hasViewportUnits(false)
     , m_styleRecalcElementCounter(0)
     , m_extractingPageModel(false)
+    , m_suppressingScriptExecution(false)
     , m_loggingDynamicHTMLInsertions(false)
 {
     /* m_extractingPageModel is set to true only if
@@ -559,7 +560,12 @@ Document::Document(const DocumentInit& initializer, DocumentClassFlags documentC
     m_styleEngine = StyleEngine::create(*this);
 
     if (m_extractingPageModel) {
-        MYWTF_LOG(ResourceLoading, "___ document %p: constructor. url: [%s]", this, url().string().ascii().data());
+        MYWTF_LOG_WITH_DOC(PageModeling, this,
+                           "constructor. url: [%s] parentDoc %p ownerDoc %p "
+                           "suppressingScriptExecution %d currentTime %llu ms",
+                           url().string().ascii().data(), initializer.parent(),
+                           initializer.owner(), m_suppressingScriptExecution,
+                           (unsigned long long)currentTimeMS());
     }
 
 #ifndef NDEBUG
@@ -644,7 +650,7 @@ Document::~Document()
     setClient(0);
 
     if (m_extractingPageModel) {
-        MYWTF_LOG(ResourceLoading, "___ document %p: destructor", this);
+        MYWTF_LOG_WITH_DOC(PageModeling, this, "destructor");
     }
 
 #ifdef DO_LOG_DYNAMIC_HTML_INSERTIONS
@@ -655,8 +661,8 @@ Document::~Document()
             const String& written = it->second;
             if (written.length()) {
                 const String& author = it->first;
-                MYWTF_LOG(ResourceLoading, "___ document %p: script [%s] wrote [%s]",
-                          this, author.ascii().data(), written.ascii().data());
+                MYWTF_LOG_WITH_DOC(PageModeling, this, "script [%s] wrote [%s]",
+                                   author.ascii().data(), written.ascii().data());
             }
         }
 
@@ -666,8 +672,8 @@ Document::~Document()
             const String& html = it->second;
             if (html.length()) {
                 const String& author = it->first;
-                MYWTF_LOG(ResourceLoading, "___ document %p: script [%s] setInnerHTML [%s]",
-                          this, author.ascii().data(), html.ascii().data());
+                MYWTF_LOG_WITH_DOC(PageModeling, this, "script [%s] setInnerHTML [%s]",
+                                   author.ascii().data(), html.ascii().data());
             }
         }
     }
@@ -1198,8 +1204,9 @@ PassRefPtrWillBeRawPtr<Element> Document::createElement(const QualifiedName& qNa
         exceptionState.GetScriptNameOrSourceURL(&url);
         if (url.isEmpty()) {
             if (!createdByParser) {
-                MYWTF_LOG(ResourceLoading, "___ element %s created by neither parser nor a script!",
-                          qName.toString().ascii().data());
+                MYWTF_LOG_WITH_DOC(PageModeling, this,
+                                   "FATAL: element %s created by neither parser nor a script!",
+                                   qName.toString().ascii().data());
                 ASSERT(false);
             }
         } else {
@@ -1343,6 +1350,8 @@ KURL Document::baseURI() const
 
 void Document::setContent(const String& content)
 {
+    ASSERT(!m_extractingPageModel);
+
     open();
     // FIXME: This should probably use insert(), but that's (intentionally)
     // not implemented for the XML parser as it's normally synonymous with
@@ -1912,6 +1921,10 @@ void Document::updateRenderTree(StyleRecalcChange change)
     if (inStyleRecalc())
         return;
 
+    if (isExtractingPageModel()) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this, "begin update render tree");
+    }
+
     // Entering here from inside layout or paint would be catastrophic since recalcStyle can
     // tear down the render tree or (unfortunately) run script. Kill the whole renderer if
     // someone managed to get into here from inside layout or paint.
@@ -1971,6 +1984,10 @@ void Document::updateRenderTree(StyleRecalcChange change)
     TRACE_EVENT_END1("blink", "Document::updateRenderTree", "elementCount", m_styleRecalcElementCounter);
     // FIXME(361045): remove InspectorInstrumentation calls once DevTools Timeline migrates to tracing.
     InspectorInstrumentation::didRecalculateStyle(cookie, m_styleRecalcElementCounter);
+
+    if (isExtractingPageModel()) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this, "done update render tree");
+    }
 }
 
 void Document::updateStyle(StyleRecalcChange change)
@@ -1980,6 +1997,10 @@ void Document::updateStyle(StyleRecalcChange change)
     HTMLFrameOwnerElement::UpdateSuspendScope suspendWidgetHierarchyUpdates;
     m_lifecycle.advanceTo(DocumentLifecycle::InStyleRecalc);
 
+    if (isExtractingPageModel()) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this, "begin update style");
+    }
+
     if (styleChangeType() >= SubtreeStyleChange)
         change = Force;
 
@@ -2033,6 +2054,10 @@ void Document::updateStyle(StyleRecalcChange change)
     ASSERT(!childNeedsStyleRecalc());
     ASSERT(inStyleRecalc());
     m_lifecycle.advanceTo(DocumentLifecycle::StyleClean);
+
+    if (isExtractingPageModel()) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this, "done update style");
+    }
 }
 
 void Document::updateRenderTreeForNodeIfNeeded(Node* node)
@@ -2197,16 +2222,12 @@ void Document::setIsRunningScript(bool isRunningScript)
         // crash if more than 1 current script so we can take a look.
         ASSERT(m_isRunningScriptCount == 1);
     }
-    if (m_extractingPageModel) {
-        MYWTF_LOG(ResourceLoading, "___ document %p new m_isRunningScriptCount= _%d_",
-                  this, m_isRunningScriptCount);
-    }
 
     //#ifdef DO_LOG_DYNAMIC_HTML_INSERTIONS
 #if 0
     if (m_loggingDynamicHTMLInsertions) {
-        MYWTF_LOG(ResourceLoading, "___ document %p: new m_isRunningScriptCount= _%d_",
-                  this, m_isRunningScriptCount);
+        MYWTF_LOG_WITH_DOC(PageModeling, this, "new m_isRunningScriptCount= _%d_",
+                           m_isRunningScriptCount);
 
         if (m_isRunningScriptCount == 0 && m_current_doc_write_strings) {
             const String s = m_current_doc_write_strings->toString();
@@ -2878,6 +2899,14 @@ int Document::elapsedTime() const
 
 void Document::write(const SegmentedString& text, Document* ownerDocument, ExceptionState& exceptionState)
 {
+    if (m_suppressingScriptExecution) {
+        ASSERT_NOT_REACHED();
+    }
+
+    if (m_extractingPageModel) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this, "doc.write() %d bytes", text.length());
+    }
+
     if (importLoader()) {
         exceptionState.throwDOMException(InvalidStateError, "Imported document doesn't support write().");
         return;
@@ -2968,23 +2997,20 @@ void Document::setURL(const KURL& url)
     if (newURL == m_url)
         return;
 
-#if defined(DO_EXTRACT_PAGE_MODEL)
-     // only extract models of html documents with http/s URLs.
-     if (m_url.isEmpty()) {
-         ASSERT(!m_extractingPageModel);
-         m_extractingPageModel = isHTMLDocument() && newURL.protocolIsInHTTPFamily();
-     } else if (m_extractingPageModel) {
-         // we don't support changing document url,
-         ASSERT_NOT_REACHED();
-
-         // make sure the protocol family is not changing
-         // ASSERT(newURL.protocolIsInHTTPFamily());
-         // MYWTF_LOG(ResourceLoading, "___ document %p: changes to new URL: [%s]",
-         //           this, newURL.string().ascii().data());
-     }
-#else
-     m_extractingPageModel = false;
-#endif
+    if (settings() && settings()->extractPageModel()) {
+        // only extract models of html documents with http/s URLs.
+        if (m_url.isEmpty()) {
+            ASSERT(!m_extractingPageModel);
+            m_extractingPageModel = isHTMLDocument() && newURL.protocolIsInHTTPFamily();
+
+            if (m_extractingPageModel) {
+                m_suppressingScriptExecution = settings()->suppressScriptExecution();
+            }
+        } else if (m_extractingPageModel) {
+            // we don't support changing document url,
+            ASSERT_NOT_REACHED();
+        }
+    }
 
     m_url = newURL;
     updateBaseURL();
@@ -3157,6 +3183,10 @@ LocalFrame* Document::findUnsafeParentScrollPropagationBoundary()
 
 void Document::didLoadAllImports()
 {
+    if (m_extractingPageModel) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this,
+                           "done loading all imports");
+    }
     if (!haveStylesheetsLoaded())
         return;
     if (!importLoader())
@@ -3166,6 +3196,10 @@ void Document::didLoadAllImports()
 
 void Document::didRemoveAllPendingStylesheet()
 {
+    if (m_extractingPageModel) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this,
+                           "done loading all stylesheets");
+    }
     styleResolverMayHaveChanged();
 
     // Only imports on master documents can trigger rendering.
@@ -3178,6 +3212,11 @@ void Document::didRemoveAllPendingStylesheet()
 
 void Document::didLoadAllScriptBlockingResources()
 {
+    if (m_extractingPageModel) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this,
+                           "done loading all script blocking resources");
+    }
+
     m_executeScriptsWaitingForResourcesTimer.startOneShot(0, FROM_HERE);
 
     if (frame())
@@ -4598,12 +4637,13 @@ void Document::pushCurrentScript(PassRefPtrWillBeRawPtr<HTMLScriptElement> newCu
     const KURL scriptUrl = newCurrentScript->src();
     m_currentScriptStack.append(newCurrentScript);
     // crash if more than 1 current script so we can take a look.
-    ASSERT(m_currentScriptStack.size() == 1);
+    ASSERT_WITH_MESSAGE(m_currentScriptStack.size() == 1,
+                        "___ FATAL multiple concurrent scripts. we need to take a look");
 
 #ifdef DO_LOG_DYNAMIC_HTML_INSERTIONS
     if (m_loggingDynamicHTMLInsertions) {
-        MYWTF_LOG(ResourceLoading, "___ document %p: script pushed [%s]",
-                  this, scriptUrl.string().ascii().data());
+        MYWTF_LOG_WITH_DOC(PageModeling, this, "script pushed [%s]",
+                           scriptUrl.string().ascii().data());
         m_scriptToDocWrite.append(std::pair<String, String>(scriptUrl.string(), ""));
     }
 #endif
@@ -4617,8 +4657,8 @@ void Document::popCurrentScript()
 #ifdef DO_LOG_DYNAMIC_HTML_INSERTIONS
     const KURL scriptUrl = m_currentScriptStack.last()->src();
     if (m_loggingDynamicHTMLInsertions) {
-        MYWTF_LOG(ResourceLoading, "___ document %p: script popped [%s]",
-                  this, scriptUrl.string().ascii().data());
+        MYWTF_LOG_WITH_DOC(PageModeling, this, "script popped [%s]",
+                           scriptUrl.string().ascii().data());
     }
 #endif
 
@@ -4822,6 +4862,9 @@ PassRefPtrWillBeRawPtr<HTMLCollection> Document::documentNamedItems(const Atomic
 
 void Document::finishedParsing()
 {
+    if (isExtractingPageModel()) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this, "begin");
+    }
     ASSERT(!scriptableDocumentParser() || !m_parser->isParsing());
     ASSERT(!scriptableDocumentParser() || m_readyState != Loading);
     setParsing(false);
@@ -4871,6 +4914,10 @@ void Document::finishedParsing()
 
     if (HTMLImportLoader* import = importLoader())
         import->didFinishParsing();
+
+    if (isExtractingPageModel()) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this, "done");
+    }
 }
 
 void Document::elementDataCacheClearTimerFired(Timer<Document>*)
@@ -5334,11 +5381,27 @@ Element* Document::pointerLockElement() const
     return 0;
 }
 
+void Document::incrementLoadEventDelayCount()
+{
+    ++m_loadEventDelayCount;
+    if (isExtractingPageModel()) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this,
+                           "new loadEventDelayCount: %d",
+                           m_loadEventDelayCount);
+    }
+}
+
 void Document::decrementLoadEventDelayCount()
 {
     ASSERT(m_loadEventDelayCount);
     --m_loadEventDelayCount;
 
+    if (isExtractingPageModel()) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this,
+                           "new loadEventDelayCount: %d",
+                           m_loadEventDelayCount);
+    }
+
     if (!m_loadEventDelayCount)
         checkLoadEventSoon();
 }
diff --git a/Source/core/dom/Document.h b/Source/core/dom/Document.h
index 67d2387..8aae86b 100644
--- a/Source/core/dom/Document.h
+++ b/Source/core/dom/Document.h
@@ -923,7 +923,7 @@ public:
     Element* pointerLockElement() const;
 
     // Used to allow element that loads data without going through a FrameLoader to delay the 'load' event.
-    void incrementLoadEventDelayCount() { ++m_loadEventDelayCount; }
+    void incrementLoadEventDelayCount();
     void decrementLoadEventDelayCount();
     void checkLoadEventSoon();
     bool isDelayingLoadEvent();
@@ -976,7 +976,6 @@ public:
 
     ElementDataCache* elementDataCache() { return m_elementDataCache.get(); }
 
-    void didLoadAllScriptBlockingResources();
     void didRemoveAllPendingStylesheet();
     void clearStyleResolver();
 
@@ -1038,6 +1037,7 @@ public:
     AtomicString convertLocalName(const AtomicString&);
 
     inline bool isExtractingPageModel() const { return m_extractingPageModel; }
+    inline bool isSuppressingScriptExecution() const { return m_suppressingScriptExecution; }
 
     bool isLoggingDynamicHTMLInsertions() const { return m_loggingDynamicHTMLInsertions; }
     void settingInnerHTML(const Element& element, const String& html, const ExceptionState& exceptionState);
@@ -1375,7 +1375,10 @@ private:
 
     int m_styleRecalcElementCounter;
 
+    void didLoadAllScriptBlockingResources();
+
     bool m_extractingPageModel;
+    bool m_suppressingScriptExecution;
 
     String checkScriptSource(const ExceptionState& exceptionState) const;
 
-- 
2.1.4


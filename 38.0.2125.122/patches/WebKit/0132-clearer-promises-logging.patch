From bfb55b7ff83b456bc7941a52ea38f2030d30c147 Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Tue, 17 May 2016 21:26:26 -0500
Subject: [PATCH] clearer promises logging

---
 Source/bindings/core/v8/ScriptPromise.cpp | 12 ++++++++++++
 Source/bindings/core/v8/ScriptPromise.h   |  2 ++
 Source/core/css/FontFaceSet.cpp           | 23 ++++++++++++++++++++---
 3 files changed, 34 insertions(+), 3 deletions(-)

diff --git a/Source/bindings/core/v8/ScriptPromise.cpp b/Source/bindings/core/v8/ScriptPromise.cpp
index 8bc2172..3f80527 100644
--- a/Source/bindings/core/v8/ScriptPromise.cpp
+++ b/Source/bindings/core/v8/ScriptPromise.cpp
@@ -141,6 +141,18 @@ ScriptPromise::ScriptPromise(ScriptState* scriptState, v8::Handle<v8::Value> val
     m_promise = ScriptValue(scriptState, value);
 }
 
+uint32_t ScriptPromise::instNum() const
+{
+    ASSERT(!isUndefinedOrNull() && !isNull());
+    v8::Local<v8::Value> value = m_promise.v8Value();
+    v8::Local<v8::Promise> promise = value.As<v8::Promise>();
+    v8::Local<v8::String> key = v8::String::NewFromUtf8(m_scriptState->isolate(), "instNum");
+    v8::Local<v8::Value> instNumObj = promise->Get(key);
+    ASSERT(instNumObj->IsUint32());
+    const uint32_t instNum = instNumObj->ToUint32()->Value();
+    return instNum;
+}
+
 ScriptPromise ScriptPromise::then(PassOwnPtr<ScriptFunction> onFulfilled, PassOwnPtr<ScriptFunction> onRejected)
 {
     MYWTF_LOG(PageModeling, "begin");
diff --git a/Source/bindings/core/v8/ScriptPromise.h b/Source/bindings/core/v8/ScriptPromise.h
index e1f8d4b..b45b4e5 100644
--- a/Source/bindings/core/v8/ScriptPromise.h
+++ b/Source/bindings/core/v8/ScriptPromise.h
@@ -92,6 +92,8 @@ public:
         return m_promise.isEmpty();
     }
 
+    uint32_t instNum() const;
+
     void clear()
     {
         m_promise.clear();
diff --git a/Source/core/css/FontFaceSet.cpp b/Source/core/css/FontFaceSet.cpp
index ff9364d..9bffb66 100644
--- a/Source/core/css/FontFaceSet.cpp
+++ b/Source/core/css/FontFaceSet.cpp
@@ -270,9 +270,24 @@ void FontFaceSet::fontLoaded(FontFace* fontFace)
 void FontFaceSet::loadError(FontFace* fontFace)
 {
     m_histogram.updateStatus(fontFace);
+
+    const Document* doc = document();
+    const bool isExtractingPageModel = doc && doc->isExtractingPageModel();
+    const double scopeStart = currentTimeMS();
+
+    if (isExtractingPageModel) {
+        MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, doc,
+                             "fontfaceset:%u font face load error begin", instNum());
+    }
+
     if (RuntimeEnabledFeatures::fontLoadEventsEnabled())
         m_failedFonts.append(fontFace);
     removeFromLoadingFonts(fontFace);
+
+    if (isExtractingPageModel) {
+        MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, doc,
+                             "fontfaceset:%u font face load error done", instNum());
+    }
 }
 
 void FontFaceSet::addToLoadingFonts(PassRefPtrWillBeRawPtr<FontFace> fontFace)
@@ -444,7 +459,6 @@ void FontFaceSet::fireDoneEventIfPossible()
         doneEvent = FontFaceSetLoadEvent::createForFontFaces(EventTypeNames::loadingdone, m_loadedFonts);
         m_loadedFonts.clear();
         if (!m_failedFonts.isEmpty()) {
-            ASSERT(!d->isExtractingPageModel());
             errorEvent = FontFaceSetLoadEvent::createForFontFaces(EventTypeNames::loadingerror, m_failedFonts);
             m_failedFonts.clear();
         }
@@ -506,14 +520,17 @@ ScriptPromise FontFaceSet::load(ScriptState* scriptState, const String& fontStri
     }
     ScriptPromise promise = resolver->promise();
     scopeStart = currentTimeMS();
+    const uint32_t promiseInstNum = promise.instNum();
     if (isExtractingPageModel) {
         MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, doc,
-                             "fontfaceset:%u begin loading fonts", instNum());
+                             "fontfaceset:%u: promise:%u: begin calling executor",
+                             instNum(), promiseInstNum);
     }
     resolver->loadFonts(executionContext()); // After this, resolver->promise() may return null.
     if (isExtractingPageModel) {
         MYWTF_LOG_2_WITH_DOC(PageModeling, scopeStart, doc,
-                             "fontfaceset:%u done loading fonts", instNum());
+                             "fontfaceset:%u: promise:%u: done calling executor",
+                             instNum(), promiseInstNum);
     }
     return promise;
 }
-- 
2.1.4


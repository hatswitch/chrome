From 35fad0fa1339df4aaae23e50d4f145507967d640 Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Mon, 22 Feb 2016 17:25:43 -0600
Subject: [PATCH 69/74] some minor change in Element regarding
 recording/logging the element's byte range within html. also log some shadow
 root stuff

---
 Source/core/dom/Element.cpp | 73 ++++++++++++++++++++++++++++++++-------------
 1 file changed, 52 insertions(+), 21 deletions(-)

diff --git a/Source/core/dom/Element.cpp b/Source/core/dom/Element.cpp
index 85f1e02..7784964 100644
--- a/Source/core/dom/Element.cpp
+++ b/Source/core/dom/Element.cpp
@@ -132,6 +132,17 @@ static Attr* findAttrNodeInList(const AttrNodeList& attrNodeList, const Qualifie
     return 0;
 }
 
+static void
+logByteRange(const Element* element, int starting, int ending)
+{
+    const Document& document = element->document();
+    ASSERT(document.isExtractingPageModel());
+    MYWTF_LOG_WITH_DOC(PageModeling, &document,
+                       "element %p tag [%s] byte range [%d, %d]",
+                       element, element->tagName().lower().ascii().data(),
+                       starting, ending);
+}
+
 PassRefPtrWillBeRawPtr<Element> Element::create(const QualifiedName& tagName, Document* document)
 {
     return adoptRefWillBeNoop(new Element(tagName, document, CreateElement));
@@ -919,6 +930,14 @@ void Element::setSynchronizedLazyAttribute(const QualifiedName& name, const Atom
 
 ALWAYS_INLINE void Element::setAttributeInternal(size_t index, const QualifiedName& name, const AtomicString& newValue, SynchronizationOfLazyAttribute inSynchronizationOfLazyAttribute, const DependencyParentInfo* depParent)
 {
+    if (document().isExtractingPageModel() && depParent) {
+        MYWTF_LOG_WITH_DOC(
+            PageModeling, &document(),
+            "element %p setting attr [%s] to [%s], depParent [%s]",
+            this, name.toString().ascii().data(), newValue.ascii().data(),
+            DependencyParentInfo::buildDepParentOutputString(*depParent).data());
+    }
+
     if (newValue.isNull()) {
         if (index != kNotFound)
             removeAttributeInternal(index, inSynchronizationOfLazyAttribute);
@@ -1717,6 +1736,18 @@ PassRefPtrWillBeRawPtr<ShadowRoot> Element::createShadowRoot(ExceptionState& exc
         return nullptr;
     }
 
+    if (document().isExtractingPageModel()) {
+        MYWTF_LOG_WITH_DOC(PageModeling, &document(),
+                           "shadowRoot created on element %p", this);
+        if (!careAboutByteOffsetsWithinHTML()) {
+            /* if we normally don't care about byte offsets for this
+             * type of element, then we log them now because for this
+             * specific one we are curious.
+             */
+            logByteRange(this, m_startingByteOffsetWithinHTML, m_endingByteOffsetWithinHTML);
+        }
+    }
+
     return PassRefPtrWillBeRawPtr<ShadowRoot>(ensureShadow().addShadowRoot(*this, ShadowRoot::AuthorShadowRoot));
 }
 
@@ -1749,6 +1780,19 @@ ShadowRoot& Element::ensureUserAgentShadowRoot()
         return *shadowRoot;
     ShadowRoot& shadowRoot = ensureShadow().addShadowRoot(*this, ShadowRoot::UserAgentShadowRoot);
     didAddUserAgentShadowRoot(shadowRoot);
+
+    if (document().isExtractingPageModel()) {
+        MYWTF_LOG_WITH_DOC(PageModeling, &document(),
+                           "shadowRoot created on element %p", this);
+        if (!careAboutByteOffsetsWithinHTML()) {
+            /* if we normally don't care about byte offsets for this
+             * type of element, then we log them now because for this
+             * specific one we are curious.
+             */
+            logByteRange(this, m_startingByteOffsetWithinHTML, m_endingByteOffsetWithinHTML);
+        }
+    }
+
     return shadowRoot;
 }
 
@@ -3307,24 +3351,9 @@ void Element::trace(Visitor* visitor)
     ContainerNode::trace(visitor);
 }
 
-static void
-logByteRange(const Element* element, int starting, int ending)
-{
-    const Document& document = element->document();
-    ASSERT(document.isExtractingPageModel());
-    MYWTF_LOG_WITH_DOC(PageModeling, &document,
-                       "element %p tag [%s] byte range [%d, %d]",
-                       element, element->tagName().lower().ascii().data(),
-                       starting, ending);
-}
-
 void
 Element::setByteOffsetsWithinHTML(int starting, int ending)
 {
-    if (!careAboutByteOffsetsWithinHTML()) {
-        return;
-    }
-
     // catch multiple sets
     ASSERT_WITH_MESSAGE(m_startingByteOffsetWithinHTML == -1,
            "element %p tag %s", this, tagName().ascii().data());
@@ -3335,23 +3364,25 @@ Element::setByteOffsetsWithinHTML(int starting, int ending)
     ASSERT(ending > starting);
     m_startingByteOffsetWithinHTML = starting;
     m_endingByteOffsetWithinHTML = ending;
-    logByteRange(this, m_startingByteOffsetWithinHTML, m_endingByteOffsetWithinHTML);
+
+    if (careAboutByteOffsetsWithinHTML()) {
+        logByteRange(this, m_startingByteOffsetWithinHTML, m_endingByteOffsetWithinHTML);
+    }
 }
 
 void
 Element::setEndingByteOffsetWithinHTML(int offset)
 {
-    if (!careAboutByteOffsetsWithinHTML()) {
-        return;
-    }
-
     // starting offset must have been set
     ASSERT_WITH_MESSAGE(m_startingByteOffsetWithinHTML >= 0,
                         "element %p tag %s", this, tagName().ascii().data());
     ASSERT_WITH_MESSAGE(offset > m_endingByteOffsetWithinHTML,
                         "element %p tag %s", this, tagName().ascii().data());
     m_endingByteOffsetWithinHTML = offset;
-    logByteRange(this, m_startingByteOffsetWithinHTML, m_endingByteOffsetWithinHTML);
+
+    if (careAboutByteOffsetsWithinHTML()) {
+        logByteRange(this, m_startingByteOffsetWithinHTML, m_endingByteOffsetWithinHTML);
+    }
 }
 
 } // namespace blink
-- 
2.1.4


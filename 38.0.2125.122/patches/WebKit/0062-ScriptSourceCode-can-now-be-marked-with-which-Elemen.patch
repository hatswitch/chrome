From 7122d72f9d0031e1d1accb5377785fa054fa4256 Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Mon, 22 Feb 2016 11:34:15 -0600
Subject: [PATCH 62/74] * ScriptSourceCode can now be marked with which Element
 it might come from. * ScriptLoader does the marking before handing the script
 source code to the ScriptController to execute. it also logs more inside
 ::prepareScript(). * ScriptController logs before and after compiling and
 running the compiled script (including logging the element marked with the
 script source code).

---
 Source/bindings/core/v8/ScriptController.cpp | 34 ++++++++--
 Source/bindings/core/v8/ScriptSourceCode.h   | 11 ++++
 Source/core/dom/ScriptLoader.cpp             | 99 +++++++++++++++++++++++++++-
 3 files changed, 136 insertions(+), 8 deletions(-)

diff --git a/Source/bindings/core/v8/ScriptController.cpp b/Source/bindings/core/v8/ScriptController.cpp
index 86dfac9..c1e4266 100644
--- a/Source/bindings/core/v8/ScriptController.cpp
+++ b/Source/bindings/core/v8/ScriptController.cpp
@@ -187,20 +187,42 @@ v8::Local<v8::Value> ScriptController::executeScriptAndReturnValue(v8::Handle<v8
         v8::TryCatch tryCatch;
         tryCatch.SetVerbose(true);
 
+        Document* document = m_frame->document();
+        ASSERT(document);
+
+        const bool isExtractingPageModel = document->isExtractingPageModel();
+        const Element* element = source.element();
+        ASSERT(element);
+        const String url = source.url().string();
+
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, document,
+                               "begin compiling script element %p url [%s]",
+                               element, url.ascii().data());
+        }
+
         v8::Handle<v8::Script> script = V8ScriptRunner::compileScript(source, m_isolate, corsStatus, v8CacheOptions);
 
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, document,
+                               "done compiling script element %p url [%s]",
+                               element, url.ascii().data());
+        }
+
         // Keep LocalFrame (and therefore ScriptController) alive.
         RefPtr<LocalFrame> protect(m_frame);
-        Document* document = m_frame->document();
-        ASSERT(document);
-        if (document->url().protocolIsInHTTPFamily()) {
-            MYWTF_LOG(ResourceLoading, "___ begin running script [%s]", source.url().string().ascii().data());
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, document,
+                               "begin running script element %p url [%s]",
+                               element, url.ascii().data());
         }
         document->setIsRunningScript(true);
         result = V8ScriptRunner::runCompiledScript(script, m_frame->document(), m_isolate);
         document->setIsRunningScript(false);
-        if (document->url().protocolIsInHTTPFamily()) {
-            MYWTF_LOG(ResourceLoading, "___ done running script [%s]", source.url().string().ascii().data());
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, document,
+                               "done running script element %p url [%s]",
+                               element, url.ascii().data());
         }
         ASSERT(!tryCatch.HasCaught() || result.IsEmpty());
     }
diff --git a/Source/bindings/core/v8/ScriptSourceCode.h b/Source/bindings/core/v8/ScriptSourceCode.h
index 8cd98fb..1c96e32 100644
--- a/Source/bindings/core/v8/ScriptSourceCode.h
+++ b/Source/bindings/core/v8/ScriptSourceCode.h
@@ -38,6 +38,8 @@
 #include "wtf/text/TextPosition.h"
 #include "wtf/text/WTFString.h"
 
+#include "core/dom/Element.h"
+
 namespace blink {
 
 class ScriptSourceCode {
@@ -47,6 +49,7 @@ public:
         , m_resource(0)
         , m_url(url)
         , m_startPosition(startPosition)
+        , m_element(NULL)
     {
         if (!m_url.isEmpty())
             m_url.removeFragmentIdentifier();
@@ -58,6 +61,7 @@ public:
         : m_source(cs->script())
         , m_resource(cs)
         , m_startPosition(TextPosition::minimumPosition())
+        , m_element(NULL)
     {
     }
 
@@ -77,11 +81,18 @@ public:
     int startLine() const { return m_startPosition.m_line.oneBasedInt(); }
     const TextPosition& startPosition() const { return m_startPosition; }
 
+    const Element* element() const { return m_element; }
+    void setElement(const Element* elem) { ASSERT(!m_element); m_element = elem; }
+
 private:
     String m_source;
     ResourcePtr<ScriptResource> m_resource;
     mutable KURL m_url;
     TextPosition m_startPosition;
+
+    // only to help us know which element, if any, this script source
+    // code is from
+    const Element* m_element;
 };
 
 } // namespace blink
diff --git a/Source/core/dom/ScriptLoader.cpp b/Source/core/dom/ScriptLoader.cpp
index a96512a..90e44f5 100644
--- a/Source/core/dom/ScriptLoader.cpp
+++ b/Source/core/dom/ScriptLoader.cpp
@@ -51,6 +51,8 @@
 #include "wtf/text/StringBuilder.h"
 #include "wtf/text/StringHash.h"
 
+#include "platform/Logging.h"
+
 namespace blink {
 
 ScriptLoader::ScriptLoader(Element* element, bool parserInserted, bool alreadyStarted)
@@ -168,8 +170,22 @@ bool ScriptLoader::isScriptTypeSupported(LegacyTypeSupport supportLegacyTypes) c
 // http://dev.w3.org/html5/spec/Overview.html#prepare-a-script
 bool ScriptLoader::prepareScript(const TextPosition& scriptStartPosition, LegacyTypeSupport supportLegacyTypes)
 {
+    const bool isExtractingPageModel = m_element->document().isExtractingPageModel();
+    if (isExtractingPageModel) {
+        MYWTF_LOG_WITH_DOC(PageModeling, &(m_element->document()),
+                           "begin prepare script element %p", m_element);
+    }
+
     if (m_alreadyStarted)
+    {
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, &(m_element->document()),
+                               "already started");
+            MYWTF_LOG_WITH_DOC(PageModeling, &(m_element->document()),
+                               "done");
+        }
         return false;
+    }
 
     ScriptLoaderClient* client = this->client();
 
@@ -182,19 +198,38 @@ bool ScriptLoader::prepareScript(const TextPosition& scriptStartPosition, Legacy
     }
 
     if (wasParserInserted && !client->asyncAttributeValue())
+    {
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, &(m_element->document()),
+                               "forceAsync= 1");
+        }
         m_forceAsync = true;
+    }
 
     // FIXME: HTML5 spec says we should check that all children are either comments or empty text nodes.
     if (!client->hasSourceAttribute() && !m_element->hasChildren())
+    {
+        ASSERT_NOT_REACHED();
         return false;
+    }
 
     if (!m_element->inDocument())
+    {
+        ASSERT_NOT_REACHED();
         return false;
+    }
 
     if (!isScriptTypeSupported(supportLegacyTypes))
+    {
+        ASSERT_NOT_REACHED();
         return false;
+    }
 
     if (wasParserInserted) {
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, &(m_element->document()),
+                               "forceAsync= 0");
+        }
         m_parserInserted = true;
         m_forceAsync = false;
     }
@@ -206,10 +241,16 @@ bool ScriptLoader::prepareScript(const TextPosition& scriptStartPosition, Legacy
     Document* contextDocument = elementDocument.contextDocument().get();
 
     if (!contextDocument || !contextDocument->allowExecutingScripts(m_element))
+    {
+        ASSERT_NOT_REACHED();
         return false;
+    }
 
     if (!isScriptForEventSupported())
+    {
+        ASSERT_NOT_REACHED();
         return false;
+    }
 
     if (!client->charsetAttributeValue().isEmpty())
         m_characterEncoding = client->charsetAttributeValue();
@@ -217,32 +258,79 @@ bool ScriptLoader::prepareScript(const TextPosition& scriptStartPosition, Legacy
         m_characterEncoding = elementDocument.charset();
 
     if (client->hasSourceAttribute()) {
-        if (!fetchScript(client->sourceAttributeValue()))
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument,
+                               "it has source attribute, so go fetch it!");
+        }
+        if (!fetchScript(client->sourceAttributeValue())) {
+            ASSERT_NOT_REACHED();
             return false;
+        }
+    } else {
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument,
+                               "it has no source attribute, so not fetching it");
+        }
     }
 
     if (client->hasSourceAttribute() && client->deferAttributeValue() && m_parserInserted && !client->asyncAttributeValue()) {
         m_willExecuteWhenDocumentFinishedParsing = true;
         m_willBeParserExecuted = true;
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument,
+                               "[willExecuteWhenDocumentFinishedParsing= 1, "
+                               "willBeParserExecuted= 1]"
+                               );
+        }
     } else if (client->hasSourceAttribute() && m_parserInserted && !client->asyncAttributeValue()) {
         m_willBeParserExecuted = true;
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument,
+                               "[willBeParserExecuted= 1]"
+                               );
+        }
     } else if (!client->hasSourceAttribute() && m_parserInserted && !elementDocument.isRenderingReady()) {
         m_willBeParserExecuted = true;
         m_readyToBeParserExecuted = true;
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument,
+                               "[willBeParserExecuted= 1, readyToBeParserExecuted= 1]"
+                               );
+        }
     } else if (client->hasSourceAttribute() && !client->asyncAttributeValue() && !m_forceAsync) {
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument,
+                               "[willExecuteInOrder= 1]"
+                               );
+            MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument,
+                               "queue for in-order execution"
+                               );
+        }
         m_willExecuteInOrder = true;
         contextDocument->scriptRunner()->queueScriptForExecution(this, m_resource, ScriptRunner::IN_ORDER_EXECUTION);
         m_resource->addClient(this);
     } else if (client->hasSourceAttribute()) {
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument,
+                               "queue for async execution"
+                               );
+        }
         contextDocument->scriptRunner()->queueScriptForExecution(this, m_resource, ScriptRunner::ASYNC_EXECUTION);
         m_resource->addClient(this);
     } else {
         // Reset line numbering for nested writes.
         TextPosition position = elementDocument.isInDocumentWrite() ? TextPosition() : scriptStartPosition;
         KURL scriptURL = (!elementDocument.isInDocumentWrite() && m_parserInserted) ? elementDocument.url() : KURL();
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument, "go execute it!");
+        }
         executeScript(ScriptSourceCode(scriptContent(), scriptURL, position));
     }
 
+    if (isExtractingPageModel) {
+        MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument, "done");
+    }
+
     return true;
 }
 
@@ -293,10 +381,15 @@ bool isSVGScriptLoader(Element* element)
     return isSVGScriptElement(*element);
 }
 
-void ScriptLoader::executeScript(const ScriptSourceCode& sourceCode)
+void ScriptLoader::executeScript(const ScriptSourceCode& sourceCodeOrig)
 {
     ASSERT(m_alreadyStarted);
 
+    /* we make a copy so we can setElement() on it. copying seems ok,
+     * e.g., HTMLScriptRunner::sourceFromPendingScript() returns a
+     * copy. */
+    ScriptSourceCode sourceCode = sourceCodeOrig;
+
     if (sourceCode.isEmpty())
         return;
 
@@ -340,6 +433,8 @@ void ScriptLoader::executeScript(const ScriptSourceCode& sourceCode)
     if (!m_isExternalScript || (sourceCode.resource() && sourceCode.resource()->passesAccessControlCheck(m_element->document().securityOrigin())))
         corsCheck = SharableCrossOrigin;
 
+    sourceCode.setElement(m_element);
+
     // Create a script from the script element node, using the script
     // block's source and the script block's type.
     // Note: This is where the script is compiled and actually executed.
-- 
2.1.4


From a3d1990c848061740a841d2e8daa1472f04ff034 Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Thu, 18 Feb 2016 02:25:45 -0600
Subject: [PATCH 54/74] HTMLDocumentParser keeps track of how many bytes it has
 parsed so that it can pass the info through the tokens to Elements so they
 know their byte ranges within HTML

---
 Source/core/html/parser/AtomicHTMLToken.h        | 18 +++++++++++++
 Source/core/html/parser/HTMLConstructionSite.cpp |  6 +++++
 Source/core/html/parser/HTMLDocumentParser.cpp   | 32 ++++++++++++++++++++++++
 Source/core/html/parser/HTMLDocumentParser.h     | 19 ++++++++++++++
 Source/core/html/parser/HTMLToken.h              | 24 ++++++++++++++++++
 Source/core/html/parser/HTMLTreeBuilder.cpp      |  8 +++++-
 6 files changed, 106 insertions(+), 1 deletion(-)

diff --git a/Source/core/html/parser/AtomicHTMLToken.h b/Source/core/html/parser/AtomicHTMLToken.h
index 720736c..673f071 100644
--- a/Source/core/html/parser/AtomicHTMLToken.h
+++ b/Source/core/html/parser/AtomicHTMLToken.h
@@ -109,8 +109,17 @@ public:
         return m_doctypeData->m_systemIdentifier;
     }
 
+    int startingByteOffsetWithinHTML() const {
+        return m_startingByteOffsetWithinHTML;
+    }
+    int endingByteOffsetWithinHTML() const {
+        return m_endingByteOffsetWithinHTML;
+    }
+
     explicit AtomicHTMLToken(HTMLToken& token)
         : m_type(token.type())
+        , m_startingByteOffsetWithinHTML(token.startingByteOffsetWithinHTML())
+        , m_endingByteOffsetWithinHTML(token.endingByteOffsetWithinHTML())
     {
         switch (m_type) {
         case HTMLToken::Uninitialized:
@@ -144,6 +153,8 @@ public:
 
     explicit AtomicHTMLToken(const CompactHTMLToken& token)
         : m_type(token.type())
+        , m_startingByteOffsetWithinHTML(-1)
+        , m_endingByteOffsetWithinHTML(-1)
     {
         switch (m_type) {
         case HTMLToken::Uninitialized:
@@ -183,6 +194,8 @@ public:
     explicit AtomicHTMLToken(HTMLToken::Type type)
         : m_type(type)
         , m_selfClosing(false)
+        , m_startingByteOffsetWithinHTML(-1)
+        , m_endingByteOffsetWithinHTML(-1)
     {
     }
 
@@ -191,6 +204,8 @@ public:
         , m_name(name)
         , m_selfClosing(false)
         , m_attributes(attributes)
+        , m_startingByteOffsetWithinHTML(-1)
+        , m_endingByteOffsetWithinHTML(-1)
     {
         ASSERT(usesName());
     }
@@ -218,6 +233,9 @@ private:
     bool m_selfClosing;
 
     Vector<Attribute> m_attributes;
+
+    const int m_startingByteOffsetWithinHTML;
+    const int m_endingByteOffsetWithinHTML;
 };
 
 inline void AtomicHTMLToken::initializeAttributes(const HTMLToken::AttributeList& attributes)
diff --git a/Source/core/html/parser/HTMLConstructionSite.cpp b/Source/core/html/parser/HTMLConstructionSite.cpp
index 3bc6cd0..8aead3f 100644
--- a/Source/core/html/parser/HTMLConstructionSite.cpp
+++ b/Source/core/html/parser/HTMLConstructionSite.cpp
@@ -60,6 +60,12 @@ static const unsigned maximumHTMLParserDOMTreeDepth = 512;
 
 static inline void setAttributes(Element* element, AtomicHTMLToken* token, ParserContentPolicy parserContentPolicy)
 {
+    const Document& document = element->document();
+    if (document.isExtractingPageModel()) {
+        element->setByteOffsetsWithinHTML(token->startingByteOffsetWithinHTML(),
+                                          token->endingByteOffsetWithinHTML());
+    }
+
     if (!scriptingContentIsAllowed(parserContentPolicy))
         element->stripScriptingAttributes(token->attributes());
     element->parserSetAttributes(token->attributes());
diff --git a/Source/core/html/parser/HTMLDocumentParser.cpp b/Source/core/html/parser/HTMLDocumentParser.cpp
index 5c78b8d..25ef55d 100644
--- a/Source/core/html/parser/HTMLDocumentParser.cpp
+++ b/Source/core/html/parser/HTMLDocumentParser.cpp
@@ -46,6 +46,8 @@
 #include "public/platform/WebThreadedDataReceiver.h"
 #include "wtf/Functional.h"
 
+#include "platform/Logging.h"
+
 namespace blink {
 
 using namespace HTMLNames;
@@ -117,6 +119,7 @@ HTMLDocumentParser::HTMLDocumentParser(HTMLDocument& document, bool reportErrors
     , m_endWasDelayed(false)
     , m_haveBackgroundParser(false)
     , m_pumpSessionNestingLevel(0)
+    , m_numParsedBytes(0)
 {
     ASSERT(shouldUseThreading() || (m_token && m_tokenizer));
 }
@@ -603,6 +606,14 @@ void HTMLDocumentParser::pumpTokenizer(SynchronousMode mode)
 
     m_xssAuditor.init(document(), &m_xssAuditorDelegate);
 
+    const Document* doc = document();
+    const bool isExtractingPageModel = doc && doc->isExtractingPageModel();
+
+    if (isExtractingPageModel) {
+        // we should look into cases with more complex nesting
+        ASSERT(m_pumpSessionNestingLevel <= 1);
+    }
+
     while (canTakeNextToken(mode, session) && !session.needsYield) {
         if (!isParsingFragment())
             m_sourceTracker.start(m_input.current(), m_tokenizer.get(), token());
@@ -613,6 +624,27 @@ void HTMLDocumentParser::pumpTokenizer(SynchronousMode mode)
         if (!isParsingFragment()) {
             m_sourceTracker.end(m_input.current(), m_tokenizer.get(), token());
 
+            if (isExtractingPageModel) {
+                if (token().type() != HTMLToken::EndOfFile) {
+                    token().setStartingByteOffsetWithinHTML(m_numParsedBytes);
+
+                    /* copied this length computation from
+                       HTMLSourceTracker::sourceForToken() */
+                    const size_t tokenLen = static_cast<size_t>(token().endIndex() - token().startIndex());
+                    m_numParsedBytes += tokenLen;
+
+                    token().setEndingByteOffsetWithinHTML(m_numParsedBytes - 1);
+                } else {
+                    MYWTF_LOG_WITH_DOC(PageModeling, doc,
+                                       "EndOfFile token after %zu parsed bytes",
+                                       m_numParsedBytes);
+                }
+                // we haven't seen non-zero startindex yet, so not
+                // sure what that might mean, so assert here and we'll
+                // take a look when it happens.
+                ASSERT(token().startIndex() == 0);
+            }
+
             // We do not XSS filter innerHTML, which means we (intentionally) fail
             // http/tests/security/xssAuditor/dom-write-innerHTML.html
             if (OwnPtr<XSSInfo> xssInfo = m_xssAuditor.filterToken(FilterTokenRequest(token(), m_sourceTracker, m_tokenizer->shouldAllowCDATA())))
diff --git a/Source/core/html/parser/HTMLDocumentParser.h b/Source/core/html/parser/HTMLDocumentParser.h
index d9958b1..269d12b 100644
--- a/Source/core/html/parser/HTMLDocumentParser.h
+++ b/Source/core/html/parser/HTMLDocumentParser.h
@@ -205,6 +205,25 @@ private:
     bool m_endWasDelayed;
     bool m_haveBackgroundParser;
     unsigned m_pumpSessionNestingLevel;
+
+    /* all bytes in html file are converted into some token, e.g.,
+     * even meaningless whitespace bytes before <html> tag are parsed
+     * into a "character" token.
+     *
+     * therefore we can use the accumulated token lengths to know our
+     * current byte offset within the html document.
+     *
+     * CAVEAT: document.write() will interfere with this way of
+     * determining the offset within the html, i.e., tokens parsed
+     * from strings inserted by document.write() will contribute
+     * here. therefore, --suppress-script-execution command-line
+     * option should be used to prevent interference from
+     * document.write().
+     *
+     * m_input.current().numberOfCharactersConsumed() does not always
+     * work; in some cases, it decreases.
+     */
+    size_t m_numParsedBytes;
 };
 
 }
diff --git a/Source/core/html/parser/HTMLToken.h b/Source/core/html/parser/HTMLToken.h
index dce73e9..f1a923f 100644
--- a/Source/core/html/parser/HTMLToken.h
+++ b/Source/core/html/parser/HTMLToken.h
@@ -109,6 +109,9 @@ public:
         // HTMLToken will be destroyed and the VectorBuffer released.
         m_data.shrink(0);
         m_orAllData = 0;
+
+        m_startingByteOffsetWithinHTML = -1;
+        m_endingByteOffsetWithinHTML = -1;
     }
 
     bool isUninitialized() { return m_type == Uninitialized; }
@@ -435,6 +438,24 @@ public:
         m_orAllData = 0;
     }
 
+    int startingByteOffsetWithinHTML() const { return m_startingByteOffsetWithinHTML; }
+    void setStartingByteOffsetWithinHTML(int offset)
+    {
+        // catch multiple sets
+        ASSERT(m_startingByteOffsetWithinHTML == -1);
+        ASSERT(offset >= 0);
+        m_startingByteOffsetWithinHTML = offset;
+    }
+
+    int endingByteOffsetWithinHTML() const { return m_endingByteOffsetWithinHTML; }
+    void setEndingByteOffsetWithinHTML(int offset)
+    {
+        // catch multiple sets
+        ASSERT(m_endingByteOffsetWithinHTML == -1);
+        ASSERT(offset >= 0);
+        m_endingByteOffsetWithinHTML = offset;
+    }
+
 private:
     Type m_type;
     Attribute::Range m_range; // Always starts at zero.
@@ -451,6 +472,9 @@ private:
 
     // For DOCTYPE
     OwnPtr<DoctypeData> m_doctypeData;
+
+    int m_startingByteOffsetWithinHTML;
+    int m_endingByteOffsetWithinHTML;
 };
 
 }
diff --git a/Source/core/html/parser/HTMLTreeBuilder.cpp b/Source/core/html/parser/HTMLTreeBuilder.cpp
index 0a604ac..336b220 100644
--- a/Source/core/html/parser/HTMLTreeBuilder.cpp
+++ b/Source/core/html/parser/HTMLTreeBuilder.cpp
@@ -2096,8 +2096,14 @@ void HTMLTreeBuilder::processEndTag(AtomicHTMLToken* token)
         if (token->name() == scriptTag) {
             // Pause ourselves so that parsing stops until the script can be processed by the caller.
             ASSERT(m_tree.currentStackItem()->hasTagName(scriptTag));
-            if (scriptingContentIsAllowed(m_tree.parserContentPolicy()))
+
+            m_tree.currentStackItem()->element()->setEndingByteOffsetWithinHTML(
+                token->endingByteOffsetWithinHTML());
+
+            if (scriptingContentIsAllowed(m_tree.parserContentPolicy())) {
                 m_scriptToProcess = m_tree.currentElement();
+                ASSERT(m_scriptToProcess->endingByteOffsetWithinHTML() == token->endingByteOffsetWithinHTML());
+            }
             m_tree.openElements()->pop();
             setInsertionMode(m_originalInsertionMode);
 
-- 
2.1.4


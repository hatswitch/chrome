From cd6b475d805cad5e2331dbc2aa70b4f726b9b3d3 Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Wed, 4 May 2016 19:59:14 -0500
Subject: [PATCH 128/131] more logging changes to make facilitate log parsing

---
 Source/core/dom/Document.cpp                    | 41 +++++++++++++++++++++++--
 Source/core/dom/ScriptLoader.cpp                | 20 ++++++------
 Source/core/frame/LocalDOMWindow.cpp            | 14 ++++++---
 Source/core/html/HTMLScriptElement.cpp          |  4 +--
 Source/core/html/parser/HTMLDocumentParser.cpp  | 30 ++++++++++++++++--
 Source/core/loader/DocumentThreadableLoader.cpp | 10 ++++--
 Source/core/xml/XMLHttpRequest.cpp              | 24 ++++++++++++---
 7 files changed, 115 insertions(+), 28 deletions(-)

diff --git a/Source/core/dom/Document.cpp b/Source/core/dom/Document.cpp
index cf86357..d94ba1f 100644
--- a/Source/core/dom/Document.cpp
+++ b/Source/core/dom/Document.cpp
@@ -1337,7 +1337,19 @@ void Document::setReadyState(ReadyState readyState)
     }
 
     m_readyState = readyState;
+    const String eventName = EventTypeNames::readystatechange;
+    const String readyStateStr = this->readyState();
+    if (m_extractingPageModel) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this,
+                           "begin dispatching event [%s], state [%s]",
+                           eventName.ascii().data(), readyStateStr.ascii().data());
+    }
     dispatchEvent(Event::create(EventTypeNames::readystatechange));
+    if (m_extractingPageModel) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this,
+                           "done dispatching event [%s], state [%s]",
+                           eventName.ascii().data(), readyStateStr.ascii().data());
+    }
 }
 
 bool Document::isLoadCompleted()
@@ -3312,7 +3324,7 @@ void Document::didLoadAllScriptBlockingResources()
 {
     if (m_extractingPageModel) {
         MYWTF_LOG_WITH_DOC(PageModeling, this,
-                           "done loading all script blocking resources");
+                           "done loading all script blocking resources. start timer");
     }
 
     m_executeScriptsWaitingForResourcesTimer.startOneShot(0, FROM_HERE);
@@ -3326,10 +3338,35 @@ void Document::didLoadAllScriptBlockingResources()
 
 void Document::executeScriptsWaitingForResourcesTimerFired(Timer<Document>*)
 {
-    if (!isRenderingReady())
+    if (m_extractingPageModel) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this,
+                           "begin executing scripts blocked by style resources");
+    }
+
+    if (!isRenderingReady()) {
+        // this can happen because after the parser
+        // executeScriptsWaitingForResources(), it resumes parsing
+        // (before returning to us), which can encounter a <style>
+        // element (which can only be inline, i.e., cannot link to
+        // external resource), which after being parsed will call
+        // didLoadAllScriptBlockingResources() and start the timer
+        // again, then the parser continues and can run into an
+        // external style sheet that requires network fetch, therefore
+        // "isRenderingReady()" will start returning false, then after
+        // that our timer here fires and we are back in here.
+        if (m_extractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, this,
+                               "NOT executing scripts blocked by style resources");
+        }
         return;
+    }
     if (ScriptableDocumentParser* parser = scriptableDocumentParser())
         parser->executeScriptsWaitingForResources();
+
+    if (m_extractingPageModel) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this,
+                           "done executing scripts blocked by style resources");
+    }
 }
 
 CSSStyleSheet& Document::elementSheet()
diff --git a/Source/core/dom/ScriptLoader.cpp b/Source/core/dom/ScriptLoader.cpp
index 172529a..0fb81f5 100644
--- a/Source/core/dom/ScriptLoader.cpp
+++ b/Source/core/dom/ScriptLoader.cpp
@@ -174,7 +174,7 @@ bool ScriptLoader::prepareScript(const TextPosition& scriptStartPosition, Legacy
     const uint32_t elemInstNum = m_element->instNum();
     if (isExtractingPageModel) {
         MYWTF_LOG_WITH_DOC(PageModeling, &(m_element->document()),
-                           "script elem:%u: begin prepare",
+                           "script elem:%u: begin prepare script",
                            elemInstNum);
     }
 
@@ -185,7 +185,7 @@ bool ScriptLoader::prepareScript(const TextPosition& scriptStartPosition, Legacy
                                "script elem:%u: already started",
                                elemInstNum);
             MYWTF_LOG_WITH_DOC(PageModeling, &(m_element->document()),
-                               "script elem:%u: done",
+                               "script elem:%u: done prepare script",
                                elemInstNum);
         }
         return false;
@@ -234,7 +234,7 @@ bool ScriptLoader::prepareScript(const TextPosition& scriptStartPosition, Legacy
                                elemInstNum, type.ascii().data(), language.ascii().data());
             // ASSERT(type == "application/ld+json" || type == "text/template" || type == "text/html");
             MYWTF_LOG_WITH_DOC(PageModeling, &(m_element->document()),
-                               "script elem:%u: done",
+                               "script elem:%u: done prepare script",
                                elemInstNum);
         }
         return false;
@@ -331,13 +331,13 @@ bool ScriptLoader::prepareScript(const TextPosition& scriptStartPosition, Legacy
     } else if (client->hasSourceAttribute() && !client->asyncAttributeValue() && !m_forceAsync) {
         if (isExtractingPageModel) {
             MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument,
-                               "script elem:%u: [willExecuteInOrder= 1] ()",
-                               elemInstNum
-                               );
-            MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument,
-                               "script elem:%u: [queue for in-order execution] ()",
+                               "script elem:%u: [willExecuteInOrder= 1] (has src attr, not async)",
                                elemInstNum
                                );
+            // MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument,
+            //                    "script elem:%u: [queue for in-order execution] ()",
+            //                    elemInstNum
+            //                    );
         }
         m_willExecuteInOrder = true;
         contextDocument->scriptRunner()->queueScriptForExecution(this, m_resource, ScriptRunner::IN_ORDER_EXECUTION);
@@ -345,7 +345,7 @@ bool ScriptLoader::prepareScript(const TextPosition& scriptStartPosition, Legacy
     } else if (client->hasSourceAttribute()) {
         if (isExtractingPageModel) {
             MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument,
-                               "script elem:%u: [queue for async execution] ()",
+                               "script elem:%u: [queue for async execution] (has src attr)",
                                elemInstNum
                                );
         }
@@ -363,7 +363,7 @@ bool ScriptLoader::prepareScript(const TextPosition& scriptStartPosition, Legacy
     }
 
     if (isExtractingPageModel) {
-        MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument, "script elem:%u: done",
+        MYWTF_LOG_WITH_DOC(PageModeling, &elementDocument, "script elem:%u: done prepare script",
                            elemInstNum);
     }
 
diff --git a/Source/core/frame/LocalDOMWindow.cpp b/Source/core/frame/LocalDOMWindow.cpp
index c45d7f7..f6fe56e 100644
--- a/Source/core/frame/LocalDOMWindow.cpp
+++ b/Source/core/frame/LocalDOMWindow.cpp
@@ -414,7 +414,7 @@ PassRefPtrWillBeRawPtr<Document> LocalDOMWindow::installNewDocument(const String
 
     if (m_document->isExtractingPageModel()) {
         MYWTF_LOG_WITH_DOC(PageModeling, m_document,
-                           "installed on dom_window:%u",
+                           "installed on window:%u",
                            instNum());
     }
 
@@ -1645,9 +1645,12 @@ bool LocalDOMWindow::dispatchEvent(PassRefPtrWillBeRawPtr<Event> prpEvent, PassR
     RefPtrWillBeRawPtr<Event> event = prpEvent;
 
     const bool extractingModel = document() && document()->isExtractingPageModel();
+    const uint32_t windowInstNum = instNum();
+    const String eventName = event->type();
     if (extractingModel) {
-        MYWTF_LOG_WITH_DOC(PageModeling, document(), "%s event: whole document: begin",
-                           event->type().ascii().data());
+        MYWTF_LOG_WITH_DOC(PageModeling, document(),
+                           "window:%u, event [%s]: whole document: begin",
+                           windowInstNum, eventName.ascii().data());
     }
 
     event->setTarget(prpTarget ? prpTarget : this);
@@ -1661,8 +1664,9 @@ bool LocalDOMWindow::dispatchEvent(PassRefPtrWillBeRawPtr<Event> prpEvent, PassR
     bool result = fireEventListeners(event.get());
 
     if (extractingModel) {
-        MYWTF_LOG_WITH_DOC(PageModeling, document(), "%s event: whole document: done",
-                           event->type().ascii().data());
+        MYWTF_LOG_WITH_DOC(PageModeling, document(),
+                           "window:%u, event [%s]: whole document: done",
+                           windowInstNum, eventName.ascii().data());
     }
 
     InspectorInstrumentation::didDispatchEventOnWindow(cookie);
diff --git a/Source/core/html/HTMLScriptElement.cpp b/Source/core/html/HTMLScriptElement.cpp
index e0b087f..80fc8a1 100644
--- a/Source/core/html/HTMLScriptElement.cpp
+++ b/Source/core/html/HTMLScriptElement.cpp
@@ -48,8 +48,8 @@ inline HTMLScriptElement::HTMLScriptElement(Document& document, bool wasInserted
     if (document.isExtractingPageModel()) {
         MYWTF_LOG_WITH_DOC(
             PageModeling, &document,
-            "new script elem:%u createdByParser= %d depParents= [%s]",
-            instNum(), wasInsertedByParser,
+            "new script elem:%u createdByParser= %d alreadyStarted= %d depParents= [%s]",
+            instNum(), m_loader->isParserInserted(), m_loader->alreadyStarted(),
             DependencyParentInfo::buildDepParentsOutputString(depParents()).ascii().data());
     }
 }
diff --git a/Source/core/html/parser/HTMLDocumentParser.cpp b/Source/core/html/parser/HTMLDocumentParser.cpp
index 0d58c20..b4e8c7f 100644
--- a/Source/core/html/parser/HTMLDocumentParser.cpp
+++ b/Source/core/html/parser/HTMLDocumentParser.cpp
@@ -641,8 +641,11 @@ void HTMLDocumentParser::pumpTokenizer(SynchronousMode mode)
 
     String msg;
     if (document()->isExtractingPageModel()) {
-        msg = String::format("doc:%u: parser:%u: pumpTokenizer()",
-                             document()->instNum(), instNum());
+        const char* file = strrchr(__FILE__, '/');
+        file = file ? file + 1 : __FILE__;
+        msg = String::format("doc:%u: parser:%u: pumpTokenizer() (%s:%u)",
+                             document()->instNum(), instNum(),
+                             file, __LINE__);
     }
     PumpSession session(m_pumpSessionNestingLevel, contextForParsingSession(),
                         document()->isExtractingPageModel() ? &msg : NULL);
@@ -1195,9 +1198,30 @@ void HTMLDocumentParser::executeScriptsWaitingForResources()
     // pumpTokenizer can cause this parser to be detached from the Document,
     // but we need to ensure it isn't deleted yet.
     RefPtrWillBeRawPtr<HTMLDocumentParser> protect(this);
+
+    if (m_isExtractingPageModel) {
+        MYWTF_LOG_WITH_DOC_AND_PARSER(PageModeling, document(), this,
+                                      "begin calling runner's executeScriptsWaitingForResources()");
+    }
+
     m_scriptRunner->executeScriptsWaitingForResources();
-    if (!isWaitingForScripts())
+
+    if (m_isExtractingPageModel) {
+        MYWTF_LOG_WITH_DOC_AND_PARSER(PageModeling, document(), this,
+                                      "done calling runner's executeScriptsWaitingForResources()");
+    }
+
+    if (!isWaitingForScripts()) {
+        if (m_isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC_AND_PARSER(PageModeling, document(), this,
+                                          "not waiting for script, so begin resume parsing");
+        }
         resumeParsingAfterScriptExecution();
+        if (m_isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC_AND_PARSER(PageModeling, document(), this,
+                                          "not waiting for script, so done resume parsing");
+        }
+    }
 }
 
 void HTMLDocumentParser::parseDocumentFragment(const String& source, DocumentFragment* fragment, Element* contextElement, ParserContentPolicy parserContentPolicy)
diff --git a/Source/core/loader/DocumentThreadableLoader.cpp b/Source/core/loader/DocumentThreadableLoader.cpp
index 78b0220..00af758 100644
--- a/Source/core/loader/DocumentThreadableLoader.cpp
+++ b/Source/core/loader/DocumentThreadableLoader.cpp
@@ -512,6 +512,12 @@ void DocumentThreadableLoader::loadRequest(const ResourceRequest& request, Resou
         if (m_options.crossOriginRequestPolicy == AllowCrossOriginRequests)
             newRequest.setOriginRestriction(FetchRequest::NoOriginRestriction);
         ASSERT(!resource());
+        if (m_isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC_AND_THREADABLE_LOADER(
+                PageModeling, &m_document, this,
+                "begin creating async= %d request, isPreflight= %d",
+                m_async, m_actualRequest ? 1 : 0);
+        }
         if (request.requestContext() == blink::WebURLRequest::RequestContextVideo || request.requestContext() == blink::WebURLRequest::RequestContextAudio)
             setResource(m_document.fetcher()->fetchMedia(newRequest));
         else
@@ -521,10 +527,10 @@ void DocumentThreadableLoader::loadRequest(const ResourceRequest& request, Resou
             InspectorInstrumentation::documentThreadableLoaderStartedLoadingForClient(&m_document, identifier, m_client);
         }
 
-        if (resource() && m_isExtractingPageModel) {
+        if (m_isExtractingPageModel) {
             MYWTF_LOG_WITH_DOC_AND_THREADABLE_LOADER(
                 PageModeling, &m_document, this,
-                "async= %d loading, isPreflight= %d",
+                "done creating async= %d request, isPreflight= %d",
                 m_async, m_actualRequest ? 1 : 0);
         }
 
diff --git a/Source/core/xml/XMLHttpRequest.cpp b/Source/core/xml/XMLHttpRequest.cpp
index b1d1dc3..a0d5314 100644
--- a/Source/core/xml/XMLHttpRequest.cpp
+++ b/Source/core/xml/XMLHttpRequest.cpp
@@ -855,21 +855,31 @@ void XMLHttpRequest::createRequest(PassRefPtr<FormData> httpBody, ExceptionState
         // FIXME: Maybe we need to be able to send XMLHttpRequests from onunload, <http://bugs.webkit.org/show_bug.cgi?id=10904>.
         // FIXME: Maybe create() can return null for other reasons too?
         ASSERT(!m_loader);
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC_AND_XMLHTTPREQUEST(PageModeling, document, this,
+                                                  "begin creating async loader");
+        }
         m_loader = ThreadableLoader::create(executionContext, this, request, options, resourceLoaderOptions);
         if (isExtractingPageModel) {
             MYWTF_LOG_WITH_DOC_AND_XMLHTTPREQUEST(PageModeling, document, this,
-                                                  "threadableLoader:%u,",
+                                                  "done creating async loader"
+                                                  " threadableLoader:%u,",
                                                   m_loader->instNum());
         }
     } else {
         if (isExtractingPageModel) {
             MYWTF_LOG_WITH_DOC_AND_XMLHTTPREQUEST(PageModeling, document, this,
-                                                  "loading synchronously");
+                                                  "begin loading synchronously");
         }
 
         // Use count for XHR synchronous requests.
         UseCounter::count(&executionContext, UseCounter::XMLHttpRequestSynchronous);
         ThreadableLoader::loadResourceSynchronously(executionContext, request, *this, options, resourceLoaderOptions);
+
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC_AND_XMLHTTPREQUEST(PageModeling, document, this,
+                                                  "done loading synchronously");
+        }
     }
 
     if (!m_exceptionCode && m_error)
@@ -1275,13 +1285,19 @@ void XMLHttpRequest::didFinishLoading(unsigned long identifier, double)
     const ExecutionContext* context = executionContext();
     ASSERT(context);
     const Document* document = context->isDocument() ? toDocument(context) : NULL;
+    const bool isExtractingPageModel = document && document->isExtractingPageModel();
 
-    if (document && document->isExtractingPageModel()) {
+    if (isExtractingPageModel) {
         MYWTF_LOG_WITH_DOC_AND_XMLHTTPREQUEST(PageModeling, document, this,
-                                              "finished loading");
+                                              "finished loading, begin changing state");
     }
 
     changeState(DONE);
+
+    if (isExtractingPageModel) {
+        MYWTF_LOG_WITH_DOC_AND_XMLHTTPREQUEST(PageModeling, document, this,
+                                              "finished loading, done changing state");
+    }
 }
 
 void XMLHttpRequest::didSendData(unsigned long long bytesSent, unsigned long long totalBytesToBeSent)
-- 
2.1.4


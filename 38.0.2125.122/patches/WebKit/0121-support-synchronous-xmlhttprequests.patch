From 3c6efc35aed7e406880a244ac67a62ed4f449e0b Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Sun, 3 Apr 2016 16:08:17 -0500
Subject: [PATCH 121/123] support synchronous xmlhttprequests

---
 Source/core/loader/DocumentThreadableLoader.cpp | 47 +++++++++++++++++++++++--
 Source/core/loader/DocumentThreadableLoader.h   |  2 ++
 Source/core/loader/ThreadableLoader.cpp         | 12 +++++++
 Source/core/loader/ThreadableLoader.h           |  6 +++-
 Source/core/xml/XMLHttpRequest.cpp              | 32 +++++++++++------
 5 files changed, 86 insertions(+), 13 deletions(-)

diff --git a/Source/core/loader/DocumentThreadableLoader.cpp b/Source/core/loader/DocumentThreadableLoader.cpp
index bb4cdbc..78b0220 100644
--- a/Source/core/loader/DocumentThreadableLoader.cpp
+++ b/Source/core/loader/DocumentThreadableLoader.cpp
@@ -53,8 +53,14 @@
 #include "public/platform/WebURLRequest.h"
 #include "wtf/Assertions.h"
 
+#include "platform/Logging.h"
+
 namespace blink {
 
+#define MYWTF_LOG_WITH_DOC_AND_THREADABLE_LOADER(channel, docPtr, threadableLoaderPtr, fmt, ...) \
+    MYWTF_LOG_WITH_DOC(channel, (docPtr), "threadableLoader:%u, " fmt, (threadableLoaderPtr)->instNum(), ##__VA_ARGS__)
+
+
 void DocumentThreadableLoader::loadResourceSynchronously(Document& document, const ResourceRequest& request, ThreadableLoaderClient& client, const ThreadableLoaderOptions& options, const ResourceLoaderOptions& resourceLoaderOptions)
 {
     // The loader will be deleted as soon as this function exits.
@@ -82,13 +88,17 @@ DocumentThreadableLoader::DocumentThreadableLoader(Document& document, Threadabl
     , m_async(blockingBehavior == LoadAsynchronously)
     , m_timeoutTimer(this, &DocumentThreadableLoader::didTimeout)
     , m_requestStartedSeconds(0.0)
+    , m_isExtractingPageModel(document.isExtractingPageModel())
 {
-    ASSERT(m_async || !document.isExtractingPageModel());
-
     ASSERT(client);
     // Setting an outgoing referer is only supported in the async code path.
     ASSERT(m_async || request.httpReferrer().isEmpty());
 
+    if (m_isExtractingPageModel) {
+        MYWTF_LOG_WITH_DOC_AND_THREADABLE_LOADER(PageModeling, &m_document, this,
+                                                 "async= %d", m_async);
+    }
+
     m_requestStartedSeconds = monotonicallyIncreasingTime();
 
     // Save any CORS simple headers on the request here. If this request redirects cross-origin, we cancel the old request
@@ -148,6 +158,11 @@ void DocumentThreadableLoader::makeCrossOriginAccessRequest(const ResourceReques
             // Create a ResourceLoaderOptions for preflight.
             ResourceLoaderOptions preflightOptions = *m_actualOptions;
             preflightOptions.allowCredentials = DoNotAllowStoredCredentials;
+            // if (m_isExtractingPageModel) {
+            //     MYWTF_LOG_WITH_DOC_AND_THREADABLE_LOADER(
+            //         PageModeling, &m_document, this,
+            //         "about to send preflight request");
+            // }
             loadRequest(preflightRequest, preflightOptions);
         }
     }
@@ -453,6 +468,11 @@ void DocumentThreadableLoader::loadActualRequest()
 
     clearResource();
 
+    // if (m_isExtractingPageModel) {
+    //     MYWTF_LOG_WITH_DOC_AND_THREADABLE_LOADER(
+    //         PageModeling, &m_document, this,
+    //         "about to send actual request");
+    // }
     loadRequest(*actualRequest, *actualOptions);
 }
 
@@ -500,13 +520,36 @@ void DocumentThreadableLoader::loadRequest(const ResourceRequest& request, Resou
             unsigned long identifier = resource()->identifier();
             InspectorInstrumentation::documentThreadableLoaderStartedLoadingForClient(&m_document, identifier, m_client);
         }
+
+        if (resource() && m_isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC_AND_THREADABLE_LOADER(
+                PageModeling, &m_document, this,
+                "async= %d loading, isPreflight= %d",
+                m_async, m_actualRequest ? 1 : 0);
+        }
+
         return;
     }
 
     FetchRequest fetchRequest(request, m_options.initiator, resourceLoaderOptions);
     if (m_options.crossOriginRequestPolicy == AllowCrossOriginRequests)
         fetchRequest.setOriginRestriction(FetchRequest::NoOriginRestriction);
+
+    if (m_isExtractingPageModel) {
+        MYWTF_LOG_WITH_DOC_AND_THREADABLE_LOADER(
+                PageModeling, &m_document, this,
+                "async= %d loading, isPreflight= %d",
+                m_async, m_actualRequest ? 1 : 0);
+    }
     ResourcePtr<Resource> resource = m_document.fetcher()->fetchSynchronously(fetchRequest);
+
+    if (m_isExtractingPageModel) {
+        MYWTF_LOG_WITH_DOC_AND_THREADABLE_LOADER(
+                PageModeling, &m_document, this,
+                "res:%u,", resource ? resource->instNum() : 0);
+        ASSERT(resource);
+    }
+
     ResourceResponse response = resource ? resource->response() : ResourceResponse();
     unsigned long identifier = resource ? resource->identifier() : std::numeric_limits<unsigned long>::max();
     ResourceError error = resource ? resource->resourceError() : ResourceError();
diff --git a/Source/core/loader/DocumentThreadableLoader.h b/Source/core/loader/DocumentThreadableLoader.h
index f40797c..a82f564 100644
--- a/Source/core/loader/DocumentThreadableLoader.h
+++ b/Source/core/loader/DocumentThreadableLoader.h
@@ -132,6 +132,8 @@ class DocumentThreadableLoader FINAL : public ThreadableLoader, private Resource
         HTTPHeaderMap m_simpleRequestHeaders; // stores simple request headers in case of a cross-origin redirect.
         Timer<DocumentThreadableLoader> m_timeoutTimer;
         double m_requestStartedSeconds; // Time an asynchronous fetch request is started
+
+        const bool m_isExtractingPageModel;
     };
 
 } // namespace blink
diff --git a/Source/core/loader/ThreadableLoader.cpp b/Source/core/loader/ThreadableLoader.cpp
index 138c39d..1507516 100644
--- a/Source/core/loader/ThreadableLoader.cpp
+++ b/Source/core/loader/ThreadableLoader.cpp
@@ -42,6 +42,18 @@
 
 namespace blink {
 
+static uint32_t nextThreadableLoaderInstNum()
+{
+    static uint32_t next = 1;
+    ASSERT(next <= 0xfffff);
+    return ++next;
+}
+
+ThreadableLoader::ThreadableLoader()
+    : m_instNum(nextThreadableLoaderInstNum())
+{
+}
+
 PassRefPtr<ThreadableLoader> ThreadableLoader::create(ExecutionContext& context, ThreadableLoaderClient* client, const ResourceRequest& request, const ThreadableLoaderOptions& options, const ResourceLoaderOptions& resourceLoaderOptions)
 {
     ASSERT(client);
diff --git a/Source/core/loader/ThreadableLoader.h b/Source/core/loader/ThreadableLoader.h
index f0eddc6..5287a32 100644
--- a/Source/core/loader/ThreadableLoader.h
+++ b/Source/core/loader/ThreadableLoader.h
@@ -139,8 +139,12 @@ namespace blink {
 
         virtual ~ThreadableLoader() { }
 
+        inline uint32_t instNum() const { return m_instNum; }
+
     protected:
-        ThreadableLoader() { }
+        ThreadableLoader();
+
+        const uint32_t m_instNum;
     };
 
 } // namespace blink
diff --git a/Source/core/xml/XMLHttpRequest.cpp b/Source/core/xml/XMLHttpRequest.cpp
index 66df1ec..b1d1dc3 100644
--- a/Source/core/xml/XMLHttpRequest.cpp
+++ b/Source/core/xml/XMLHttpRequest.cpp
@@ -67,6 +67,9 @@ namespace blink {
 
 DEFINE_DEBUG_ONLY_GLOBAL(WTF::RefCountedLeakCounter, xmlHttpRequestCounter, ("XMLHttpRequest"));
 
+#define MYWTF_LOG_WITH_DOC_AND_XMLHTTPREQUEST(channel, docPtr, xmlhttpreqPtr, fmt, ...) \
+    MYWTF_LOG_WITH_DOC(channel, (docPtr), "xmlhttprequest:%u, " fmt, (xmlhttpreqPtr)->instNum(), ##__VA_ARGS__)
+
 static bool isSetCookieHeader(const AtomicString& name)
 {
     return equalIgnoringCase(name, "set-cookie") || equalIgnoringCase(name, "set-cookie2");
@@ -135,8 +138,8 @@ XMLHttpRequest::XMLHttpRequest(ExecutionContext* context, PassRefPtr<SecurityOri
     const Document* document = context->isDocument() ? toDocument(context) : NULL;
 
     if (document && document->isExtractingPageModel()) {
-        MYWTF_LOG_WITH_DOC(PageModeling, document,
-                           "xmlhttprequest:%d, created", instNum());
+        MYWTF_LOG_WITH_DOC_AND_XMLHTTPREQUEST(
+            PageModeling, document, this, "created");
     }
 }
 
@@ -551,13 +554,14 @@ void XMLHttpRequest::open(const AtomicString& method, const KURL& url, bool asyn
 
     m_url = url;
 
+#if 0
     const Document* document = executionContext()->isDocument() ? toDocument(executionContext()) : NULL;
 
     if (document && document->isExtractingPageModel()) {
-        MYWTF_LOG_WITH_DOC(PageModeling, document,
-                           "xmlhttprequest:%d, url [%s]", instNum(),
-                           m_url.string().ascii().data());
+        MYWTF_LOG_WITH_DOC_AND_XMLHTTPREQUEST(PageModeling, document, this,
+                                              "url [%s]", m_url.string().ascii().data());
     }
+#endif
 
     m_async = async;
 
@@ -852,8 +856,16 @@ void XMLHttpRequest::createRequest(PassRefPtr<FormData> httpBody, ExceptionState
         // FIXME: Maybe create() can return null for other reasons too?
         ASSERT(!m_loader);
         m_loader = ThreadableLoader::create(executionContext, this, request, options, resourceLoaderOptions);
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC_AND_XMLHTTPREQUEST(PageModeling, document, this,
+                                                  "threadableLoader:%u,",
+                                                  m_loader->instNum());
+        }
     } else {
-        ASSERT_WITH_MESSAGE(!isExtractingPageModel, "synchronous ajax request not yet covered");
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC_AND_XMLHTTPREQUEST(PageModeling, document, this,
+                                                  "loading synchronously");
+        }
 
         // Use count for XHR synchronous requests.
         UseCounter::count(&executionContext, UseCounter::XMLHttpRequestSynchronous);
@@ -893,8 +905,8 @@ void XMLHttpRequest::abort()
     const Document* document = executionContext()->isDocument() ? toDocument(executionContext()) : NULL;
 
     if (document && document->isExtractingPageModel()) {
-        MYWTF_LOG_WITH_DOC(PageModeling, document,
-                           "xmlhttprequest:%d, aborted", instNum());
+        MYWTF_LOG_WITH_DOC_AND_XMLHTTPREQUEST(PageModeling, document, this,
+                                              "aborted");
         ASSERT_NOT_REACHED();
     }
 }
@@ -1265,8 +1277,8 @@ void XMLHttpRequest::didFinishLoading(unsigned long identifier, double)
     const Document* document = context->isDocument() ? toDocument(context) : NULL;
 
     if (document && document->isExtractingPageModel()) {
-        MYWTF_LOG_WITH_DOC(PageModeling, document,
-                           "xmlhttprequest:%d, finished loading", instNum());
+        MYWTF_LOG_WITH_DOC_AND_XMLHTTPREQUEST(PageModeling, document, this,
+                                              "finished loading");
     }
 
     changeState(DONE);
-- 
2.1.4


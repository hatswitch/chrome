From eb06593f65387674a08d67fe9da05fd50f0d3f4d Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Mon, 14 Mar 2016 02:18:24 -0500
Subject: [PATCH 98/98] * add some asserts and logging * background html parser
 annotates tokens with their offsets within html

---
 Source/core/dom/Document.cpp                     | 12 +++++++++
 Source/core/fetch/ResourceFetcher.cpp            | 10 +++++++-
 Source/core/fetch/ResourceFetcher.h              |  5 ++++
 Source/core/frame/LocalDOMWindow.cpp             | 12 +++++++++
 Source/core/html/parser/AtomicHTMLToken.h        |  4 +--
 Source/core/html/parser/BackgroundHTMLParser.cpp | 32 ++++++++++++++++++++++++
 Source/core/html/parser/BackgroundHTMLParser.h   |  8 ++++++
 Source/core/html/parser/CompactHTMLToken.cpp     |  5 ++++
 Source/core/html/parser/CompactHTMLToken.h       | 21 ++++++++++++++++
 Source/core/html/parser/HTMLDocumentParser.cpp   | 29 +++++++++++++++++++++
 10 files changed, 135 insertions(+), 3 deletions(-)

diff --git a/Source/core/dom/Document.cpp b/Source/core/dom/Document.cpp
index 3562924..8438c9c 100644
--- a/Source/core/dom/Document.cpp
+++ b/Source/core/dom/Document.cpp
@@ -5345,6 +5345,8 @@ void Document::addMessage(PassRefPtrWillBeRawPtr<ConsoleMessage> consoleMessage)
 // FIXME(crbug.com/305497): This should be removed after ExecutionContext-LocalDOMWindow migration.
 void Document::postTask(PassOwnPtr<ExecutionContextTask> task)
 {
+    ASSERT(!m_extractingPageModel);
+
     m_taskRunner->postTask(task);
 }
 
@@ -5355,6 +5357,8 @@ void Document::postInspectorTask(PassOwnPtr<ExecutionContextTask> task)
 
 void Document::tasksWereSuspended()
 {
+    ASSERT(!m_extractingPageModel);
+
     scriptRunner()->suspend();
 
     if (m_parser)
@@ -5365,6 +5369,8 @@ void Document::tasksWereSuspended()
 
 void Document::tasksWereResumed()
 {
+    ASSERT(!m_extractingPageModel);
+
     scriptRunner()->resume();
 
     if (m_parser)
@@ -5379,18 +5385,24 @@ void Document::tasksWereResumed()
 // should be moved to LocalDOMWindow once it inherits ExecutionContext
 void Document::suspendScheduledTasks()
 {
+    ASSERT(!m_extractingPageModel);
+
     ExecutionContext::suspendScheduledTasks();
     m_taskRunner->suspend();
 }
 
 void Document::resumeScheduledTasks()
 {
+    ASSERT(!m_extractingPageModel);
+
     ExecutionContext::resumeScheduledTasks();
     m_taskRunner->resume();
 }
 
 bool Document::tasksNeedSuspension()
 {
+    ASSERT(!m_extractingPageModel);
+
     Page* page = this->page();
     return page && page->defersLoading();
 }
diff --git a/Source/core/fetch/ResourceFetcher.cpp b/Source/core/fetch/ResourceFetcher.cpp
index df091ad..5449d14 100644
--- a/Source/core/fetch/ResourceFetcher.cpp
+++ b/Source/core/fetch/ResourceFetcher.cpp
@@ -80,7 +80,7 @@ using blink::WebURLRequest;
 namespace blink {
 
 #define MYWTF_LOG_WITH_DOC_AND_FETCHER(channel, docPtr, fetcherPtr, fmt, ...) \
-    MYWTF_LOG_WITH_DOC(channel, (docPtr), "fetcher %p, " fmt, (fetcherPtr), ##__VA_ARGS__)
+    MYWTF_LOG_WITH_DOC(channel, (docPtr), "fetcher:%u, " fmt, (fetcherPtr)->instNum(), ##__VA_ARGS__)
 
 static Resource* createResource(Resource::Type type, const ResourceRequest& request, const String& charset)
 {
@@ -232,6 +232,13 @@ static WebURLRequest::RequestContext requestContextFromType(const ResourceFetche
     return WebURLRequest::RequestContextSubresource;
 }
 
+static uint32_t nextResourceFetcherInstNum()
+{
+    static uint32_t next = 1;
+    ASSERT(next <= 0xfffff);
+    return ++next;
+}
+
 ResourceFetcher::ResourceFetcher(DocumentLoader* documentLoader)
     : m_document(nullptr)
     , m_documentLoader(documentLoader)
@@ -242,6 +249,7 @@ ResourceFetcher::ResourceFetcher(DocumentLoader* documentLoader)
     , m_imagesEnabled(true)
     , m_allowStaleResources(false)
     , m_memoryCache(NULL)
+    , m_instNum(nextResourceFetcherInstNum())
 {
 }
 
diff --git a/Source/core/fetch/ResourceFetcher.h b/Source/core/fetch/ResourceFetcher.h
index 64e1d82..a199547 100644
--- a/Source/core/fetch/ResourceFetcher.h
+++ b/Source/core/fetch/ResourceFetcher.h
@@ -166,6 +166,9 @@ public:
     };
     void requestLoadStarted(Resource*, const FetchRequest&, ResourceLoadStartType);
     static const ResourceLoaderOptions& defaultResourceOptions();
+
+    inline uint32_t instNum() const { return m_instNum; }
+
 private:
 
     explicit ResourceFetcher(DocumentLoader*);
@@ -247,6 +250,8 @@ private:
 
     // just for logging of which memory cache this fetcher uses
     MemoryCache* m_memoryCache;
+
+    const uint32_t m_instNum;
 };
 
 class ResourceCacheValidationSuppressor {
diff --git a/Source/core/frame/LocalDOMWindow.cpp b/Source/core/frame/LocalDOMWindow.cpp
index 03d7035..c45d7f7 100644
--- a/Source/core/frame/LocalDOMWindow.cpp
+++ b/Source/core/frame/LocalDOMWindow.cpp
@@ -412,6 +412,12 @@ PassRefPtrWillBeRawPtr<Document> LocalDOMWindow::installNewDocument(const String
     m_eventQueue = DOMWindowEventQueue::create(m_document.get());
     m_document->attach();
 
+    if (m_document->isExtractingPageModel()) {
+        MYWTF_LOG_WITH_DOC(PageModeling, m_document,
+                           "installed on dom_window:%u",
+                           instNum());
+    }
+
     if (!m_frame)
         return m_document;
 
@@ -432,11 +438,15 @@ PassRefPtrWillBeRawPtr<Document> LocalDOMWindow::installNewDocument(const String
 
 EventQueue* LocalDOMWindow::eventQueue() const
 {
+    ASSERT(!m_document || m_document->isExtractingPageModel());
+
     return m_eventQueue.get();
 }
 
 void LocalDOMWindow::enqueueWindowEvent(PassRefPtrWillBeRawPtr<Event> event)
 {
+    ASSERT(!m_document || m_document->isExtractingPageModel());
+
     if (!m_eventQueue)
         return;
     event->setTarget(this);
@@ -445,6 +455,8 @@ void LocalDOMWindow::enqueueWindowEvent(PassRefPtrWillBeRawPtr<Event> event)
 
 void LocalDOMWindow::enqueueDocumentEvent(PassRefPtrWillBeRawPtr<Event> event)
 {
+    ASSERT(!m_document || m_document->isExtractingPageModel());
+
     if (!m_eventQueue)
         return;
     event->setTarget(m_document.get());
diff --git a/Source/core/html/parser/AtomicHTMLToken.h b/Source/core/html/parser/AtomicHTMLToken.h
index 31e1cf9..62c3abc 100644
--- a/Source/core/html/parser/AtomicHTMLToken.h
+++ b/Source/core/html/parser/AtomicHTMLToken.h
@@ -157,8 +157,8 @@ public:
 
     explicit AtomicHTMLToken(const CompactHTMLToken& token)
         : m_type(token.type())
-        , m_startingByteOffsetWithinHTML(-1)
-        , m_endingByteOffsetWithinHTML(-1)
+        , m_startingByteOffsetWithinHTML(token.startingByteOffsetWithinHTML())
+        , m_endingByteOffsetWithinHTML(token.endingByteOffsetWithinHTML())
         , m_inferred(false)
     {
         switch (m_type) {
diff --git a/Source/core/html/parser/BackgroundHTMLParser.cpp b/Source/core/html/parser/BackgroundHTMLParser.cpp
index 57d5587..af1b148 100644
--- a/Source/core/html/parser/BackgroundHTMLParser.cpp
+++ b/Source/core/html/parser/BackgroundHTMLParser.cpp
@@ -32,8 +32,13 @@
 #include "wtf/MainThread.h"
 #include "wtf/text/TextPosition.h"
 
+#include "platform/Logging.h"
+
 namespace blink {
 
+#define MYWTF_LOG_WITH_DOCINSTNUM_AND_PARSER(channel, docInstNum, parserPtr, fmt, ...) \
+    MYWTF_LOG(channel, "doc:%u backgroundparser:%u: " fmt, docInstNum, (parserPtr)->instNum(), ##__VA_ARGS__)
+
 // On a network with high latency and high bandwidth, using a device
 // with a fast CPU, we could end up speculatively tokenizing
 // the whole document, well ahead of when the main-thread actually needs it.
@@ -94,7 +99,11 @@ BackgroundHTMLParser::BackgroundHTMLParser(PassRefPtr<WeakReference<BackgroundHT
     , m_preloadScanner(config->preloadScanner.release())
     , m_decoder(config->decoder.release())
     , m_isExtractingPageModel(config->isExtractingPageModel)
+    , m_instNum(config->instNum)
+    , m_docInstNum(config->docInstNum)
+    , m_numParsedBytes(0)
 {
+    ASSERT(m_instNum > 0);
 }
 
 BackgroundHTMLParser::~BackgroundHTMLParser()
@@ -103,6 +112,7 @@ BackgroundHTMLParser::~BackgroundHTMLParser()
 
 void BackgroundHTMLParser::appendRawBytesFromParserThread(const char* data, int dataLength)
 {
+    ASSERT_NOT_REACHED();
     ASSERT(m_decoder);
     updateDocument(m_decoder->decode(data, dataLength));
 }
@@ -209,6 +219,28 @@ void BackgroundHTMLParser::pumpTokenizer()
         }
         m_sourceTracker.end(m_input.current(), m_tokenizer.get(), *m_token);
 
+        if (m_isExtractingPageModel) {
+            if (m_token->type() != HTMLToken::EndOfFile) {
+                m_token->setStartingByteOffsetWithinHTML(m_numParsedBytes);
+
+                /* copied this length computation from
+                   HTMLSourceTracker::sourceForToken() */
+                const size_t tokenLen = static_cast<size_t>(m_token->endIndex() - m_token->startIndex());
+                m_numParsedBytes += tokenLen;
+
+                m_token->setEndingByteOffsetWithinHTML(m_numParsedBytes - 1);
+            } else {
+                MYWTF_LOG_WITH_DOCINSTNUM_AND_PARSER(
+                    PageModeling, m_docInstNum, this,
+                    "EndOfFile token after %zu parsed bytes",
+                    m_numParsedBytes);
+            }
+            // we haven't seen non-zero startindex yet, so not
+            // sure what that might mean, so assert here and we'll
+            // take a look when it happens.
+            ASSERT(m_token->startIndex() == 0);
+        }
+
         {
             TextPosition position = TextPosition(m_input.current().currentLine(), m_input.current().currentColumn());
 
diff --git a/Source/core/html/parser/BackgroundHTMLParser.h b/Source/core/html/parser/BackgroundHTMLParser.h
index 876d5bf..83563c9 100644
--- a/Source/core/html/parser/BackgroundHTMLParser.h
+++ b/Source/core/html/parser/BackgroundHTMLParser.h
@@ -45,6 +45,7 @@ class SharedBuffer;
 class XSSAuditor;
 
 class BackgroundHTMLParser {
+    WTF_MAKE_NONCOPYABLE(BackgroundHTMLParser);
     WTF_MAKE_FAST_ALLOCATED;
 public:
     struct Configuration {
@@ -55,6 +56,8 @@ public:
         OwnPtr<TextResourceDecoder> decoder;
 
         bool isExtractingPageModel;
+        int instNum;
+        int docInstNum;
     };
 
     static void start(PassRefPtr<WeakReference<BackgroundHTMLParser> >, PassOwnPtr<Configuration>);
@@ -81,6 +84,8 @@ public:
 
     void forcePlaintextForTextDocument();
 
+    inline uint32_t instNum() const { return m_instNum; }
+
 private:
     BackgroundHTMLParser(PassRefPtr<WeakReference<BackgroundHTMLParser> >, PassOwnPtr<Configuration>);
     ~BackgroundHTMLParser();
@@ -110,6 +115,9 @@ private:
     DocumentEncodingData m_lastSeenEncodingData;
 
     const bool m_isExtractingPageModel;
+    const uint32_t m_instNum;
+    const uint32_t m_docInstNum;
+    size_t m_numParsedBytes;
 };
 
 }
diff --git a/Source/core/html/parser/CompactHTMLToken.cpp b/Source/core/html/parser/CompactHTMLToken.cpp
index ea04f82..7873130 100644
--- a/Source/core/html/parser/CompactHTMLToken.cpp
+++ b/Source/core/html/parser/CompactHTMLToken.cpp
@@ -36,6 +36,9 @@ struct SameSizeAsCompactHTMLToken  {
     String data;
     Vector<Attribute> vector;
     TextPosition textPosition;
+
+    int m_startingByteOffsetWithinHTML;
+    int m_endingByteOffsetWithinHTML;
 };
 
 COMPILE_ASSERT(sizeof(CompactHTMLToken) == sizeof(SameSizeAsCompactHTMLToken), CompactHTMLToken_should_stay_small);
@@ -45,6 +48,8 @@ CompactHTMLToken::CompactHTMLToken(const HTMLToken* token, const TextPosition& t
     , m_isAll8BitData(false)
     , m_doctypeForcesQuirks(false)
     , m_textPosition(textPosition)
+    , m_startingByteOffsetWithinHTML(token->startingByteOffsetWithinHTML())
+    , m_endingByteOffsetWithinHTML(token->endingByteOffsetWithinHTML())
 {
     switch (m_type) {
     case HTMLToken::Uninitialized:
diff --git a/Source/core/html/parser/CompactHTMLToken.h b/Source/core/html/parser/CompactHTMLToken.h
index c61dec8..95c00e4 100644
--- a/Source/core/html/parser/CompactHTMLToken.h
+++ b/Source/core/html/parser/CompactHTMLToken.h
@@ -66,6 +66,24 @@ public:
     const String& systemIdentifier() const { return m_attributes[0].value; }
     bool doctypeForcesQuirks() const { return m_doctypeForcesQuirks; }
 
+    int startingByteOffsetWithinHTML() const { return m_startingByteOffsetWithinHTML; }
+    void setStartingByteOffsetWithinHTML(int offset)
+    {
+        // catch multiple sets
+        ASSERT(m_startingByteOffsetWithinHTML == -1);
+        ASSERT(offset >= 0);
+        m_startingByteOffsetWithinHTML = offset;
+    }
+
+    int endingByteOffsetWithinHTML() const { return m_endingByteOffsetWithinHTML; }
+    void setEndingByteOffsetWithinHTML(int offset)
+    {
+        // catch multiple sets
+        ASSERT(m_endingByteOffsetWithinHTML == -1);
+        ASSERT(offset >= 0);
+        m_endingByteOffsetWithinHTML = offset;
+    }
+
 private:
     unsigned m_type : 4;
     unsigned m_selfClosing : 1;
@@ -75,6 +93,9 @@ private:
     String m_data; // "name", "characters", or "data" depending on m_type
     Vector<Attribute> m_attributes;
     TextPosition m_textPosition;
+
+    int m_startingByteOffsetWithinHTML;
+    int m_endingByteOffsetWithinHTML;
 };
 
 typedef Vector<CompactHTMLToken> CompactHTMLTokenStream;
diff --git a/Source/core/html/parser/HTMLDocumentParser.cpp b/Source/core/html/parser/HTMLDocumentParser.cpp
index 3b52ed7..6c68906 100644
--- a/Source/core/html/parser/HTMLDocumentParser.cpp
+++ b/Source/core/html/parser/HTMLDocumentParser.cpp
@@ -58,6 +58,18 @@ using namespace HTMLNames;
 static const char start_preloadscanner_msg[] = "tell preloadscanner to scan";
 static const char preloadscanner_done_msg[] = "preloadscanner done scanning";
 
+/* background parser is in another thread, so we can't call its
+ * functions directly, so instead of letting it pick its next instnum,
+ * we'll have the parser pick it here and pass into the background
+ * parser's constructor
+ */
+static uint32_t nextBackgroundHTMLParserInstNum()
+{
+    static uint32_t next = 1;
+    ASSERT(next <= 0xfffff);
+    return ++next;
+}
+
 // This is a direct transcription of step 4 from:
 // http://www.whatwg.org/specs/web-apps/current-work/multipage/the-end.html#fragment-case
 static HTMLTokenizer::State tokenizerStateForContextElement(Element* contextElement, bool reportErrors, const HTMLParserOptions& options)
@@ -88,6 +100,7 @@ public:
     explicit ParserDataReceiver(WeakPtr<BackgroundHTMLParser> backgroundParser)
         : m_backgroundParser(backgroundParser)
     {
+        ASSERT_NOT_REACHED();
     }
 
     // WebThreadedDataReceiver
@@ -437,6 +450,8 @@ void HTMLDocumentParser::validateSpeculations(PassOwnPtr<ParsedChunk> chunk)
 
 void HTMLDocumentParser::discardSpeculationsAndResumeFrom(PassOwnPtr<ParsedChunk> lastChunkBeforeScript, PassOwnPtr<HTMLToken> token, PassOwnPtr<HTMLTokenizer> tokenizer)
 {
+    ASSERT(!m_isExtractingPageModel);
+
     m_weakFactory.revokeAll();
     m_speculations.clear();
 
@@ -839,6 +854,13 @@ void HTMLDocumentParser::startBackgroundParser()
     config->preloadScanner = adoptPtr(new TokenPreloadScanner(document()->url().copy(), createMediaValues(document())));
     config->decoder = takeDecoder();
     config->isExtractingPageModel = m_isExtractingPageModel;
+    config->instNum = m_backgroundParserInstNum = nextBackgroundHTMLParserInstNum();
+    config->docInstNum = document()->instNum();
+
+    if (m_isExtractingPageModel) {
+        MYWTF_LOG_WITH_DOC_AND_PARSER(PageModeling, document(), this,
+            "started backgroundparser:%u", m_backgroundParserInstNum);
+    }
 
     ASSERT(config->xssAuditor->isSafeToSendToAnotherThread());
     ASSERT(config->preloadScanner->isSafeToSendToAnotherThread());
@@ -850,6 +872,13 @@ void HTMLDocumentParser::stopBackgroundParser()
     ASSERT(shouldUseThreading());
     ASSERT(m_haveBackgroundParser);
     m_haveBackgroundParser = false;
+    m_backgroundParserInstNum = 0;
+
+    if (m_isExtractingPageModel) {
+        MYWTF_LOG_WITH_DOC_AND_PARSER(
+            PageModeling, document(), this,
+            "stopped backgroundparser:%u", m_backgroundParserInstNum);
+    }
 
     HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::stop, m_backgroundParser));
     m_weakFactory.revokeAll();
-- 
2.1.4


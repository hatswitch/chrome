From b0f51e31d0d371b8a16a869fb829e74961adcf68 Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Thu, 17 Mar 2016 17:00:03 -0500
Subject: [PATCH 101/101] enable more accurate tracking of location of the code
 that posts Tasks. with this, message_loop.cc can log more accurate info.
 (webkit side commit)

---
 Source/core/dom/Document.cpp                     | 11 +++++---
 Source/core/dom/MainThreadTaskRunner.cpp         | 16 ++++++++++--
 Source/core/frame/DOMTimer.cpp                   |  2 +-
 Source/core/html/parser/BackgroundHTMLParser.cpp |  6 +++--
 Source/core/html/parser/HTMLDocumentParser.cpp   | 33 ++++++++++++++++--------
 Source/core/html/parser/HTMLParserThread.cpp     | 10 ++++---
 Source/core/html/parser/HTMLParserThread.h       |  3 ++-
 Source/core/html/parser/HTMLResourcePreloader.h  |  6 +++--
 Source/core/inspector/TraceEventDispatcher.cpp   |  5 +++-
 Source/modules/webaudio/AsyncAudioDecoder.cpp    |  5 +++-
 Source/modules/webaudio/AudioContext.cpp         | 11 ++++++--
 Source/platform/SharedBuffer.cpp                 |  4 ++-
 Source/platform/TraceLocation.h                  |  8 ++++--
 Source/platform/blob/BlobRegistry.cpp            | 24 ++++++++++++-----
 Source/platform/exported/WebSchedulerProxy.cpp   |  4 +++
 Source/platform/fonts/mac/FontCacheMac.mm        |  2 +-
 Source/platform/scheduler/Scheduler.cpp          | 21 +++++++++++++--
 Source/web/WebKit.cpp                            |  6 +++--
 Source/web/WebSharedWorkerImpl.cpp               |  8 +++---
 Source/wtf/MainThread.cpp                        | 16 +++++++++---
 Source/wtf/MainThread.h                          | 10 +++++--
 Source/wtf/text/StringImpl.cpp                   |  5 +++-
 Source/wtf/text/StringImplCF.cpp                 |  4 ++-
 public/platform/Platform.h                       |  2 ++
 public/platform/WebThread.h                      |  8 +++++-
 25 files changed, 174 insertions(+), 56 deletions(-)

diff --git a/Source/core/dom/Document.cpp b/Source/core/dom/Document.cpp
index 8438c9c..9b044d6 100644
--- a/Source/core/dom/Document.cpp
+++ b/Source/core/dom/Document.cpp
@@ -576,16 +576,17 @@ Document::Document(const DocumentInit& initializer, DocumentClassFlags documentC
     }
 
     if (m_extractingPageModel) {
+        ASSERT(isMainThread());
         MYWTF_LOG_WITH_DOC(
             PageModeling, this,
-            "document %p instNum %u constructor. url: [%s] parentDoc:%u ownerDoc:%u contextDoc:%u"
-            "resource fetcher %p",
+            "document %p instNum %u constructor. url: [%s] parentDoc:%u ownerDoc:%u contextDoc:%u "
+            "resourceFetcher:%u",
             this, instNum(),
             url().string().ascii().data(),
             initializer.parent() ? initializer.parent()->instNum() : 0,
             initializer.owner() ? initializer.owner()->instNum() : 0,
             m_contextDocument.get() ? m_contextDocument->instNum() : 0,
-            m_fetcher.get());
+            m_fetcher->instNum());
 
         // we need to understand other cases
         ASSERT(initializer.parent() == initializer.owner());
@@ -966,6 +967,8 @@ ScriptValue Document::registerElement(ScriptState* scriptState, const AtomicStri
 
 CustomElementMicrotaskRunQueue* Document::customElementMicrotaskRunQueue()
 {
+    ASSERT(!m_extractingPageModel);
+
     if (!m_customElementMicrotaskRunQueue)
         m_customElementMicrotaskRunQueue = CustomElementMicrotaskRunQueue::create();
     return m_customElementMicrotaskRunQueue.get();
@@ -5352,6 +5355,8 @@ void Document::postTask(PassOwnPtr<ExecutionContextTask> task)
 
 void Document::postInspectorTask(PassOwnPtr<ExecutionContextTask> task)
 {
+    ASSERT(!m_extractingPageModel);
+
     m_taskRunner->postInspectorTask(task);
 }
 
diff --git a/Source/core/dom/MainThreadTaskRunner.cpp b/Source/core/dom/MainThreadTaskRunner.cpp
index c89b243..2fab721 100644
--- a/Source/core/dom/MainThreadTaskRunner.cpp
+++ b/Source/core/dom/MainThreadTaskRunner.cpp
@@ -81,12 +81,24 @@ void MainThreadTaskRunner::postTask(PassOwnPtr<ExecutionContextTask> task)
 {
     if (!task->taskNameForInstrumentation().isEmpty())
         InspectorInstrumentation::didPostExecutionContextTask(m_context, task.get());
-    callOnMainThread(PerformTaskContext::didReceiveTask, new PerformTaskContext(m_weakFactory.createWeakPtr(), task, false));
+    // callOnMainThread(PerformTaskContext::didReceiveTask, new PerformTaskContext(m_weakFactory.createWeakPtr(), task, false));
+
+    if (m_context && m_context->isDocument() && toDocument(m_context)->isExtractingPageModel()) {
+        ASSERT_NOT_REACHED();
+    }
+    static const blink::TraceLocation here = FROM_HERE;
+    callOnMainThread(PerformTaskContext::didReceiveTask, new PerformTaskContext(m_weakFactory.createWeakPtr(), task, false), &here);
 }
 
 void MainThreadTaskRunner::postInspectorTask(PassOwnPtr<ExecutionContextTask> task)
 {
-    callOnMainThread(PerformTaskContext::didReceiveTask, new PerformTaskContext(m_weakFactory.createWeakPtr(), task, true));
+    // callOnMainThread(PerformTaskContext::didReceiveTask, new PerformTaskContext(m_weakFactory.createWeakPtr(), task, true));
+
+    if (m_context && m_context->isDocument() && toDocument(m_context)->isExtractingPageModel()) {
+        ASSERT_NOT_REACHED();
+    }
+    static const blink::TraceLocation here = FROM_HERE;
+    callOnMainThread(PerformTaskContext::didReceiveTask, new PerformTaskContext(m_weakFactory.createWeakPtr(), task, true), &here);
 }
 
 void MainThreadTaskRunner::perform(PassOwnPtr<ExecutionContextTask> task, bool isInspectorTask)
diff --git a/Source/core/frame/DOMTimer.cpp b/Source/core/frame/DOMTimer.cpp
index 0d994ae..6303d47 100644
--- a/Source/core/frame/DOMTimer.cpp
+++ b/Source/core/frame/DOMTimer.cpp
@@ -111,7 +111,7 @@ DOMTimer::DOMTimer(ExecutionContext* context, PassOwnPtr<ScheduledAction> action
         if (document->isExtractingPageModel()) {
             MYWTF_LOG_WITH_DOC(
                 PageModeling, document,
-                "timerID:%d created, interval %llu, singleShot= %d, scheduled action:%u",
+                "timerID:%d created, interval %llu, singleShot= %d, scheduledAction:%u",
                 m_timeoutID, (unsigned long long)(intervalMilliseconds * 1000),
                 singleShot, m_action->instNum());
             // still have to handle repeating timers
diff --git a/Source/core/html/parser/BackgroundHTMLParser.cpp b/Source/core/html/parser/BackgroundHTMLParser.cpp
index af1b148..cd21bde 100644
--- a/Source/core/html/parser/BackgroundHTMLParser.cpp
+++ b/Source/core/html/parser/BackgroundHTMLParser.cpp
@@ -150,7 +150,8 @@ void BackgroundHTMLParser::updateDocument(const String& decodedData)
         m_lastSeenEncodingData = encodingData;
 
         m_xssAuditor->setEncoding(encodingData.encoding());
-        callOnMainThread(bind(&HTMLDocumentParser::didReceiveEncodingDataFromBackgroundParser, m_parser, encodingData));
+        // callOnMainThread(bind(&HTMLDocumentParser::didReceiveEncodingDataFromBackgroundParser, m_parser, encodingData));
+        callOnMainThreadWithLocation(bind(&HTMLDocumentParser::didReceiveEncodingDataFromBackgroundParser, m_parser, encodingData), FROM_HERE);
     }
 
     if (decodedData.isEmpty())
@@ -286,7 +287,8 @@ void BackgroundHTMLParser::sendTokensToMainThread()
     chunk->inputCheckpoint = m_input.createCheckpoint(m_pendingTokens->size());
     chunk->preloadScannerCheckpoint = m_preloadScanner->createCheckpoint();
     chunk->tokens = m_pendingTokens.release();
-    callOnMainThread(bind(&HTMLDocumentParser::didReceiveParsedChunkFromBackgroundParser, m_parser, chunk.release()));
+    // callOnMainThread(bind(&HTMLDocumentParser::didReceiveParsedChunkFromBackgroundParser, m_parser, chunk.release()));
+    callOnMainThreadWithLocation(bind(&HTMLDocumentParser::didReceiveParsedChunkFromBackgroundParser, m_parser, chunk.release()), FROM_HERE);
 
     m_pendingTokens = adoptPtr(new CompactHTMLTokenStream);
 }
diff --git a/Source/core/html/parser/HTMLDocumentParser.cpp b/Source/core/html/parser/HTMLDocumentParser.cpp
index 6c68906..c4bbc47 100644
--- a/Source/core/html/parser/HTMLDocumentParser.cpp
+++ b/Source/core/html/parser/HTMLDocumentParser.cpp
@@ -466,7 +466,8 @@ void HTMLDocumentParser::discardSpeculationsAndResumeFrom(PassOwnPtr<ParsedChunk
     m_input.current().clear(); // FIXME: This should be passed in instead of cleared.
 
     ASSERT(checkpoint->unparsedInput.isSafeToSendToAnotherThread());
-    HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::resumeFrom, m_backgroundParser, checkpoint.release()));
+    static const blink::TraceLocation here = FROM_HERE;
+    HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::resumeFrom, m_backgroundParser, checkpoint.release()), &here);
 }
 
 void HTMLDocumentParser::processParsedChunkFromBackgroundParser(PassOwnPtr<ParsedChunk> popChunk)
@@ -491,7 +492,8 @@ void HTMLDocumentParser::processParsedChunkFromBackgroundParser(PassOwnPtr<Parse
     OwnPtr<ParsedChunk> chunk(popChunk);
     OwnPtr<CompactHTMLTokenStream> tokens = chunk->tokens.release();
 
-    HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::startedChunkWithCheckpoint, m_backgroundParser, chunk->inputCheckpoint));
+    static const blink::TraceLocation here = FROM_HERE;
+    HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::startedChunkWithCheckpoint, m_backgroundParser, chunk->inputCheckpoint), &here);
 
     for (XSSInfoStream::const_iterator it = chunk->xssInfos.begin(); it != chunk->xssInfos.end(); ++it) {
         m_textPosition = (*it)->m_textPosition;
@@ -596,7 +598,8 @@ void HTMLDocumentParser::forcePlaintextForTextDocument()
         if (!m_haveBackgroundParser)
             startBackgroundParser();
 
-        HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::forcePlaintextForTextDocument, m_backgroundParser));
+        static const blink::TraceLocation here = FROM_HERE;
+        HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::forcePlaintextForTextDocument, m_backgroundParser), &here);
     } else
         m_tokenizer->setState(HTMLTokenizer::PLAINTEXTState);
 }
@@ -864,7 +867,8 @@ void HTMLDocumentParser::startBackgroundParser()
 
     ASSERT(config->xssAuditor->isSafeToSendToAnotherThread());
     ASSERT(config->preloadScanner->isSafeToSendToAnotherThread());
-    HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::start, reference.release(), config.release()));
+    static const blink::TraceLocation here = FROM_HERE;
+    HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::start, reference.release(), config.release()), &here);
 }
 
 void HTMLDocumentParser::stopBackgroundParser()
@@ -880,7 +884,8 @@ void HTMLDocumentParser::stopBackgroundParser()
             "stopped backgroundparser:%u", m_backgroundParserInstNum);
     }
 
-    HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::stop, m_backgroundParser));
+    static const blink::TraceLocation here = FROM_HERE;
+    HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::stop, m_backgroundParser), &here);
     m_weakFactory.revokeAll();
 }
 
@@ -1020,7 +1025,8 @@ void HTMLDocumentParser::finish()
     if (m_haveBackgroundParser) {
         if (!m_input.haveSeenEndOfFile())
             m_input.closeWithoutMarkingEndOfFile();
-        HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::finish, m_backgroundParser));
+        static const blink::TraceLocation here = FROM_HERE;
+        HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::finish, m_backgroundParser), &here);
         return;
     }
 
@@ -1199,7 +1205,8 @@ void HTMLDocumentParser::appendBytes(const char* data, size_t length)
         memcpy(buffer->data(), data, length);
         TRACE_EVENT1("net", "HTMLDocumentParser::appendBytes", "size", (unsigned)length);
 
-        HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::appendRawBytesFromMainThread, m_backgroundParser, buffer.release()));
+        static const blink::TraceLocation here = FROM_HERE;
+        HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::appendRawBytesFromMainThread, m_backgroundParser, buffer.release()), &here);
         return;
     }
 
@@ -1212,8 +1219,10 @@ void HTMLDocumentParser::flush()
     if (isDetached() || needsDecoder())
         return;
 
-    if (m_haveBackgroundParser)
-        HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::flush, m_backgroundParser));
+    if (m_haveBackgroundParser) {
+        static const blink::TraceLocation here = FROM_HERE;
+        HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::flush, m_backgroundParser), &here);
+    }
     else
         DecodedDataDocumentParser::flush();
 }
@@ -1223,8 +1232,10 @@ void HTMLDocumentParser::setDecoder(PassOwnPtr<TextResourceDecoder> decoder)
     ASSERT(decoder);
     DecodedDataDocumentParser::setDecoder(decoder);
 
-    if (m_haveBackgroundParser)
-        HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::setDecoder, m_backgroundParser, takeDecoder()));
+    if (m_haveBackgroundParser) {
+        static const blink::TraceLocation here = FROM_HERE;
+        HTMLParserThread::shared()->postTask(bind(&BackgroundHTMLParser::setDecoder, m_backgroundParser, takeDecoder()), &here);
+    }
 }
 
 }
diff --git a/Source/core/html/parser/HTMLParserThread.cpp b/Source/core/html/parser/HTMLParserThread.cpp
index aeaca7d..e568c3d 100644
--- a/Source/core/html/parser/HTMLParserThread.cpp
+++ b/Source/core/html/parser/HTMLParserThread.cpp
@@ -95,7 +95,8 @@ blink::WebThread& HTMLParserThread::platformThread()
 {
     if (!isRunning()) {
         m_thread = adoptPtr(blink::Platform::current()->createThread("HTMLParserThread"));
-        postTask(WTF::bind(&HTMLParserThread::setupHTMLParserThread, this));
+        static const blink::TraceLocation here = FROM_HERE;
+        postTask(WTF::bind(&HTMLParserThread::setupHTMLParserThread, this), &here);
     }
     return *m_thread;
 }
@@ -105,9 +106,12 @@ bool HTMLParserThread::isRunning()
     return !!m_thread;
 }
 
-void HTMLParserThread::postTask(const Closure& closure)
+void HTMLParserThread::postTask(const Closure& closure, const blink::TraceLocation* location)
 {
-    platformThread().postTask(new Task(closure));
+    platformThread().postTask(new Task(closure),
+                              location ? location->fileName() : NULL,
+                              location ? location->functionName() : NULL,
+                              location ? location->lineNumber() : -1);
 }
 
 } // namespace blink
diff --git a/Source/core/html/parser/HTMLParserThread.h b/Source/core/html/parser/HTMLParserThread.h
index 8504d56..54c5013 100644
--- a/Source/core/html/parser/HTMLParserThread.h
+++ b/Source/core/html/parser/HTMLParserThread.h
@@ -36,6 +36,7 @@
 #include "public/platform/WebThread.h"
 #include "wtf/Functional.h"
 #include "wtf/OwnPtr.h"
+#include "platform/TraceLocation.h"
 
 namespace blink {
 
@@ -49,7 +50,7 @@ public:
     // It is an error to call shared() before init() or after shutdown();
     static HTMLParserThread* shared();
 
-    void postTask(const Closure&);
+    void postTask(const Closure&, const blink::TraceLocation* location=NULL);
     blink::WebThread& platformThread();
     bool isRunning();
 
diff --git a/Source/core/html/parser/HTMLResourcePreloader.h b/Source/core/html/parser/HTMLResourcePreloader.h
index fe2cb27..c6890b3 100644
--- a/Source/core/html/parser/HTMLResourcePreloader.h
+++ b/Source/core/html/parser/HTMLResourcePreloader.h
@@ -68,8 +68,10 @@ private:
         , m_allowCredentials(DoNotAllowStoredCredentials)
         , m_discoveryTime(monotonicallyIncreasingTime())
     {
-        MYWTF_LOG(PageModeling, "new PreloadRequest baseURL [%s] resourceURL [%s]",
-                  baseURL.string().ascii().data(), resourceURL.ascii().data());
+        /* if (!KURL(resourceURL.protocolIsData()) { */
+        /*     MYWTF_LOG(PageModeling, "new PreloadRequest baseURL [%s] resourceURL [%s]", */
+        /*               baseURL.string().ascii().data(), resourceURL.ascii().data()); */
+        /* } */
     }
 
     KURL completeURL(Document*);
diff --git a/Source/core/inspector/TraceEventDispatcher.cpp b/Source/core/inspector/TraceEventDispatcher.cpp
index 110cbcb..860364e 100644
--- a/Source/core/inspector/TraceEventDispatcher.cpp
+++ b/Source/core/inspector/TraceEventDispatcher.cpp
@@ -64,7 +64,10 @@ void TraceEventDispatcher::enqueueEvent(const TraceEvent& event)
             return;
     }
     m_processEventsTaskInFlight = true;
-    callOnMainThread(bind(&TraceEventDispatcher::processBackgroundEventsTask, this));
+    // callOnMainThread(bind(&TraceEventDispatcher::processBackgroundEventsTask, this));
+
+    ASSERT_NOT_REACHED();
+    callOnMainThreadWithLocation(bind(&TraceEventDispatcher::processBackgroundEventsTask, this), FROM_HERE);
 }
 
 void TraceEventDispatcher::processBackgroundEventsTask()
diff --git a/Source/modules/webaudio/AsyncAudioDecoder.cpp b/Source/modules/webaudio/AsyncAudioDecoder.cpp
index fc678cb..6c28e36 100644
--- a/Source/modules/webaudio/AsyncAudioDecoder.cpp
+++ b/Source/modules/webaudio/AsyncAudioDecoder.cpp
@@ -69,7 +69,10 @@ void AsyncAudioDecoder::decode(ArrayBuffer* audioData, float sampleRate, AudioBu
 
     // Decoding is finished, but we need to do the callbacks on the main thread.
     // The leaked reference to audioBuffer is picked up in notifyComplete.
-    callOnMainThread(WTF::bind(&AsyncAudioDecoder::notifyComplete, audioData, successCallback, errorCallback, bus.release().leakRef()));
+    // callOnMainThread(WTF::bind(&AsyncAudioDecoder::notifyComplete, audioData, successCallback, errorCallback, bus.release().leakRef()));
+
+    ASSERT_NOT_REACHED();
+    callOnMainThreadWithLocation(WTF::bind(&AsyncAudioDecoder::notifyComplete, audioData, successCallback, errorCallback, bus.release().leakRef()), FROM_HERE);
 }
 
 void AsyncAudioDecoder::notifyComplete(ArrayBuffer* audioData, AudioBufferCallback* successCallback, AudioBufferCallback* errorCallback, AudioBus* audioBus)
diff --git a/Source/modules/webaudio/AudioContext.cpp b/Source/modules/webaudio/AudioContext.cpp
index 97000f9..13574f4 100644
--- a/Source/modules/webaudio/AudioContext.cpp
+++ b/Source/modules/webaudio/AudioContext.cpp
@@ -254,7 +254,10 @@ void AudioContext::stop()
     // of dealing with all of its ActiveDOMObjects at this point. uninitialize() can de-reference other
     // ActiveDOMObjects so let's schedule uninitialize() to be called later.
     // FIXME: see if there's a more direct way to handle this issue.
-    callOnMainThread(bind(&AudioContext::uninitialize, PassRefPtrWillBeRawPtr<AudioContext>(this)));
+    // callOnMainThread(bind(&AudioContext::uninitialize, PassRefPtrWillBeRawPtr<AudioContext>(this)));
+
+    ASSERT_NOT_REACHED();
+    callOnMainThreadWithLocation(bind(&AudioContext::uninitialize, PassRefPtrWillBeRawPtr<AudioContext>(this)), FROM_HERE);
 }
 
 bool AudioContext::hasPendingActivity() const
@@ -822,7 +825,11 @@ void AudioContext::scheduleNodeDeletion()
         // Don't let ourself get deleted before the callback.
         // See matching deref() in deleteMarkedNodesDispatch().
         ref();
-        callOnMainThread(deleteMarkedNodesDispatch, this);
+        // callOnMainThread(deleteMarkedNodesDispatch, this);
+
+        ASSERT_NOT_REACHED();
+        static const blink::TraceLocation here = FROM_HERE;
+        callOnMainThread(deleteMarkedNodesDispatch, this, &here);
     }
 }
 
diff --git a/Source/platform/SharedBuffer.cpp b/Source/platform/SharedBuffer.cpp
index ab4b747..60ca9d6 100644
--- a/Source/platform/SharedBuffer.cpp
+++ b/Source/platform/SharedBuffer.cpp
@@ -128,7 +128,9 @@ static void didCreateSharedBuffer(SharedBuffer* buffer)
     MutexLocker locker(statsMutex());
     liveBuffers().add(buffer);
 
-    callOnMainThread(printStats, 0);
+    // callOnMainThread(printStats, 0);
+    ASSERT_NOT_REACHED();
+    callOnMainThreadWithLocation(printStats, 0, FROM_HERE);
 }
 
 static void willDestroySharedBuffer(SharedBuffer* buffer)
diff --git a/Source/platform/TraceLocation.h b/Source/platform/TraceLocation.h
index 91513a9..e0573f8 100644
--- a/Source/platform/TraceLocation.h
+++ b/Source/platform/TraceLocation.h
@@ -15,25 +15,29 @@ class TraceLocation {
 public:
     // Currenetly only store the bits used in Blink, base::Location stores more.
     // These char*s are not copied and must live for the duration of the program.
-    TraceLocation(const char* functionName, const char* fileName)
+    TraceLocation(const char* functionName, const char* fileName, const int lineNumber=-1)
         : m_functionName(functionName)
         , m_fileName(fileName)
+        , m_lineNumber(lineNumber)
     { }
 
     TraceLocation()
         : m_functionName("unknown")
         , m_fileName("unknown")
+        , m_lineNumber(-1)
     { }
 
     const char* functionName() const { return m_functionName; }
     const char* fileName() const { return m_fileName; }
+    int lineNumber() const { return m_lineNumber; }
 
 private:
     const char* m_functionName;
     const char* m_fileName;
+    int m_lineNumber;
 };
 
-#define FROM_HERE ::blink::TraceLocation(__FUNCTION__, __FILE__)
+#define FROM_HERE ::blink::TraceLocation(__FUNCTION__, __FILE__, __LINE__)
 } // namespace blink
 
 #endif // TraceLocation_h
diff --git a/Source/platform/blob/BlobRegistry.cpp b/Source/platform/blob/BlobRegistry.cpp
index 48b0f11..8d17dc7 100644
--- a/Source/platform/blob/BlobRegistry.cpp
+++ b/Source/platform/blob/BlobRegistry.cpp
@@ -169,7 +169,9 @@ void BlobRegistry::registerStreamURL(const KURL& url, const String& type)
             registry->registerStreamURL(url, type);
     } else {
         OwnPtr<BlobRegistryContext> context = adoptPtr(new BlobRegistryContext(url, type));
-        callOnMainThread(&registerStreamURLTask, context.leakPtr());
+        ASSERT_NOT_REACHED();
+        static const blink::TraceLocation here = FROM_HERE;
+        callOnMainThread(&registerStreamURLTask, context.leakPtr(), &here);
     }
 }
 
@@ -189,7 +191,9 @@ void BlobRegistry::registerStreamURL(SecurityOrigin* origin, const KURL& url, co
             registry->registerStreamURL(url, srcURL);
     } else {
         OwnPtr<BlobRegistryContext> context = adoptPtr(new BlobRegistryContext(url, srcURL));
-        callOnMainThread(&registerStreamURLFromTask, context.leakPtr());
+        ASSERT_NOT_REACHED();
+        static const blink::TraceLocation here = FROM_HERE;
+        callOnMainThread(&registerStreamURLFromTask, context.leakPtr(), &here);
     }
 }
 
@@ -211,7 +215,9 @@ void BlobRegistry::addDataToStream(const KURL& url, PassRefPtr<RawData> streamDa
         }
     } else {
         OwnPtr<BlobRegistryContext> context = adoptPtr(new BlobRegistryContext(url, streamData));
-        callOnMainThread(&addDataToStreamTask, context.leakPtr());
+        ASSERT_NOT_REACHED();
+        static const blink::TraceLocation here = FROM_HERE;
+        callOnMainThread(&addDataToStreamTask, context.leakPtr(), &here);
     }
 }
 
@@ -229,7 +235,9 @@ void BlobRegistry::finalizeStream(const KURL& url)
             registry->finalizeStream(url);
     } else {
         OwnPtr<BlobRegistryContext> context = adoptPtr(new BlobRegistryContext(url));
-        callOnMainThread(&finalizeStreamTask, context.leakPtr());
+        ASSERT_NOT_REACHED();
+        static const blink::TraceLocation here = FROM_HERE;
+        callOnMainThread(&finalizeStreamTask, context.leakPtr(), &here);
     }
 }
 
@@ -247,7 +255,9 @@ void BlobRegistry::abortStream(const KURL& url)
             registry->abortStream(url);
     } else {
         OwnPtr<BlobRegistryContext> context = adoptPtr(new BlobRegistryContext(url));
-        callOnMainThread(&abortStreamTask, context.leakPtr());
+        ASSERT_NOT_REACHED();
+        static const blink::TraceLocation here = FROM_HERE;
+        callOnMainThread(&abortStreamTask, context.leakPtr(), &here);
     }
 }
 
@@ -267,7 +277,9 @@ void BlobRegistry::unregisterStreamURL(const KURL& url)
             registry->unregisterStreamURL(url);
     } else {
         OwnPtr<BlobRegistryContext> context = adoptPtr(new BlobRegistryContext(url));
-        callOnMainThread(&unregisterStreamURLTask, context.leakPtr());
+        ASSERT_NOT_REACHED();
+        static const blink::TraceLocation here = FROM_HERE;
+        callOnMainThread(&unregisterStreamURLTask, context.leakPtr(), &here);
     }
 }
 
diff --git a/Source/platform/exported/WebSchedulerProxy.cpp b/Source/platform/exported/WebSchedulerProxy.cpp
index 016abb0..7245505 100644
--- a/Source/platform/exported/WebSchedulerProxy.cpp
+++ b/Source/platform/exported/WebSchedulerProxy.cpp
@@ -39,12 +39,16 @@ WebSchedulerProxy::~WebSchedulerProxy()
 
 void WebSchedulerProxy::postInputTask(const WebTraceLocation& webLocation, WebThread::Task* task)
 {
+    ASSERT_NOT_REACHED();
+
     TraceLocation location(webLocation.functionName(), webLocation.fileName());
     m_scheduler->postInputTask(location, bind(&runTask, adoptPtr(task)));
 }
 
 void WebSchedulerProxy::postCompositorTask(const WebTraceLocation& webLocation, WebThread::Task* task)
 {
+    ASSERT_NOT_REACHED();
+
     TraceLocation location(webLocation.functionName(), webLocation.fileName());
     m_scheduler->postCompositorTask(location, bind(&runTask, adoptPtr(task)));
 }
diff --git a/Source/platform/fonts/mac/FontCacheMac.mm b/Source/platform/fonts/mac/FontCacheMac.mm
index 2abefa3..f454eaf 100644
--- a/Source/platform/fonts/mac/FontCacheMac.mm
+++ b/Source/platform/fonts/mac/FontCacheMac.mm
@@ -54,7 +54,7 @@ namespace blink {
 static void invalidateFontCache(void*)
 {
     if (!isMainThread()) {
-        callOnMainThread(&invalidateFontCache, 0);
+        callOnMainThreadWithLocation(&invalidateFontCache, 0, FROM_HERE);
         return;
     }
     FontCache::fontCache()->invalidate();
diff --git a/Source/platform/scheduler/Scheduler.cpp b/Source/platform/scheduler/Scheduler.cpp
index 39cb5ca..c9ec220 100644
--- a/Source/platform/scheduler/Scheduler.cpp
+++ b/Source/platform/scheduler/Scheduler.cpp
@@ -13,7 +13,6 @@
 #include "wtf/MainThread.h"
 #include "wtf/ThreadingPrimitives.h"
 
-
 namespace blink {
 
 namespace {
@@ -27,6 +26,7 @@ public:
         , m_allottedTimeMs(allottedTimeMs)
         , m_location(location)
     {
+        ASSERT_NOT_REACHED();
     }
 
     // WebThread::Task implementation.
@@ -53,6 +53,7 @@ public:
     MainThreadPendingHighPriorityTaskRunner()
     {
         ASSERT(Scheduler::shared());
+        ASSERT_NOT_REACHED();
     }
 
     // WebThread::Task implementation.
@@ -116,6 +117,12 @@ Scheduler::Scheduler()
     , m_mainThread(blink::Platform::current()->currentThread())
     , m_highPriorityTaskCount(0)
 {
+    // MYWTF_LOG(PageModeling, "scheduler %p created, mainthread %p", this, m_mainThread);
+    // XXX/now sure why m_mainThread would be NULL here, but it does
+    // happen. the backtrace doesn't help much.
+    if (m_mainThread) {
+        m_mainThread->set_thread_name("MyWebKitMainThread");
+    }
 }
 
 Scheduler::~Scheduler()
@@ -128,16 +135,23 @@ Scheduler::~Scheduler()
 void Scheduler::scheduleIdleTask(const TraceLocation& location, const IdleTask& idleTask)
 {
     // TODO: send a real allottedTime here.
+    ASSERT_NOT_REACHED();
     m_mainThread->postTask(new MainThreadIdleTaskAdapter(idleTask, 0, location));
 }
 
 void Scheduler::postTask(const TraceLocation& location, const Task& task)
 {
-    m_mainThread->postTask(new MainThreadPendingTaskRunner(task, location));
+    // m_mainThread->postTask(new MainThreadPendingTaskRunner(task, location));
+
+    MainThreadPendingTaskRunner *tr = new MainThreadPendingTaskRunner(task, location);
+    // MYWTF_LOG(PageModeling, "posting task %p by [%s:%d, %s()] on mainThread %p",
+    //           tr, location.fileName(), location.lineNumber(), location.functionName(), m_mainThread);
+    m_mainThread->postTask(tr, location.fileName(), location.functionName(), location.lineNumber());
 }
 
 void Scheduler::postInputTask(const TraceLocation& location, const Task& task)
 {
+    ASSERT_NOT_REACHED();
     Locker<Mutex> lock(m_pendingTasksMutex);
     m_pendingInputTasks.append(TracedTask(task, location));
     atomicIncrement(&m_highPriorityTaskCount);
@@ -146,6 +160,7 @@ void Scheduler::postInputTask(const TraceLocation& location, const Task& task)
 
 void Scheduler::postCompositorTask(const TraceLocation& location, const Task& task)
 {
+    ASSERT_NOT_REACHED();
     Locker<Mutex> lock(m_pendingTasksMutex);
     m_pendingCompositorTasks.append(TracedTask(task, location));
     atomicIncrement(&m_highPriorityTaskCount);
@@ -184,11 +199,13 @@ void Scheduler::runHighPriorityTasks()
 
     int highPriorityTasksExecuted = 0;
     while (!inputTasks.isEmpty()) {
+        ASSERT_NOT_REACHED();
         inputTasks.takeFirst().run();
         highPriorityTasksExecuted++;
     }
 
     while (!compositorTasks.isEmpty()) {
+        ASSERT_NOT_REACHED();
         compositorTasks.takeFirst().run();
         highPriorityTasksExecuted++;
     }
diff --git a/Source/web/WebKit.cpp b/Source/web/WebKit.cpp
index 1c8ac99..9ced5fe 100644
--- a/Source/web/WebKit.cpp
+++ b/Source/web/WebKit.cpp
@@ -145,9 +145,11 @@ static void cryptographicallyRandomValues(unsigned char* buffer, size_t length)
     Platform::current()->cryptographicallyRandomValues(buffer, length);
 }
 
-static void callOnMainThreadFunction(WTF::MainThreadFunction function, void* context)
+static void callOnMainThreadFunction(WTF::MainThreadFunction function, void* context, const TraceLocation* location=NULL)
 {
-    Scheduler::shared()->postTask(FROM_HERE, bind(function, context));
+    // Scheduler::shared()->postTask(FROM_HERE, bind(function, context));
+
+    Scheduler::shared()->postTask(location ? *location : FROM_HERE, bind(function, context));
 }
 
 void initializeWithoutV8(Platform* platform)
diff --git a/Source/web/WebSharedWorkerImpl.cpp b/Source/web/WebSharedWorkerImpl.cpp
index 49bb895..eede649 100644
--- a/Source/web/WebSharedWorkerImpl.cpp
+++ b/Source/web/WebSharedWorkerImpl.cpp
@@ -253,7 +253,7 @@ void WebSharedWorkerImpl::postMessageToPageInspector(const String& message)
     // that the temporary created by isolatedCopy() will always be destroyed
     // before the copy in the closure is used on the main thread.
     const Closure& boundFunction = bind(&WebSharedWorkerClient::dispatchDevToolsMessage, m_clientWeakPtr, message.isolatedCopy());
-    callOnMainThread(boundFunction);
+    callOnMainThreadWithLocation(boundFunction, FROM_HERE);
 }
 
 void WebSharedWorkerImpl::updateInspectorStateCookie(const String& cookie)
@@ -262,12 +262,12 @@ void WebSharedWorkerImpl::updateInspectorStateCookie(const String& cookie)
     // that the temporary created by isolatedCopy() will always be destroyed
     // before the copy in the closure is used on the main thread.
     const Closure& boundFunction = bind(&WebSharedWorkerClient::saveDevToolsAgentState, m_clientWeakPtr, cookie.isolatedCopy());
-    callOnMainThread(boundFunction);
+    callOnMainThreadWithLocation(boundFunction, FROM_HERE);
 }
 
 void WebSharedWorkerImpl::workerGlobalScopeClosed()
 {
-    callOnMainThread(bind(&WebSharedWorkerImpl::workerGlobalScopeClosedOnMainThread, this));
+    callOnMainThreadWithLocation(bind(&WebSharedWorkerImpl::workerGlobalScopeClosedOnMainThread, this), FROM_HERE);
 }
 
 void WebSharedWorkerImpl::workerGlobalScopeClosedOnMainThread()
@@ -284,7 +284,7 @@ void WebSharedWorkerImpl::workerGlobalScopeStarted(WorkerGlobalScope*)
 
 void WebSharedWorkerImpl::workerThreadTerminated()
 {
-    callOnMainThread(bind(&WebSharedWorkerImpl::workerThreadTerminatedOnMainThread, this));
+    callOnMainThreadWithLocation(bind(&WebSharedWorkerImpl::workerThreadTerminatedOnMainThread, this), FROM_HERE);
 }
 
 void WebSharedWorkerImpl::workerThreadTerminatedOnMainThread()
diff --git a/Source/wtf/MainThread.cpp b/Source/wtf/MainThread.cpp
index cbb98f5..96570a9 100644
--- a/Source/wtf/MainThread.cpp
+++ b/Source/wtf/MainThread.cpp
@@ -38,9 +38,10 @@
 namespace WTF {
 
 static ThreadIdentifier mainThreadIdentifier;
-static void (*callOnMainThreadFunction)(MainThreadFunction, void*);
+static void (*callOnMainThreadFunction)(MainThreadFunction, void*,
+                                        const blink::TraceLocation*);
 
-void initializeMainThread(void (*function)(MainThreadFunction, void*))
+void initializeMainThread(void (*function)(MainThreadFunction, void*, const blink::TraceLocation*))
 {
     static bool initializedMainThread;
     if (initializedMainThread)
@@ -51,9 +52,10 @@ void initializeMainThread(void (*function)(MainThreadFunction, void*))
     mainThreadIdentifier = currentThread();
 }
 
-void callOnMainThread(MainThreadFunction* function, void* context)
+void callOnMainThread(MainThreadFunction* function, void* context,
+                      const blink::TraceLocation* location)
 {
-    (*callOnMainThreadFunction)(function, context);
+    (*callOnMainThreadFunction)(function, context, location);
 }
 
 static void callFunctionObject(void* context)
@@ -68,6 +70,12 @@ void callOnMainThread(const Function<void()>& function)
     callOnMainThread(callFunctionObject, new Function<void()>(function));
 }
 
+void callOnMainThreadWithLocation(const Function<void()>& function,
+                                  const blink::TraceLocation& location)
+{
+    callOnMainThread(callFunctionObject, new Function<void()>(function), &location);
+}
+
 bool isMainThread()
 {
     return currentThread() == mainThreadIdentifier;
diff --git a/Source/wtf/MainThread.h b/Source/wtf/MainThread.h
index 7e1aaa8..210d561 100644
--- a/Source/wtf/MainThread.h
+++ b/Source/wtf/MainThread.h
@@ -33,6 +33,7 @@
 #include <stdint.h>
 
 #include "wtf/WTFExport.h"
+#include "Source/platform/TraceLocation.h"
 
 namespace WTF {
 
@@ -40,13 +41,18 @@ typedef uint32_t ThreadIdentifier;
 typedef void MainThreadFunction(void*);
 
 // Must be called from the main thread.
-WTF_EXPORT void initializeMainThread(void (*)(MainThreadFunction, void*));
+WTF_EXPORT void initializeMainThread(void (*)(MainThreadFunction, void*, const blink::TraceLocation*));
 
-WTF_EXPORT void callOnMainThread(MainThreadFunction*, void* context);
+WTF_EXPORT void callOnMainThread(MainThreadFunction*, void* context,
+                                 const blink::TraceLocation* = NULL);
 
 template<typename> class Function;
 WTF_EXPORT void callOnMainThread(const Function<void ()>&);
 
+template<typename> class Function;
+WTF_EXPORT void callOnMainThreadWithLocation(const Function<void ()>&,
+                                             const blink::TraceLocation&);
+
 WTF_EXPORT bool isMainThread();
 
 } // namespace WTF
diff --git a/Source/wtf/text/StringImpl.cpp b/Source/wtf/text/StringImpl.cpp
index 79d878d..a766cef 100644
--- a/Source/wtf/text/StringImpl.cpp
+++ b/Source/wtf/text/StringImpl.cpp
@@ -255,7 +255,10 @@ void StringStats::printStats()
     double overheadPercent = (double)totalOverhead / (double)totalDataBytes * 100;
     dataLogF("         StringImpl overheader: %8u (%5.2f%%)\n", totalOverhead, overheadPercent);
 
-    callOnMainThread(printLiveStringStats, 0);
+    // callOnMainThread(printLiveStringStats, 0);
+
+    ASSERT_NOT_REACHED();
+    callOnMainThreadWithLocation(printLiveStringStats, 0, FROM_HERE);
 }
 #endif
 
diff --git a/Source/wtf/text/StringImplCF.cpp b/Source/wtf/text/StringImplCF.cpp
index 558c30e..fb4eb99 100644
--- a/Source/wtf/text/StringImplCF.cpp
+++ b/Source/wtf/text/StringImplCF.cpp
@@ -92,7 +92,9 @@ static void deallocate(void* pointer, void*)
         fastFree(header);
     } else {
         if (!isMainThread()) {
-            callOnMainThread(deallocateOnMainThread, header);
+            // callOnMainThread(deallocateOnMainThread, header);
+            ASSERT_NOT_REACHED();
+            callOnMainThreadWithLocation(deallocateOnMainThread, header, FROM_HERE);
         } else {
             underlyingString->deref(); // Balanced by call to ref in allocate above.
             fastFree(header);
diff --git a/public/platform/Platform.h b/public/platform/Platform.h
index 6fc9934..bec7c5d 100644
--- a/public/platform/Platform.h
+++ b/public/platform/Platform.h
@@ -433,6 +433,8 @@ public:
     // Callable from a background WebKit thread.
     virtual void callOnMainThread(void (*func)(void*), void* context) { }
 
+    virtual void callOnMainThreadWithLocation(void (*func)(void*), void* context,
+                                              const char* loc_fileName, const char* loc_functionName, const int loc_lineNumber) { }
 
     // Vibration -----------------------------------------------------------
 
diff --git a/public/platform/WebThread.h b/public/platform/WebThread.h
index 6bee10f..8ce58ad 100644
--- a/public/platform/WebThread.h
+++ b/public/platform/WebThread.h
@@ -27,6 +27,8 @@
 
 #include "WebCommon.h"
 
+#include <string>
+
 namespace blink {
 
 // Provides an interface to an embedder-defined thread implementation.
@@ -48,10 +50,14 @@ public:
         virtual void didProcessTask() = 0;
     };
 
+    virtual void set_thread_name(const std::string& thread_name) = 0;
+
     // postTask() and postDelayedTask() take ownership of the passed Task
     // object. It is safe to invoke postTask() and postDelayedTask() from any
     // thread.
-    virtual void postTask(Task*) = 0;
+    /* virtual void postTask(Task*) = 0; */
+    virtual void postTask(Task* task,
+                          const char* loc_fileName=NULL, const char* loc_functionName=NULL, const int loc_lineNumber=-1) = 0;
     virtual void postDelayedTask(Task*, long long delayMs) = 0;
 
     virtual bool isCurrentThread() const = 0;
-- 
2.1.4


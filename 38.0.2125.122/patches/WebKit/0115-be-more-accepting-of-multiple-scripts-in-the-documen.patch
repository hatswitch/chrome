From 04e9abbb968bd0931a5d7608ef9684545740df2e Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Tue, 29 Mar 2016 12:59:24 -0500
Subject: [PATCH 115/123] * be more accepting of multiple scripts in the
 document's script   stack, which can happen if for example on a  
 doc.write("<script>...</script>") * not supporting message channel for now *
 some logging changes

---
 Source/core/dom/Document.cpp                      | 36 +++++++++++------------
 Source/core/dom/MessageChannel.cpp                |  4 +++
 Source/core/html/parser/HTMLScriptRunner.cpp      | 27 ++++++++++++++++-
 Source/core/html/parser/NestingLevelIncrementer.h |  4 +--
 4 files changed, 49 insertions(+), 22 deletions(-)

diff --git a/Source/core/dom/Document.cpp b/Source/core/dom/Document.cpp
index a842fbc..ec3f37e 100644
--- a/Source/core/dom/Document.cpp
+++ b/Source/core/dom/Document.cpp
@@ -2270,7 +2270,7 @@ void Document::setIsRunningScript(bool isRunningScript)
     } else {
         ++m_isRunningScriptCount;
         // crash if more than 1 current script so we can take a look.
-        ASSERT(m_isRunningScriptCount == 1);
+        ASSERT(m_isRunningScriptCount == 1 || m_writeRecursionDepth > 0);
 
         // we cannot do this assert because we could reach here when
         // executing a "javascript:..." url, which doesn't go through
@@ -2972,8 +2972,6 @@ void Document::write(const SegmentedString& text, Document* ownerDocument, Excep
         return;
     }
 
-    NestingLevelIncrementer nestingLevelIncrementer(m_writeRecursionDepth);
-
     String url;
     if (m_extractingPageModel) {
         ASSERT(ownerDocument);
@@ -2982,13 +2980,15 @@ void Document::write(const SegmentedString& text, Document* ownerDocument, Excep
         exceptionState.GetScriptNameOrSourceURL(&url);
         MYWTF_LOG_WITH_DOC(
             PageModeling, this,
-            "doc.write() begin %d bytes, ownerDoc:%u script [%s] writeRecursionDepth %d",
-            text.length(), ownerDocument->instNum(), url.ascii().data(), m_writeRecursionDepth);
+            "doc.write() begin %d bytes, ownerDoc:%u script [%s]",
+            text.length(), ownerDocument->instNum(), url.ascii().data());
         //        ASSERT(ownerDocument == this);
-
-        ASSERT(m_writeRecursionDepth == 1);
     }
 
+    NestingLevelIncrementer nestingLevelIncrementer(m_writeRecursionDepth,
+                                                    m_extractingPageModel ? this : NULL,
+                                                    m_extractingPageModel ? "docWriteRecursionDepth" : NULL);
+
     m_writeRecursionIsTooDeep = (m_writeRecursionDepth > 1) && m_writeRecursionIsTooDeep;
     m_writeRecursionIsTooDeep = (m_writeRecursionDepth > cMaxWriteRecursionDepth) || m_writeRecursionIsTooDeep;
 
@@ -3029,8 +3029,8 @@ void Document::write(const SegmentedString& text, Document* ownerDocument, Excep
     if (m_extractingPageModel) {
         MYWTF_LOG_WITH_DOC(
             PageModeling, this,
-            "doc.write() done %d bytes, ownerDoc:%u script [%s] writeRecursionDepth %d",
-            text.length(), ownerDocument->instNum(), url.ascii().data(), m_writeRecursionDepth);
+            "doc.write() done %d bytes, ownerDoc:%u script [%s]",
+            text.length(), ownerDocument->instNum(), url.ascii().data());
     }
 }
 
@@ -4714,19 +4714,17 @@ void Document::pushCurrentScript(PassRefPtrWillBeRawPtr<HTMLScriptElement> newCu
 {
     ASSERT(newCurrentScript);
     const KURL scriptUrl = newCurrentScript->src();
+    const uint32_t elemInstNum = newCurrentScript->instNum();
     m_currentScriptStack.append(newCurrentScript);
-    // crash if more than 1 current script so we can take a look.
-    ASSERT_WITH_MESSAGE(m_currentScriptStack.size() == 1,
-                        "___ FATAL multiple concurrent scripts. we need to take a look");
+    if (m_extractingPageModel) {
+        MYWTF_LOG_WITH_DOC(PageModeling, this, "script elem:%u, url [%s] pushed",
+                           elemInstNum, scriptUrl.string().ascii().data());
 
-#ifdef DO_LOG_DYNAMIC_HTML_INSERTIONS
-    if (m_loggingDynamicHTMLInsertions) {
-        MYWTF_LOG_WITH_DOC(PageModeling, this, "script pushed [%s]",
-                           scriptUrl.string().ascii().data());
-        m_scriptToDocWrite.append(std::pair<String, String>(scriptUrl.string(), ""));
+        // a script that calls doc.write("<script>...</script>"); will
+        // cause us to have multiple scripts on the stack
+        ASSERT_WITH_MESSAGE(m_currentScriptStack.size() == 1 || m_writeRecursionDepth > 0,
+                            "___ FATAL multiple concurrent scripts. we need to take a look");
     }
-#endif
-
 }
 
 void Document::popCurrentScript()
diff --git a/Source/core/dom/MessageChannel.cpp b/Source/core/dom/MessageChannel.cpp
index eb82673..d39883f 100644
--- a/Source/core/dom/MessageChannel.cpp
+++ b/Source/core/dom/MessageChannel.cpp
@@ -31,6 +31,8 @@
 #include "public/platform/Platform.h"
 #include "public/platform/WebMessagePortChannel.h"
 
+#include "core/dom/Document.h"
+
 namespace blink {
 
 DEFINE_EMPTY_DESTRUCTOR_WILL_BE_REMOVED(MessageChannel);
@@ -53,6 +55,8 @@ MessageChannel::MessageChannel(ExecutionContext* context)
 {
     ScriptWrappable::init(this);
     createChannel(m_port1.get(), m_port2.get());
+
+    ASSERT(!context->isDocument() || !toDocument(context)->isExtractingPageModel());
 }
 
 void MessageChannel::trace(Visitor* visitor)
diff --git a/Source/core/html/parser/HTMLScriptRunner.cpp b/Source/core/html/parser/HTMLScriptRunner.cpp
index ac8c039..9b71209 100644
--- a/Source/core/html/parser/HTMLScriptRunner.cpp
+++ b/Source/core/html/parser/HTMLScriptRunner.cpp
@@ -340,13 +340,18 @@ void HTMLScriptRunner::runScript(Element* script, const TextPosition& scriptStar
     {
         ScriptLoader* scriptLoader = toScriptLoaderIfPossible(script);
 
+        const bool isExtractingPageModel = m_document->isExtractingPageModel();
+
         // This contains both and ASSERTION and a null check since we should not
         // be getting into the case of a null script element, but seem to be from
         // time to time. The assertion is left in to help find those cases and
         // is being tracked by <https://bugs.webkit.org/show_bug.cgi?id=60559>.
         ASSERT(scriptLoader);
         if (!scriptLoader)
+        {
+            ASSERT(!isExtractingPageModel);
             return;
+        }
 
         ASSERT(scriptLoader->isParserInserted());
 
@@ -354,12 +359,27 @@ void HTMLScriptRunner::runScript(Element* script, const TextPosition& scriptStar
             Microtask::performCheckpoint();
 
         InsertionPointRecord insertionPointRecord(m_host->inputStream());
-        NestingLevelIncrementer nestingLevelIncrementer(m_scriptNestingLevel);
+        NestingLevelIncrementer nestingLevelIncrementer(m_scriptNestingLevel,
+                                                        isExtractingPageModel ? m_document.get() : NULL,
+                                                        isExtractingPageModel ? "scriptNestingLevel" : NULL);
+
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, m_document.get(),
+                               "begin, script elem:%u", script->instNum());
+        }
 
         scriptLoader->prepareScript(scriptStartPosition);
 
         if (!scriptLoader->willBeParserExecuted())
+        {
+            if (isExtractingPageModel) {
+                MYWTF_LOG_WITH_DOC(PageModeling, m_document.get(),
+                                   "will not be executed by parser");
+                MYWTF_LOG_WITH_DOC(PageModeling, m_document.get(),
+                                   "done, script elem:%u", script->instNum());
+            }
             return;
+        }
 
         if (scriptLoader->willExecuteWhenDocumentFinishedParsing()) {
             requestDeferredScript(script);
@@ -378,6 +398,11 @@ void HTMLScriptRunner::runScript(Element* script, const TextPosition& scriptStar
         } else {
             requestParsingBlockingScript(script);
         }
+
+        if (isExtractingPageModel) {
+            MYWTF_LOG_WITH_DOC(PageModeling, m_document.get(),
+                               "done, script elem:%u", script->instNum());
+        }
     }
 }
 
diff --git a/Source/core/html/parser/NestingLevelIncrementer.h b/Source/core/html/parser/NestingLevelIncrementer.h
index 22ff9d8..65900c3 100644
--- a/Source/core/html/parser/NestingLevelIncrementer.h
+++ b/Source/core/html/parser/NestingLevelIncrementer.h
@@ -50,7 +50,7 @@ public:
 
         if (m_document && m_document->isExtractingPageModel()) {
             MYWTF_LOG_WITH_DOC(PageModeling, m_document,
-                               "parser:%u: %s %d",
+                               "parser:%u: %s to up %d",
                                m_document->parser()->instNum(), m_nestingLevelName, *m_nestingLevel);
         }
     }
@@ -61,7 +61,7 @@ public:
 
         if (m_document && m_document->isExtractingPageModel()) {
             MYWTF_LOG_WITH_DOC(PageModeling, m_document,
-                               "parser:%u: %s %d",
+                               "parser:%u: %s down to %d",
                                m_document->parser()->instNum(), m_nestingLevelName, *m_nestingLevel);
         }
     }
-- 
2.1.4


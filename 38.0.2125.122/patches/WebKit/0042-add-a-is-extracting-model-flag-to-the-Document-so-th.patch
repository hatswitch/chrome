From d77b55ef36383a39ba3f979a25d77424a983fa66 Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Tue, 26 Jan 2016 20:16:09 -0600
Subject: [PATCH 42/74] add a "is extracting model" flag to the Document, so
 that if the flag is true, then we do additional logic/logging.

modifying Document.h will require re-building over 1000 files, so we
move Document::scheduleRenderTreeUpdateIfNeeded() to .cpp file so that
we only need to rebuild a few files when adding logging to the function
when debugging.

also update correct usage of exceptionState.GetScriptNameOrSourceUrl()
---
 Source/core/dom/Document.cpp | 52 ++++++++++++++++++++++++++++++++++++++------
 Source/core/dom/Document.h   | 13 ++++-------
 2 files changed, 49 insertions(+), 16 deletions(-)

diff --git a/Source/core/dom/Document.cpp b/Source/core/dom/Document.cpp
index 30d13d7..7327361 100644
--- a/Source/core/dom/Document.cpp
+++ b/Source/core/dom/Document.cpp
@@ -206,6 +206,9 @@
 #include "wtf/text/TextEncodingRegistry.h"
 #include "platform/Logging.h"
 
+
+#define DO_EXTRACT_PAGE_MODEL
+
 using namespace WTF;
 using namespace Unicode;
 
@@ -508,7 +511,13 @@ Document::Document(const DocumentInit& initializer, DocumentClassFlags documentC
     , m_didAssociateFormControlsTimer(this, &Document::didAssociateFormControlsTimerFired)
     , m_hasViewportUnits(false)
     , m_styleRecalcElementCounter(0)
+    , m_extractingPageModel(false)
 {
+    /* m_extractingPageModel is set to true only if
+     * DO_EXTRACT_PAGE_MODEL is defined (i.e., requires re-compile),
+     * and the document is HTML, and it is loadding an http/s url.
+     */
+
     setClient(this);
     ScriptWrappable::init(this);
 
@@ -548,8 +557,8 @@ Document::Document(const DocumentInit& initializer, DocumentClassFlags documentC
     // m_fetcher.
     m_styleEngine = StyleEngine::create(*this);
 
-    if (url().protocolIsInHTTPFamily()) {
-        MYWTF_LOG(ResourceLoading, "___ document %p constructor. url: [%s]", this, url().string().ascii().data());
+    if (m_extractingPageModel) {
+        MYWTF_LOG(ResourceLoading, "___ document %p: constructor. url: [%s]", this, url().string().ascii().data());
     }
 
 #ifndef NDEBUG
@@ -633,8 +642,8 @@ Document::~Document()
 
     setClient(0);
 
-    if (url().protocolIsInHTTPFamily()) {
-        MYWTF_LOG(ResourceLoading, "___ document %p destructor", this);
+    if (m_extractingPageModel) {
+        MYWTF_LOG(ResourceLoading, "___ document %p: destructor", this);
     }
 
     InspectorCounters::decrementCounter(InspectorCounters::DocumentCounter);
@@ -782,7 +791,8 @@ PassRefPtrWillBeRawPtr<Element> Document::createElement(const AtomicString& name
 
     if (isXHTMLDocument() || isHTMLDocument())
     {
-        const String url = exceptionState.GetScriptNameOrSourceURL();
+        String url;
+        exceptionState.GetScriptNameOrSourceURL(&url);
         ASSERT(!url.isEmpty());
         const DependencyParentInfo parent(url, false, __FILE__, __LINE__);
         PassRefPtrWillBeRawPtr<HTMLElement> element = HTMLElementFactory::createHTMLElement(convertLocalName(name), *this, 0, false, &parent);
@@ -1156,7 +1166,8 @@ PassRefPtrWillBeRawPtr<Element> Document::createElement(const QualifiedName& qNa
     // FIXME: Use registered namespaces and look up in a hash to find the right factory.
     if (qName.namespaceURI() == xhtmlNamespaceURI)
     {
-        const String url = exceptionState.GetScriptNameOrSourceURL();
+        String url;
+        exceptionState.GetScriptNameOrSourceURL(&url);
         if (url.isEmpty()) {
             if (!createdByParser) {
                 MYWTF_LOG(ResourceLoading, "___ element %s created by neither parser nor a script!",
@@ -1698,6 +1709,15 @@ bool Document::shouldScheduleRenderTreeUpdate() const
     return true;
 }
 
+inline void Document::scheduleRenderTreeUpdateIfNeeded()
+{
+    // Inline early out to avoid the function calls below.
+    if (hasPendingStyleRecalc())
+        return;
+    if (shouldScheduleRenderTreeUpdate() && needsRenderTreeUpdate())
+        scheduleRenderTreeUpdate();
+}
+
 void Document::scheduleRenderTreeUpdate()
 {
     ASSERT(!hasPendingStyleRecalc());
@@ -2158,7 +2178,7 @@ void Document::setIsRunningScript(bool isRunningScript)
         // crash if more than 1 current script so we can take a look.
         ASSERT(m_isRunningScriptCount == 1);
     }
-    if (url().protocolIsInHTTPFamily()) {
+    if (m_extractingPageModel) {
         MYWTF_LOG(ResourceLoading, "___ document %p new m_isRunningScriptCount= _%d_",
                   this, m_isRunningScriptCount);
     }
@@ -2898,6 +2918,24 @@ void Document::setURL(const KURL& url)
     if (newURL == m_url)
         return;
 
+#if defined(DO_EXTRACT_PAGE_MODEL)
+     // only extract models of html documents with http/s URLs.
+     if (m_url.isEmpty()) {
+         ASSERT(!m_extractingPageModel);
+         m_extractingPageModel = isHTMLDocument() && newURL.protocolIsInHTTPFamily();
+     } else if (m_extractingPageModel) {
+         // we don't support changing document url,
+         ASSERT_NOT_REACHED();
+
+         // make sure the protocol family is not changing
+         // ASSERT(newURL.protocolIsInHTTPFamily());
+         // MYWTF_LOG(ResourceLoading, "___ document %p: changes to new URL: [%s]",
+         //           this, newURL.string().ascii().data());
+     }
+#else
+     m_extractingPageModel = false;
+#endif
+
     m_url = newURL;
     updateBaseURL();
     contextFeatures().urlDidChange(this);
diff --git a/Source/core/dom/Document.h b/Source/core/dom/Document.h
index e53c8d1..160e563 100644
--- a/Source/core/dom/Document.h
+++ b/Source/core/dom/Document.h
@@ -1037,6 +1037,8 @@ public:
 
     AtomicString convertLocalName(const AtomicString&);
 
+    bool isExtractingPageModel() const { return m_extractingPageModel; }
+
 protected:
     Document(const DocumentInit&, DocumentClassFlags = DefaultDocumentClass);
 
@@ -1369,6 +1371,8 @@ private:
     DocumentVisibilityObserverSet m_visibilityObservers;
 
     int m_styleRecalcElementCounter;
+
+    bool m_extractingPageModel;
 };
 
 inline bool Document::shouldOverrideLegacyDescription(ViewportDescription::Type origin)
@@ -1379,15 +1383,6 @@ inline bool Document::shouldOverrideLegacyDescription(ViewportDescription::Type
     return origin >= m_legacyViewportDescription.type;
 }
 
-inline void Document::scheduleRenderTreeUpdateIfNeeded()
-{
-    // Inline early out to avoid the function calls below.
-    if (hasPendingStyleRecalc())
-        return;
-    if (shouldScheduleRenderTreeUpdate() && needsRenderTreeUpdate())
-        scheduleRenderTreeUpdate();
-}
-
 DEFINE_TYPE_CASTS(Document, ExecutionContextClient, client, client->isDocument(), client.isDocument());
 DEFINE_TYPE_CASTS(Document, ExecutionContext, context, context->isDocument(), context.isDocument());
 DEFINE_NODE_TYPE_CASTS(Document, isDocumentNode());
-- 
2.1.4


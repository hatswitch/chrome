From c7cacff7da8ec7deee55e8079065c1df1886bd75 Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Tue, 1 Mar 2016 16:20:38 -0600
Subject: [PATCH 77/88] DocumentFragment remembers whether we're extracting the
 page model by at its constructor inspecting the parent document.

ShadowRoot is a DocumentFragment, so do the same for ShadowRoot.

add an ASSERT_NOT_REACHED() in Element.
---
 Source/core/dom/DocumentFragment.cpp  | 19 +++++++++++++++++--
 Source/core/dom/DocumentFragment.h    |  9 ++++++++-
 Source/core/dom/Element.cpp           |  1 +
 Source/core/dom/shadow/ShadowRoot.cpp |  2 +-
 4 files changed, 27 insertions(+), 4 deletions(-)

diff --git a/Source/core/dom/DocumentFragment.cpp b/Source/core/dom/DocumentFragment.cpp
index a0b656f..85f01e0 100644
--- a/Source/core/dom/DocumentFragment.cpp
+++ b/Source/core/dom/DocumentFragment.cpp
@@ -29,15 +29,30 @@
 
 namespace blink {
 
-DocumentFragment::DocumentFragment(Document* document, ConstructionType constructionType)
+DocumentFragment::DocumentFragment(Document* document, ConstructionType constructionType,
+                                   bool isExtractingPageModel)
     : ContainerNode(document, constructionType)
+    , m_isExtractingPageModel(document ? document->isExtractingPageModel() : isExtractingPageModel)
 {
     ScriptWrappable::init(this);
+    if (document) {
+        ASSERT(document->isExtractingPageModel() == isExtractingPageModel);
+    }
+}
+
+DocumentFragment::~DocumentFragment()
+{
+    if (m_isExtractingPageModel) {
+        // log without document because we might no longer be
+        // associated with a document. and also at this point we
+        // don't need the identity of the doc.
+        MYWTF_LOG(PageModeling, "document fragment %p destructed", this);
+    }
 }
 
 PassRefPtrWillBeRawPtr<DocumentFragment> DocumentFragment::create(Document& document)
 {
-    return adoptRefWillBeNoop(new DocumentFragment(&document, Node::CreateDocumentFragment));
+    return adoptRefWillBeNoop(new DocumentFragment(&document, Node::CreateDocumentFragment, document.isExtractingPageModel()));
 }
 
 String DocumentFragment::nodeName() const
diff --git a/Source/core/dom/DocumentFragment.h b/Source/core/dom/DocumentFragment.h
index 83a06ce..21c702b 100644
--- a/Source/core/dom/DocumentFragment.h
+++ b/Source/core/dom/DocumentFragment.h
@@ -39,10 +39,17 @@ public:
     virtual bool canContainRangeEndPoint() const OVERRIDE FINAL { return true; }
     virtual bool isTemplateContent() const { return false; }
 
+    bool isExtractingPageModel() const { return m_isExtractingPageModel; }
+
 protected:
-    DocumentFragment(Document*, ConstructionType = CreateContainer);
+    DocumentFragment(Document*, ConstructionType = CreateContainer, bool = false);
+    virtual ~DocumentFragment();
+
     virtual String nodeName() const OVERRIDE FINAL;
 
+    /* will grab value from document */
+    const bool m_isExtractingPageModel;
+
 private:
     virtual NodeType nodeType() const OVERRIDE FINAL;
     virtual PassRefPtrWillBeRawPtr<Node> cloneNode(bool deep = true) OVERRIDE;
diff --git a/Source/core/dom/Element.cpp b/Source/core/dom/Element.cpp
index 7784964..ec349c7 100644
--- a/Source/core/dom/Element.cpp
+++ b/Source/core/dom/Element.cpp
@@ -1733,6 +1733,7 @@ PassRefPtrWillBeRawPtr<ShadowRoot> Element::createShadowRoot(ExceptionState& exc
     // flag is provided for testing how author shadows interact on these elements.
     if (!areAuthorShadowsAllowed() && !RuntimeEnabledFeatures::authorShadowDOMForAnyElementEnabled()) {
         exceptionState.throwDOMException(HierarchyRequestError, "Author-created shadow roots are disabled for this element.");
+        ASSERT_NOT_REACHED();
         return nullptr;
     }
 
diff --git a/Source/core/dom/shadow/ShadowRoot.cpp b/Source/core/dom/shadow/ShadowRoot.cpp
index e78d269..6c8a52c 100644
--- a/Source/core/dom/shadow/ShadowRoot.cpp
+++ b/Source/core/dom/shadow/ShadowRoot.cpp
@@ -51,7 +51,7 @@ struct SameSizeAsShadowRoot : public DocumentFragment, public TreeScope, public
 COMPILE_ASSERT(sizeof(ShadowRoot) == sizeof(SameSizeAsShadowRoot), shadowroot_should_stay_small);
 
 ShadowRoot::ShadowRoot(Document& document, ShadowRootType type)
-    : DocumentFragment(0, CreateShadowRoot)
+    : DocumentFragment(0, CreateShadowRoot, document.isExtractingPageModel())
     , TreeScope(*this, document)
     , m_prev(nullptr)
     , m_next(nullptr)
-- 
2.1.4


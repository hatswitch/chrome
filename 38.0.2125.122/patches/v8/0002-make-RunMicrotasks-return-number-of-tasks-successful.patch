From e564fcc18ec670fce4a7df96aa623ce9289d0417 Mon Sep 17 00:00:00 2001
From: giang nguyen <nguyen59@illinois.edu>
Date: Wed, 30 Mar 2016 21:54:14 -0500
Subject: [PATCH 2/8] make RunMicrotasks() return number of tasks successfully
 run

---
 include/v8.h   |  4 +++-
 src/api.cc     |  4 ++--
 src/isolate.cc | 11 ++++++++---
 src/isolate.h  |  2 +-
 src/runtime.cc |  4 ++--
 5 files changed, 16 insertions(+), 9 deletions(-)

diff --git a/include/v8.h b/include/v8.h
index be0590f..77ed635 100644
--- a/include/v8.h
+++ b/include/v8.h
@@ -4417,8 +4417,10 @@ class V8_EXPORT Isolate {
   /**
    * Experimental: Runs the Microtask Work Queue until empty
    * Any exceptions thrown by microtask callbacks are swallowed.
+   *
+   * returns how many microtasks were run
    */
-  void RunMicrotasks();
+  int RunMicrotasks();
 
   /**
    * Experimental: Enqueues the callback to the Microtask Work Queue
diff --git a/src/api.cc b/src/api.cc
index 093da98..b3bf4ef 100644
--- a/src/api.cc
+++ b/src/api.cc
@@ -6660,8 +6660,8 @@ bool Isolate::WillRunMicrotasksOrNot() const {
   return reinterpret_cast<const i::Isolate*>(this)->WillRunMicrotasksOrNot();
 }
 
-void Isolate::RunMicrotasks() {
-  reinterpret_cast<i::Isolate*>(this)->RunMicrotasks();
+int Isolate::RunMicrotasks() {
+  return reinterpret_cast<i::Isolate*>(this)->RunMicrotasks();
 }
 
 
diff --git a/src/isolate.cc b/src/isolate.cc
index 3db8223..29c6a1d 100644
--- a/src/isolate.cc
+++ b/src/isolate.cc
@@ -2284,7 +2284,7 @@ bool Isolate::WillRunMicrotasksOrNot() const {
     return pending_microtask_count() > 0;
 }
 
-void Isolate::RunMicrotasks() {
+int Isolate::RunMicrotasks() {
   // %RunMicrotasks may be called in mjsunit tests, which violates
   // this assertion, hence the check for --allow-natives-syntax.
   // TODO(adamk): However, this also fails some layout tests.
@@ -2296,6 +2296,9 @@ void Isolate::RunMicrotasks() {
   v8::Isolate::SuppressMicrotaskExecutionScope suppress(
       reinterpret_cast<v8::Isolate*>(this));
 
+  // number of microtasks we ran successfully
+  int numRun = 0;
+
   while (pending_microtask_count() > 0) {
     HandleScope scope(this);
     int num_tasks = pending_microtask_count();
@@ -2304,7 +2307,7 @@ void Isolate::RunMicrotasks() {
     set_pending_microtask_count(0);
     heap()->set_microtask_queue(heap()->empty_fixed_array());
 
-    for (int i = 0; i < num_tasks; i++) {
+    for (int i = 0; i < num_tasks; i++, ++numRun) {
       HandleScope scope(this);
       Handle<Object> microtask(queue->get(i), this);
       if (microtask->IsJSFunction()) {
@@ -2323,7 +2326,7 @@ void Isolate::RunMicrotasks() {
           // Clear out any remaining callbacks in the queue.
           heap()->set_microtask_queue(heap()->empty_fixed_array());
           set_pending_microtask_count(0);
-          return;
+          return numRun;
         }
       } else {
         Handle<CallHandlerInfo> callback_info =
@@ -2335,6 +2338,8 @@ void Isolate::RunMicrotasks() {
       }
     }
   }
+
+  return numRun;
 }
 
 
diff --git a/src/isolate.h b/src/isolate.h
index 3911a15..a3ba3ac 100644
--- a/src/isolate.h
+++ b/src/isolate.h
@@ -1098,7 +1098,7 @@ class Isolate {
   void FireCallCompletedCallback();
 
   void EnqueueMicrotask(Handle<Object> microtask);
-  void RunMicrotasks();
+  int RunMicrotasks();
   bool WillRunMicrotasksOrNot() const;
 
   void SetUseCounterCallback(v8::Isolate::UseCounterCallback callback);
diff --git a/src/runtime.cc b/src/runtime.cc
index 0473f74..cbb8b76 100644
--- a/src/runtime.cc
+++ b/src/runtime.cc
@@ -14854,8 +14854,8 @@ RUNTIME_FUNCTION(Runtime_WillRunMicrotasksOrNot) {
 RUNTIME_FUNCTION(Runtime_RunMicrotasks) {
   HandleScope scope(isolate);
   DCHECK(args.length() == 0);
-  isolate->RunMicrotasks();
-  return isolate->heap()->undefined_value();
+  const int numRun = isolate->RunMicrotasks();
+  return *isolate->factory()->NewNumberFromInt(numRun);
 }
 
 
-- 
2.1.4


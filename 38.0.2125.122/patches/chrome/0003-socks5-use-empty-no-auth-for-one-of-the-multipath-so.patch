From 35fe071d75afcb972e0922e5131158501bd1d9d7 Mon Sep 17 00:00:00 2001
From: Giang Nguyen <nguyen59@illinois.edu>
Date: Sun, 23 Nov 2014 01:32:23 -0600
Subject: [PATCH 03/22] socks5 use empty/no auth for one of the multipath, so
 that, such connections, with an empty username and an empty password, are not
 isolated from single-path connections.

---
 net/socket/socks5_client_socket.cc | 27 ++++++++++++++++++++-------
 net/socket/socks5_client_socket.h  |  4 +++-
 2 files changed, 23 insertions(+), 8 deletions(-)

diff --git a/net/socket/socks5_client_socket.cc b/net/socket/socks5_client_socket.cc
index 292a201..fbb72d9 100644
--- a/net/socket/socks5_client_socket.cc
+++ b/net/socket/socks5_client_socket.cc
@@ -30,7 +30,8 @@ const unsigned int SOCKS5ClientSocket::kAuthReadHeaderSize = 2;
 static const uint8 kSOCKS5AuthVersion = 0x01;
 
 static const char g_tor_usernames[] = {
-  'a',
+  0, /* should specify empty username, so that as far as Tor is
+	concerned, as if we didn't specify auth. */
   'b'
 };
 static int g_hostname_initial_tor_username_idx_ = 0;
@@ -55,6 +56,7 @@ SOCKS5ClientSocket::SOCKS5ClientSocket(
       was_ever_used_(false),
       host_request_info_(req_info),
       multipathMode_(multipathMode),
+      username_(0),
       net_log_(transport_->socket()->NetLog()) {
 }
 
@@ -218,7 +220,13 @@ void SOCKS5ClientSocket::OnIOComplete(int result) {
   DCHECK_NE(STATE_NONE, next_state_);
   int rv = DoLoop(result);
   if (rv != ERR_IO_PENDING) {
-    net_log_.EndEvent(NetLog::TYPE_SOCKS5_CONNECT);
+    if (!username_) {
+      net_log_.EndEvent(NetLog::TYPE_SOCKS5_CONNECT);
+    }
+    else {
+      net_log_.EndEvent(NetLog::TYPE_SOCKS5_CONNECT,
+			NetLog::IntegerCallback("username", username_));
+    }
     DoCallback(rv);
   }
 }
@@ -410,12 +418,11 @@ int SOCKS5ClientSocket::DoGreetReadComplete(int result) {
   return OK;
 }
 
-int SOCKS5ClientSocket::BuildAuthWriteBuffer(std::string* handshake)
+int SOCKS5ClientSocket::BuildAuthWriteBuffer(std::string* handshake, char& username)
     const {
   DCHECK(handshake->empty());
 
   handshake->push_back(kSOCKS5AuthVersion);
-  handshake->push_back(1); // length of username
 
   const std::string name = host_request_info_.hostname();
 
@@ -431,11 +438,17 @@ int SOCKS5ClientSocket::BuildAuthWriteBuffer(std::string* handshake)
       (g_hostname_initial_tor_username_idx_ + 1) % arraysize(g_tor_usernames);
   }
 
-  const char username = g_tor_usernames[(*hostname_to_tor_username_idx_)[name]];
+  username = g_tor_usernames[(*hostname_to_tor_username_idx_)[name]];
   (*hostname_to_tor_username_idx_)[name] =
     ((*hostname_to_tor_username_idx_)[name] + 1) % arraysize(g_tor_usernames);
 
-  handshake->push_back(username);
+  if (username) {
+    handshake->push_back(1);
+    handshake->push_back(username);
+  }
+  else {
+    handshake->push_back(0);
+  }
 
   handshake->push_back(0); // 0 is length of password
 
@@ -446,7 +459,7 @@ int SOCKS5ClientSocket::DoAuthWrite() {
   next_state_ = STATE_AUTH_WRITE_COMPLETE;
 
   if (buffer_.empty()) {
-    int rv = BuildAuthWriteBuffer(&buffer_);
+    int rv = BuildAuthWriteBuffer(&buffer_, username_);
     if (rv != OK)
       return rv;
     bytes_sent_ = 0;
diff --git a/net/socket/socks5_client_socket.h b/net/socket/socks5_client_socket.h
index 97d4d55..457ed43 100644
--- a/net/socket/socks5_client_socket.h
+++ b/net/socket/socks5_client_socket.h
@@ -124,7 +124,7 @@ class NET_EXPORT_PRIVATE SOCKS5ClientSocket : public StreamSocket {
   // Writes the SOCKS handshake buffer into |handshake|
   // and return OK on success.
   int BuildHandshakeWriteBuffer(std::string* handshake) const;
-  int BuildAuthWriteBuffer(std::string* handshake) const;
+  int BuildAuthWriteBuffer(std::string* handshake, char& username) const;
 
   CompletionCallback io_callback_;
 
@@ -160,6 +160,8 @@ class NET_EXPORT_PRIVATE SOCKS5ClientSocket : public StreamSocket {
   HostResolver::RequestInfo host_request_info_;
 
   const int multipathMode_;
+  char username_; // username used with auth handshake. if 0, then
+		  // username not sent to server
 
   BoundNetLog net_log_;
 
-- 
1.9.1


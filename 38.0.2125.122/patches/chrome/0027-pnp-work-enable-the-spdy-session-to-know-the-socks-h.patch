From e2d913ed786d085432e8dd715954383c63232ee2 Mon Sep 17 00:00:00 2001
From: Giang Nguyen <nguyen59@illinois.edu>
Date: Sat, 14 Feb 2015 04:54:14 -0600
Subject: [PATCH 27/30] pnp-work: enable the spdy session to know the socks
 handshake rtt if applicable. we will use this as an initial guide to
 determine how long to delay sending out the requests to wait for pushes.

---
 net/base/load_timing_info.h               |  3 +++
 net/http/http_proxy_client_socket.cc      |  8 ++++++++
 net/http/http_proxy_client_socket.h       |  3 +++
 net/http/http_proxy_client_socket_pool.cc |  2 ++
 net/socket/socks5_client_socket.cc        | 12 ++++++++++--
 net/socket/socks5_client_socket.h         |  6 +++++-
 net/socket/socks_client_socket_pool.cc    |  3 ++-
 net/socket/ssl_client_socket_pool.cc      |  2 ++
 net/spdy/spdy_session.cc                  |  9 +++++++++
 net/spdy/spdy_session.h                   |  4 ++++
 10 files changed, 48 insertions(+), 4 deletions(-)

diff --git a/net/base/load_timing_info.h b/net/base/load_timing_info.h
index 00dbabf..ddf414e 100644
--- a/net/base/load_timing_info.h
+++ b/net/base/load_timing_info.h
@@ -89,6 +89,9 @@ struct NET_EXPORT LoadTimingInfo {
     // the final destination server, not an SSL/SPDY proxy.
     base::TimeTicks ssl_start;
     base::TimeTicks ssl_end;
+
+    base::TimeTicks socks_handshake_start;
+    base::TimeTicks socks_handshake_end;
   };
 
   LoadTimingInfo();
diff --git a/net/http/http_proxy_client_socket.cc b/net/http/http_proxy_client_socket.cc
index 0a3408b..f0f7f24 100644
--- a/net/http/http_proxy_client_socket.cc
+++ b/net/http/http_proxy_client_socket.cc
@@ -539,4 +539,12 @@ int HttpProxyClientSocket::DoTCPRestartComplete(int result) {
   return result;
 }
 
+LoadTimingInfo::ConnectTiming HttpProxyClientSocket::connect_timing() const
+{
+  if (transport_) {
+    return transport_->connect_timing();
+  }
+  return LoadTimingInfo::ConnectTiming();
+}
+
 }  // namespace net
diff --git a/net/http/http_proxy_client_socket.h b/net/http/http_proxy_client_socket.h
index a2682c5..e8ffc64 100644
--- a/net/http/http_proxy_client_socket.h
+++ b/net/http/http_proxy_client_socket.h
@@ -19,6 +19,7 @@
 #include "net/http/http_response_info.h"
 #include "net/http/proxy_client_socket.h"
 #include "net/socket/ssl_client_socket.h"
+#include "net/socket/client_socket_handle.h"
 
 class GURL;
 
@@ -87,6 +88,8 @@ class HttpProxyClientSocket : public ProxyClientSocket {
   virtual int GetPeerAddress(IPEndPoint* address) const OVERRIDE;
   virtual int GetLocalAddress(IPEndPoint* address) const OVERRIDE;
 
+  LoadTimingInfo::ConnectTiming connect_timing() const;
+
  private:
   enum State {
     STATE_NONE,
diff --git a/net/http/http_proxy_client_socket_pool.cc b/net/http/http_proxy_client_socket_pool.cc
index 8380436..e165c48 100644
--- a/net/http/http_proxy_client_socket_pool.cc
+++ b/net/http/http_proxy_client_socket_pool.cc
@@ -359,6 +359,8 @@ int HttpProxyConnectJob::DoHttpProxyConnect() {
   HostPortPair proxy_server = params_->destination().host_port_pair();
 
   MYDVLOG(2) << "___ proxy server= " << proxy_server.ToString();
+  /* the tcp connect or ssl connect is now done. we collect the timing */
+  connect_timing_ = transport_socket_handle_->connect_timing();
 
   // Add a HttpProxy connection on top of the tcp socket.
   transport_socket_.reset(
diff --git a/net/socket/socks5_client_socket.cc b/net/socket/socks5_client_socket.cc
index 15031d9..a1ca973 100644
--- a/net/socket/socks5_client_socket.cc
+++ b/net/socket/socks5_client_socket.cc
@@ -44,7 +44,8 @@ COMPILE_ASSERT(sizeof(struct in6_addr) == 16, incorrect_system_size_of_IPv6);
 SOCKS5ClientSocket::SOCKS5ClientSocket(
     scoped_ptr<ClientSocketHandle> transport_socket,
     const HostResolver::RequestInfo& req_info,
-    const int multipathMode)
+    const int multipathMode,
+    LoadTimingInfo::ConnectTiming* connect_timing)
     : io_callback_(base::Bind(&SOCKS5ClientSocket::OnIOComplete,
                               base::Unretained(this))),
       transport_(transport_socket.Pass()),
@@ -59,7 +60,8 @@ SOCKS5ClientSocket::SOCKS5ClientSocket(
       host_request_info_(req_info),
       multipathMode_(multipathMode),
       username_(0),
-      net_log_(transport_->socket()->NetLog()) {
+      net_log_(transport_->socket()->NetLog()),
+      connect_timing_(connect_timing) {
 }
 
 SOCKS5ClientSocket::~SOCKS5ClientSocket() {
@@ -578,6 +580,9 @@ int SOCKS5ClientSocket::DoHandshakeWrite() {
   next_state_ = STATE_HANDSHAKE_WRITE_COMPLETE;
 
   if (buffer_.empty()) {
+    if (connect_timing_) {
+      connect_timing_->socks_handshake_start = base::TimeTicks::Now();
+    }
     int rv = BuildHandshakeWriteBuffer(&buffer_);
     if (rv != OK)
       return rv;
@@ -685,6 +690,9 @@ int SOCKS5ClientSocket::DoHandshakeReadComplete(int result) {
     completed_handshake_ = true;
     buffer_.clear();
     next_state_ = STATE_NONE;
+    if (connect_timing_) {
+      connect_timing_->socks_handshake_end = base::TimeTicks::Now();
+    }
     return OK;
   }
 
diff --git a/net/socket/socks5_client_socket.h b/net/socket/socks5_client_socket.h
index 6f6c301..8ca72b2 100644
--- a/net/socket/socks5_client_socket.h
+++ b/net/socket/socks5_client_socket.h
@@ -18,6 +18,7 @@
 #include "net/dns/host_resolver.h"
 #include "net/socket/stream_socket.h"
 #include "url/gurl.h"
+#include "net/base/load_timing_info.h"
 
 namespace net {
 
@@ -36,7 +37,8 @@ class NET_EXPORT_PRIVATE SOCKS5ClientSocket : public StreamSocket {
   // proxy side.
   SOCKS5ClientSocket(scoped_ptr<ClientSocketHandle> transport_socket,
                      const HostResolver::RequestInfo& req_info,
-                     const int multipathMode=0);
+                     const int multipathMode=0,
+                     LoadTimingInfo::ConnectTiming* connect_timing=NULL);
 
   // On destruction Disconnect() is called.
   virtual ~SOCKS5ClientSocket();
@@ -170,6 +172,8 @@ class NET_EXPORT_PRIVATE SOCKS5ClientSocket : public StreamSocket {
 
   BoundNetLog net_log_;
 
+  LoadTimingInfo::ConnectTiming* connect_timing_;
+
   DISALLOW_COPY_AND_ASSIGN(SOCKS5ClientSocket);
 };
 
diff --git a/net/socket/socks_client_socket_pool.cc b/net/socket/socks_client_socket_pool.cc
index fbca03a..7a35166 100644
--- a/net/socket/socks_client_socket_pool.cc
+++ b/net/socket/socks_client_socket_pool.cc
@@ -150,7 +150,8 @@ int SOCKSConnectJob::DoSOCKSConnect() {
   if (socks_params_->is_socks_v5()) {
     socket_.reset(new SOCKS5ClientSocket(transport_socket_handle_.Pass(),
                                          socks_params_->destination(),
-                                         socks_params_->multipathMode()));
+                                         socks_params_->multipathMode(),
+                                         &connect_timing_));
   } else {
     socket_.reset(new SOCKSClientSocket(transport_socket_handle_.Pass(),
                                         socks_params_->destination(),
diff --git a/net/socket/ssl_client_socket_pool.cc b/net/socket/ssl_client_socket_pool.cc
index 4797acd..2521161 100644
--- a/net/socket/ssl_client_socket_pool.cc
+++ b/net/socket/ssl_client_socket_pool.cc
@@ -405,6 +405,8 @@ int SSLConnectJob::DoCreateSSLSocket() {
     connect_timing_.connect_start = socket_connect_timing.connect_start;
     connect_timing_.dns_start = socket_connect_timing.dns_start;
     connect_timing_.dns_end = socket_connect_timing.dns_end;
+    connect_timing_.socks_handshake_start = socket_connect_timing.socks_handshake_start;
+    connect_timing_.socks_handshake_end = socket_connect_timing.socks_handshake_end;
   }
 
   ssl_socket_ = client_socket_factory_->CreateSSLClientSocket(
diff --git a/net/spdy/spdy_session.cc b/net/spdy/spdy_session.cc
index 1416bdc..834d77c 100644
--- a/net/spdy/spdy_session.cc
+++ b/net/spdy/spdy_session.cc
@@ -703,6 +703,10 @@ void SpdySession::InitializeWithSocket(
   is_secure_ = is_secure;
   certificate_error_code_ = certificate_error_code;
 
+  const LoadTimingInfo::ConnectTiming ct = connection_->connect_timing();
+  socks_handshake_rtt_ = (ct.socks_handshake_end - ct.socks_handshake_start);
+  MYDVLOG(2) << "___ socks handshake rtt= " << socks_handshake_rtt_.InSecondsF();
+
   NextProto protocol_negotiated =
       connection_->socket()->GetNegotiatedProtocol();
   if (protocol_negotiated != kProtoUnknown) {
@@ -3470,4 +3474,9 @@ SpdyStreamId SpdySession::PopStreamToPossiblyResume() {
   return 0;
 }
 
+base::TimeDelta SpdySession::get_initial_rtt_to_proxy() const
+{
+  return socks_handshake_rtt_;
+}
+
 }  // namespace net
diff --git a/net/spdy/spdy_session.h b/net/spdy/spdy_session.h
index cc804ea..34d5c37 100644
--- a/net/spdy/spdy_session.h
+++ b/net/spdy/spdy_session.h
@@ -532,6 +532,8 @@ class NET_EXPORT SpdySession : public BufferedSpdyFramerVisitorInterface,
   void notify_cache_hit(const PnPPageLoadID& pageloadID, const GURL& url,
 			const bool disk_or_mem_cache);
 
+  base::TimeDelta get_initial_rtt_to_proxy() const;
+
  private:
   friend class base::RefCounted<SpdySession>;
   friend class SpdyStreamRequest;
@@ -1210,6 +1212,8 @@ class NET_EXPORT SpdySession : public BufferedSpdyFramerVisitorInterface,
   // used by DeleteStream to easily find the PnPPageLoad
   std::map<SpdyStreamId, PnPPageLoadID> mainstream_id_to_pageloadID_;
 
+  base::TimeDelta socks_handshake_rtt_;
+
   TimeFunc time_func_;
 
   // Used for posting asynchronous IO tasks.  We use this even though
-- 
1.9.1


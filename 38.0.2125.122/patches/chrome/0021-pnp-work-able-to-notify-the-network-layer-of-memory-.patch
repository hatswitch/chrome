From a535c8f07326f128fee5bbcf56429ba5f4a90d84 Mon Sep 17 00:00:00 2001
From: Giang Nguyen <nguyen59@illinois.edu>
Date: Wed, 11 Feb 2015 13:12:36 -0600
Subject: [PATCH 21/22] pnp-work: able to notify the network layer of memory
 cache hits, allowing spdy session to reject push streams. still todo: cancel
 push streams already in progress. and also handle disk cache scenarios.

---
 content/browser/web_contents/web_contents_impl.cc |  41 ++++-
 content/browser/web_contents/web_contents_impl.h  |   8 +-
 content/common/view_messages.h                    |  17 +-
 content/renderer/render_frame_impl.cc             |  16 +-
 net/base/pnp_page_load_id.h                       |  55 ++++++
 net/http/http_network_session.cc                  |   8 +
 net/http/http_network_session.h                   |   2 +
 net/net.gypi                                      |   1 +
 net/spdy/spdy_http_stream.cc                      |   3 +-
 net/spdy/spdy_session.cc                          | 215 +++++++++++++++++++++-
 net/spdy/spdy_session.h                           |  38 +++-
 net/spdy/spdy_session_pool.cc                     |  57 +++++-
 net/spdy/spdy_session_pool.h                      |  17 ++
 net/spdy/spdy_stream.cc                           |  13 +-
 net/spdy/spdy_stream.h                            |  16 +-
 15 files changed, 468 insertions(+), 39 deletions(-)
 create mode 100644 net/base/pnp_page_load_id.h

diff --git a/content/browser/web_contents/web_contents_impl.cc b/content/browser/web_contents/web_contents_impl.cc
index 57ed3e7..b424ff7 100644
--- a/content/browser/web_contents/web_contents_impl.cc
+++ b/content/browser/web_contents/web_contents_impl.cc
@@ -162,6 +162,9 @@
 //   the user goes back.  The process only stays live if another tab is using
 //   it, but if so, the existing frame relationships will be maintained.
 
+#define MYDVLOG(level)				\
+  DVLOG(level) << __func__ << "(): "
+
 namespace content {
 namespace {
 
@@ -196,9 +199,17 @@ static int StartDownload(content::RenderFrameHost* rfh,
 void NotifyCacheOnIO(
     scoped_refptr<net::URLRequestContextGetter> request_context,
     const GURL& url,
-    const std::string& http_method) {
+    const std::string& http_method,
+    const int& renderer_id,
+    const unsigned long& main_resource_id) {
   request_context->GetURLRequestContext()->http_transaction_factory()->
       GetCache()->OnExternalCacheHit(url, http_method);
+  if (main_resource_id > 0) {
+    CHECK_GT(renderer_id, 0);
+    CHECK_EQ(http_method, "GET");
+    request_context->GetURLRequestContext()->http_transaction_factory()->
+      GetSession()->OnExternalCacheHit(url, renderer_id, main_resource_id);
+  }
 }
 
 // Helper function for retrieving all the sites in a frame tree.
@@ -2610,11 +2621,19 @@ void WebContentsImpl::OnThemeColorChanged(SkColor theme_color) {
 }
 
 void WebContentsImpl::OnDidLoadResourceFromMemoryCache(
-    const GURL& url,
-    const std::string& security_info,
-    const std::string& http_method,
-    const std::string& mime_type,
-    ResourceType resource_type) {
+  const ViewHostMsg_DidLoadResourceFromMemoryCache_Params& params)
+{
+  const GURL& url = params.url;
+  const std::string& security_info = params.security_info;
+  const std::string& http_method = params.http_method;
+  const std::string& mime_type = params.mime_type;
+  const ResourceType resource_type = params.resource_type;
+  const int renderer_id = GetRenderProcessHost()->GetID();
+
+  MYDVLOG(2) << "___ begin, url= " << url
+	     << " http_method= " << http_method << " renderer_id= " << renderer_id
+	     << " main_resource_id= " << params.main_resource_id;
+
   base::StatsCounter cache("WebKit.CacheHit");
   cache.Increment();
 
@@ -2629,7 +2648,7 @@ void WebContentsImpl::OnDidLoadResourceFromMemoryCache(
                           &signed_certificate_timestamp_ids);
   // TODO(alcutter,eranm): Pass signed_certificate_timestamp_ids into details
   LoadFromMemoryCacheDetails details(
-      url, GetRenderProcessHost()->GetID(), cert_id, cert_status, http_method,
+      url, renderer_id, cert_id, cert_status, http_method,
       mime_type, resource_type);
 
   controller_.ssl_manager()->DidLoadFromMemoryCache(details);
@@ -2641,13 +2660,15 @@ void WebContentsImpl::OnDidLoadResourceFromMemoryCache(
     scoped_refptr<net::URLRequestContextGetter> request_context(
         resource_type == RESOURCE_TYPE_MEDIA ?
             GetBrowserContext()->GetMediaRequestContextForRenderProcess(
-                GetRenderProcessHost()->GetID()) :
+                renderer_id) :
             GetBrowserContext()->GetRequestContextForRenderProcess(
-                GetRenderProcessHost()->GetID()));
+                renderer_id));
     BrowserThread::PostTask(
         BrowserThread::IO,
         FROM_HERE,
-        base::Bind(&NotifyCacheOnIO, request_context, url, http_method));
+        base::Bind(&NotifyCacheOnIO, request_context, url, http_method,
+		   renderer_id,
+		   http_method == "GET" ? params.main_resource_id : 0));
   }
 }
 
diff --git a/content/browser/web_contents/web_contents_impl.h b/content/browser/web_contents/web_contents_impl.h
index b5fe67e..643f8b2 100644
--- a/content/browser/web_contents/web_contents_impl.h
+++ b/content/browser/web_contents/web_contents_impl.h
@@ -43,6 +43,7 @@
 struct BrowserPluginHostMsg_ResizeGuest_Params;
 struct ViewHostMsg_DateTimeDialogValue_Params;
 struct ViewMsg_PostMessage_Params;
+struct ViewHostMsg_DidLoadResourceFromMemoryCache_Params;
 
 namespace content {
 class BrowserPluginEmbedder;
@@ -741,11 +742,8 @@ class CONTENT_EXPORT WebContentsImpl
 
   // IPC message handlers.
   void OnThemeColorChanged(SkColor theme_color);
-  void OnDidLoadResourceFromMemoryCache(const GURL& url,
-                                        const std::string& security_info,
-                                        const std::string& http_request,
-                                        const std::string& mime_type,
-                                        ResourceType resource_type);
+  void OnDidLoadResourceFromMemoryCache(
+      const ViewHostMsg_DidLoadResourceFromMemoryCache_Params& params);
   void OnDidDisplayInsecureContent();
   void OnDidRunInsecureContent(const std::string& security_origin,
                                const GURL& target_url);
diff --git a/content/common/view_messages.h b/content/common/view_messages.h
index 4497d50..354f5da 100644
--- a/content/common/view_messages.h
+++ b/content/common/view_messages.h
@@ -1140,12 +1140,17 @@ IPC_MESSAGE_ROUTED1(ViewHostMsg_DocumentAvailableInMainFrame,
 // The security info is non empty if the resource was originally loaded over
 // a secure connection.
 // Note: May only be sent once per URL per frame per committed load.
-IPC_MESSAGE_ROUTED5(ViewHostMsg_DidLoadResourceFromMemoryCache,
-                    GURL /* url */,
-                    std::string  /* security info */,
-                    std::string  /* http method */,
-                    std::string  /* mime type */,
-                    content::ResourceType /* resource type */)
+IPC_STRUCT_BEGIN(ViewHostMsg_DidLoadResourceFromMemoryCache_Params)
+  IPC_STRUCT_MEMBER(GURL, url)
+  IPC_STRUCT_MEMBER(std::string, security_info)
+  IPC_STRUCT_MEMBER(std::string, http_method)
+  IPC_STRUCT_MEMBER(std::string, mime_type)
+  IPC_STRUCT_MEMBER(unsigned long, main_resource_id)
+  IPC_STRUCT_MEMBER(content::ResourceType, resource_type)
+IPC_STRUCT_END()
+
+IPC_MESSAGE_ROUTED1(ViewHostMsg_DidLoadResourceFromMemoryCache,
+                    ViewHostMsg_DidLoadResourceFromMemoryCache_Params)
 
 // Sent when the renderer displays insecure content in a secure page.
 IPC_MESSAGE_ROUTED0(ViewHostMsg_DidDisplayInsecureContent)
diff --git a/content/renderer/render_frame_impl.cc b/content/renderer/render_frame_impl.cc
index 30d405f..6b07f0f 100644
--- a/content/renderer/render_frame_impl.cc
+++ b/content/renderer/render_frame_impl.cc
@@ -2738,13 +2738,15 @@ void RenderFrameImpl::didLoadResourceFromMemoryCache(
 
   // Let the browser know we loaded a resource from the memory cache.  This
   // message is needed to display the correct SSL indicators.
-  render_view_->Send(new ViewHostMsg_DidLoadResourceFromMemoryCache(
-      render_view_->GetRoutingID(),
-      url,
-      response.securityInfo(),
-      request.httpMethod().utf8(),
-      response.mimeType().utf8(),
-      WebURLRequestToResourceType(request)));
+  ViewHostMsg_DidLoadResourceFromMemoryCache_Params params;
+  params.url = url;
+  params.security_info = response.securityInfo();
+  params.http_method = request.httpMethod().utf8();
+  params.mime_type = response.mimeType().utf8();
+  params.main_resource_id = request.main_resource_id();
+  params.resource_type = WebURLRequestToResourceType(request);
+
+  Send(new ViewHostMsg_DidLoadResourceFromMemoryCache(routing_id_, params));
 }
 
 void RenderFrameImpl::didDisplayInsecureContent(blink::WebLocalFrame* frame) {
diff --git a/net/base/pnp_page_load_id.h b/net/base/pnp_page_load_id.h
new file mode 100644
index 0000000..7f1f08f
--- /dev/null
+++ b/net/base/pnp_page_load_id.h
@@ -0,0 +1,55 @@
+#ifndef NET_BASE_PNP_PAGE_LOAD_ID_H_
+#define NET_BASE_PNP_PAGE_LOAD_ID_H_
+
+#include "base/strings/string_number_conversions.h"
+
+namespace net {
+
+// Uniquely identifies a page load for pnp purposes
+struct PnPPageLoadID {
+  PnPPageLoadID() : renderer_id(-1), main_resource_id(0) {
+  }
+
+  PnPPageLoadID(int renderer_id, unsigned long main_resource_id)
+      : renderer_id(renderer_id),
+        main_resource_id(main_resource_id) {
+  }
+
+  // The unique ID of the child process (different from OS's PID).
+  int renderer_id;
+
+  unsigned long main_resource_id;
+
+  bool operator<(const PnPPageLoadID& other) const {
+    if (renderer_id == other.renderer_id)
+      return main_resource_id < other.main_resource_id;
+    return renderer_id < other.renderer_id;
+  }
+  bool operator==(const PnPPageLoadID& other) const {
+    return renderer_id == other.renderer_id &&
+        main_resource_id == other.main_resource_id;
+  }
+  bool operator!=(const PnPPageLoadID& other) const {
+    return renderer_id != other.renderer_id ||
+        main_resource_id != other.main_resource_id;
+  }
+  PnPPageLoadID& operator=(const PnPPageLoadID& other) {
+    renderer_id = other.renderer_id;
+    main_resource_id = other.main_resource_id;
+    return *this;
+  }
+
+  bool IsEmpty() const { return renderer_id == -1 && main_resource_id == 0; }
+
+  std::string ToString() const
+  {
+    std::string ret = base::IntToString(renderer_id);
+    ret += '_';
+    ret += base::IntToString(main_resource_id);
+    return ret;
+  };
+};
+
+}  // namespace content
+
+#endif  // NET_BASE_PNP_PAGE_LOAD_ID_H_
diff --git a/net/http/http_network_session.cc b/net/http/http_network_session.cc
index d3ecb3c..afd88c8 100644
--- a/net/http/http_network_session.cc
+++ b/net/http/http_network_session.cc
@@ -150,6 +150,7 @@ HttpNetworkSession::HttpNetworkSession(const Params& params)
                          params.spdy_initial_max_concurrent_streams,
                          params.spdy_max_concurrent_streams_limit,
                          params.time_func,
+                         params.enable_pnp,
                          params.trusted_spdy_proxy),
       http_stream_factory_(new HttpStreamFactoryImpl(this, false)),
       http_stream_factory_for_websocket_(new HttpStreamFactoryImpl(this, true)),
@@ -290,6 +291,13 @@ bool HttpNetworkSession::IsProtocolEnabled(AlternateProtocol protocol) const {
       protocol - ALTERNATE_PROTOCOL_MINIMUM_VALID_VERSION];
 }
 
+void HttpNetworkSession::OnExternalCacheHit(const GURL& url, const int& renderer_id,
+					    const unsigned long& main_resource_id)
+{
+  const PnPPageLoadID pageloadID(renderer_id, main_resource_id);
+  spdy_session_pool_.notify_cache_hit(pageloadID, url, false);
+}
+
 void HttpNetworkSession::GetNextProtos(
     std::vector<std::string>* next_protos) const {
   if (HttpStreamFactory::spdy_enabled()) {
diff --git a/net/http/http_network_session.h b/net/http/http_network_session.h
index 91e946d..85cc588 100644
--- a/net/http/http_network_session.h
+++ b/net/http/http_network_session.h
@@ -208,6 +208,8 @@ class NET_EXPORT HttpNetworkSession
 
   int multipathMode() const { return multipathMode_; }
   TorControl* tor_control() { return tor_control_; }
+  void OnExternalCacheHit(const GURL& url, const int& renderer_id,
+			  const unsigned long& main_resource_id);
 
   void GetNextProtos(std::vector<std::string>* next_protos) const;
 
diff --git a/net/net.gypi b/net/net.gypi
index 7a063a7..cfa423b 100644
--- a/net/net.gypi
+++ b/net/net.gypi
@@ -53,6 +53,7 @@
       'base/openssl_private_key_store.h',
       'base/openssl_private_key_store_android.cc',
       'base/openssl_private_key_store_memory.cc',
+      'base/pnp_page_load_id.h',
       'base/rand_callback.h',
       'base/registry_controlled_domains/registry_controlled_domain.cc',
       'base/registry_controlled_domains/registry_controlled_domain.h',
diff --git a/net/spdy/spdy_http_stream.cc b/net/spdy/spdy_http_stream.cc
index 0fa1846..8d1d635 100644
--- a/net/spdy/spdy_http_stream.cc
+++ b/net/spdy/spdy_http_stream.cc
@@ -210,7 +210,8 @@ int SpdyHttpStream::StartRequest(RequestPriority priority,
       SPDY_REQUEST_RESPONSE_STREAM, spdy_session_, request_info_->url,
       priority, stream_net_log,
       base::Bind(&SpdyHttpStream::OnStreamCreated,
-                 weak_factory_.GetWeakPtr(), callback));
+                 weak_factory_.GetWeakPtr(), callback),
+      request_info_);
 
   if (rv == OK) {
     stream_ = stream_request_.ReleaseStream();
diff --git a/net/spdy/spdy_session.cc b/net/spdy/spdy_session.cc
index 3c9ee72..1bd3e6e 100644
--- a/net/spdy/spdy_session.cc
+++ b/net/spdy/spdy_session.cc
@@ -429,7 +429,7 @@ void SplitPushedHeadersToRequestAndResponse(const SpdyHeaderBlock& headers,
   }
 }
 
-SpdyStreamRequest::SpdyStreamRequest() : weak_ptr_factory_(this) {
+SpdyStreamRequest::SpdyStreamRequest() : request_info_(NULL), weak_ptr_factory_(this) {
   Reset();
 }
 
@@ -443,7 +443,8 @@ int SpdyStreamRequest::StartRequest(
     const GURL& url,
     RequestPriority priority,
     const BoundNetLog& net_log,
-    const CompletionCallback& callback) {
+    const CompletionCallback& callback,
+    const HttpRequestInfo* request_info) {
   DCHECK(session);
   DCHECK(!session_);
   DCHECK(!stream_);
@@ -456,12 +457,16 @@ int SpdyStreamRequest::StartRequest(
   net_log_ = net_log;
   callback_ = callback;
 
+  request_info_ = request_info;
+  MYDVLOG(2) << "___ begin, url= " << url;
+
   base::WeakPtr<SpdyStream> stream;
   int rv = session->TryCreateStream(weak_ptr_factory_.GetWeakPtr(), &stream);
   if (rv == OK) {
     Reset();
     stream_ = stream;
   }
+  MYDVLOG(2) << "___ done, returning rv= " << rv;
   return rv;
 }
 
@@ -585,6 +590,7 @@ SpdySession::SpdySession(
     size_t max_concurrent_streams_limit,
     TimeFunc time_func,
     const HostPortPair& trusted_spdy_proxy,
+    const bool pnp_enabled,
     NetLog* net_log)
     : in_io_loop_(false),
       spdy_session_key_(spdy_session_key),
@@ -643,6 +649,7 @@ SpdySession::SpdySession(
           base::TimeDelta::FromSeconds(kDefaultConnectionAtRiskOfLossSeconds)),
       hung_interval_(base::TimeDelta::FromSeconds(kHungIntervalSeconds)),
       trusted_spdy_proxy_(trusted_spdy_proxy),
+      pnp_enabled_(pnp_enabled),
       time_func_(time_func),
       weak_factory_(this) {
   DCHECK_GE(protocol_, kProtoSPDYMinimumVersion);
@@ -879,9 +886,42 @@ int SpdySession::CreateStream(const SpdyStreamRequest& request,
       "Net.SpdyPriorityCount",
       static_cast<int>(request.priority()), 0, 10, 11);
 
+  if (pnp_enabled_) {
+    const HttpRequestInfo* request_info = request.request_info();
+    const HttpExtraPnpInfo* extra_pnp_info =
+      request_info ? request_info->extra_pnp_info : NULL;
+    if (extra_pnp_info && extra_pnp_info->main_resource_id_ > 0
+        && extra_pnp_info->is_main_resource_)
+    {
+      const PnPPageLoadID pageloadID(extra_pnp_info->renderer_id_,
+                                     extra_pnp_info->main_resource_id_);
+      pool_->add_pageload_id_to_session(pageloadID, this);
+
+      // remember the pageloadID to pageload mapping
+      std::pair<ActivePageLoadMap::iterator, bool> result =
+        pnp_pageloads_.insert(std::make_pair(pageloadID, PnPPageLoad((*stream)->instNum)));
+      CHECK(result.second);
+      std::pair<std::map<uint64, PnPPageLoadID>::iterator, bool> result2 =
+        mainstream_instNum_to_pageloadID_.insert(
+          std::make_pair((*stream)->instNum, pageloadID));
+      CHECK(result2.second);
+    }
+  }
+
   return OK;
 }
 
+SpdySession::PnPPageLoad::PnPPageLoad(const uint64 main_stream_instNum)
+  : main_stream_instNum_(main_stream_instNum)
+  , main_stream_id_(0)
+  , main_stream_done_(false)
+{
+}
+
+SpdySession::PnPPageLoad::~PnPPageLoad()
+{
+}
+
 void SpdySession::CancelStreamRequest(
     const base::WeakPtr<SpdyStreamRequest>& request) {
   DCHECK(request);
@@ -1887,6 +1927,32 @@ scoped_ptr<SpdyStream> SpdySession::ActivateCreatedStream(SpdyStream* stream) {
   CHECK_EQ(stream->stream_id(), 0u);
   CHECK(created_streams_.find(stream) != created_streams_.end());
   stream->set_stream_id(GetNewStreamId());
+
+  MYDVLOG(2) << "___ begin, stream id= " << stream->stream_id()
+             << " instnum= " << stream->instNum;
+  // update the main stream id, now that we now what it is.
+  if (pnp_enabled_) {
+    std::map<uint64, PnPPageLoadID>::iterator it =
+      mainstream_instNum_to_pageloadID_.find(stream->instNum);
+    if (it != mainstream_instNum_to_pageloadID_.end()) {
+      const PnPPageLoadID& pageloadID = it->second;
+      CHECK(!pageloadID.IsEmpty());
+      ActivePageLoadMap::iterator it2 = pnp_pageloads_.find(pageloadID);
+      CHECK(it2 != pnp_pageloads_.end());
+      PnPPageLoad& pageload = it2->second;
+      MYDVLOG(2) << "___ plid= " << pageloadID.ToString()
+                 << " main_stream_instNum_= " << pageload.main_stream_instNum_;
+      CHECK_EQ(pageload.main_stream_instNum_, stream->instNum);
+      pageload.main_stream_id_ = stream->stream_id();
+      MYDVLOG(2) << "___ updated main_stream_id_= " << pageload.main_stream_id_;
+
+      std::pair<std::map<SpdyStreamId, PnPPageLoadID>::iterator, bool> result =
+        mainstream_id_to_pageloadID_.insert(
+          std::make_pair(pageload.main_stream_id_, pageloadID));
+      CHECK(result.second);
+    }
+  }
+
   scoped_ptr<SpdyStream> owned_stream(stream);
   created_streams_.erase(stream);
   return owned_stream.Pass();
@@ -1903,6 +1969,8 @@ void SpdySession::InsertActivatedStream(scoped_ptr<SpdyStream> stream) {
 }
 
 void SpdySession::DeleteStream(scoped_ptr<SpdyStream> stream, int status) {
+  const SpdyStreamId stream_id = stream->stream_id();
+  MYDVLOG(2) << "___ begin, stream_id= " << stream_id;
   if (in_flight_write_stream_.get() == stream.get()) {
     // If we're deleting the stream for the in-flight write, we still
     // need to let the write complete, so we clear
@@ -1911,6 +1979,66 @@ void SpdySession::DeleteStream(scoped_ptr<SpdyStream> stream, int status) {
     in_flight_write_stream_.reset();
   }
 
+  if (pnp_enabled_) {
+    PnPPageLoadID pageloadID;
+    const SpdyStreamId associated_stream_id = stream->associated_stream_id();
+    const SpdyStreamId lookup_stream_id =
+      associated_stream_id > 0 ? associated_stream_id : stream_id;
+    std::map<SpdyStreamId, PnPPageLoadID>::iterator it =
+      mainstream_id_to_pageloadID_.find(lookup_stream_id);
+
+    if (associated_stream_id > 0) {
+      // stream_id is a push stream, so must have the pageloadID
+      CHECK(it != mainstream_id_to_pageloadID_.end());
+      pageloadID = (it->second);
+    } else {
+      // stream_id is a regular stream
+      if (it != mainstream_id_to_pageloadID_.end()) {
+        pageloadID = (it->second);
+      }
+    }
+
+    if (pageloadID.IsEmpty()) {
+      MYDVLOG(2) << "___ no plid found";
+    } else {
+      MYDVLOG(2) << "___ found plid= " << pageloadID.ToString();
+      ActivePageLoadMap::iterator it = pnp_pageloads_.find(pageloadID);
+      CHECK(it != pnp_pageloads_.end());
+      PnPPageLoad& pageload = it->second;
+
+      size_t numerased = 0;
+
+      if (pageload.main_stream_id_ == associated_stream_id) {
+        // remove push stream
+        std::set<SpdyStreamId>::const_iterator sit =
+          pageload.active_push_streams_.find(stream_id);
+        CHECK(sit != pageload.active_push_streams_.end());
+        MYDVLOG(2) << "___ current num push streams= "
+                   << pageload.active_push_streams_.size();
+        numerased = pageload.active_push_streams_.erase(stream_id);
+        CHECK_EQ(numerased, 1u);
+        MYDVLOG(2) << "___ new num push streams= "
+                   << pageload.active_push_streams_.size();
+      } else {
+        // it's the main stream
+        CHECK(!pageload.main_stream_done_);
+        MYDVLOG(2) << "___ setting main stream to done";
+        pageload.main_stream_done_ = true;
+        numerased = mainstream_instNum_to_pageloadID_.erase(stream->instNum);
+        CHECK_EQ(numerased, 1u);
+      }
+
+      if (pageload.IsDone()) {
+        MYDVLOG(2) << "___ page load is done => remove";
+        pool_->remove_pageload_id_to_session(pageloadID);
+        numerased = mainstream_id_to_pageloadID_.erase(pageload.main_stream_id_);
+        CHECK_EQ(numerased, 1u);
+        numerased = pnp_pageloads_.erase(pageloadID);
+        CHECK_EQ(numerased, 1u);
+      }
+    }
+  }
+
   write_queue_.RemovePendingWritesForStream(stream->GetWeakPtr());
   stream->OnClose(status);
 
@@ -2178,6 +2306,9 @@ void SpdySession::OnSynStream(SpdyStreamId stream_id,
                               bool fin,
                               bool unidirectional,
                               const SpdyHeaderBlock& headers) {
+  MYDVLOG(2) << "___ begin, stream_id= " << stream_id
+	     << " associated_stream_id= " << associated_stream_id;
+
   CHECK(in_io_loop_);
 
   if (GetProtocolVersion() >= SPDY4) {
@@ -2223,8 +2354,30 @@ void SpdySession::OnSynStream(SpdyStreamId stream_id,
   push_requests.Increment();
 
   const GURL url = active_it->second.stream->GetUrlFromHeaders();
-  MYDVLOG(2) << "___ received push SYN_STREAM for url= "
-             << url;
+  MYDVLOG(2) << "___ accepted push stream url= " << url;
+
+  if (pnp_enabled_) {
+    MYDVLOG(2) << "___ add it to the pnp page load's active push streams";
+    std::map<SpdyStreamId, PnPPageLoadID>::iterator it =
+      mainstream_id_to_pageloadID_.find(associated_stream_id);
+    CHECK(it != mainstream_id_to_pageloadID_.end());
+    const PnPPageLoadID& pageloadID = it->second;
+    CHECK(!pageloadID.IsEmpty());
+    MYDVLOG(2) << "___ plid= " << pageloadID.ToString();
+
+    ActivePageLoadMap::iterator it2 = pnp_pageloads_.find(pageloadID);
+    CHECK(it2 != pnp_pageloads_.end());
+
+    PnPPageLoad& pageload = it2->second;
+    CHECK_EQ(pageload.main_stream_id_, associated_stream_id);
+
+    MYDVLOG(2) << "___ page load's current num push streams= "
+               << pageload.active_push_streams_.size();
+    pageload.active_push_streams_.insert(stream_id);
+    MYDVLOG(2) << "___ page load's new num push streams= "
+               << pageload.active_push_streams_.size();
+  }
+
   base::MessageLoop::current()->PostTask(
       FROM_HERE, base::Bind(&SpdySession::check_push_interest_and_notify,
                             weak_factory_.GetWeakPtr(),
@@ -2233,6 +2386,7 @@ void SpdySession::OnSynStream(SpdyStreamId stream_id,
 
 void SpdySession::register_push_interest(const GURL& url, const base::Closure& cb)
 {
+  CHECK(pnp_enabled_);
   MYDVLOG(2) << "___ push_interest_ size= " << push_interest_.size();
   if (push_interest_.find(url) != push_interest_.end()) {
     LOG(WARNING) << "ERROR: interest in url= " << url << " already registered";
@@ -2244,6 +2398,7 @@ void SpdySession::register_push_interest(const GURL& url, const base::Closure& c
 
 void SpdySession::remove_push_interest(const GURL& url)
 {
+  CHECK(pnp_enabled_);
   MYDVLOG(2) << "___ push_interest_ size= " << push_interest_.size();
   if (push_interest_.find(url) == push_interest_.end()) {
     LOG(WARNING) << "ERROR: interest in url= " << url << " NOT registered";
@@ -2266,6 +2421,24 @@ void SpdySession::check_push_interest_and_notify(const GURL& url)
   }
 }
 
+void SpdySession::notify_cache_hit(const PnPPageLoadID& pageloadID, const GURL& url,
+				   const bool disk_or_mem_cache)
+{
+  MYDVLOG(2) << "___ begin, plid= " << pageloadID.ToString() << " url= " <<url;
+  CHECK(!pageloadID.IsEmpty());
+  ActivePageLoadMap::iterator it = pnp_pageloads_.find(pageloadID);
+  if (it != pnp_pageloads_.end()) {
+    PnPPageLoad& pageload = it->second;
+
+    CHECK_GT(pageload.main_stream_id_, 0u);
+
+    MYDVLOG(2) << "___ remembering the cache hit url";
+    pageload.cache_hits_.insert(url);
+    MYDVLOG(2) << "___ new num entries= " << pageload.cache_hits_.size();
+  }
+  MYDVLOG(2) << "___ done";
+}
+
 void SpdySession::DeleteExpiredPushedStreams() {
   if (unclaimed_pushed_streams_.empty())
     return;
@@ -2555,6 +2728,8 @@ bool SpdySession::TryCreatePushStream(SpdyStreamId stream_id,
                                       SpdyStreamId associated_stream_id,
                                       SpdyPriority priority,
                                       const SpdyHeaderBlock& headers) {
+  MYDVLOG(2) << "___ begin, stream_id= " << stream_id
+             << " associated_stream_id= " << associated_stream_id;
   // Server-initiated streams should have even sequence numbers.
   if ((stream_id & 0x1) != 0) {
     LOG(WARNING) << "Received invalid push stream id " << stream_id;
@@ -2606,6 +2781,8 @@ bool SpdySession::TryCreatePushStream(SpdyStreamId stream_id,
     return false;
   }
 
+  MYDVLOG(2) << "___ pushed url= " << gurl;
+
   // Verify we have a valid stream association.
   ActiveStreamMap::iterator associated_it =
       active_streams_.find(associated_stream_id);
@@ -2658,13 +2835,41 @@ bool SpdySession::TryCreatePushStream(SpdyStreamId stream_id,
     return false;
   }
 
+  if (pnp_enabled_) {
+    MYDVLOG(2) << "___ is this push stream useless?";
+    std::map<SpdyStreamId, PnPPageLoadID>::iterator it =
+      mainstream_id_to_pageloadID_.find(associated_stream_id);
+    CHECK(it != mainstream_id_to_pageloadID_.end());
+    const PnPPageLoadID& pageloadID = it->second;
+    CHECK(!pageloadID.IsEmpty());
+    MYDVLOG(2) << "___ plid= " << pageloadID.ToString();
+
+    ActivePageLoadMap::const_iterator it2 = pnp_pageloads_.find(pageloadID);
+    CHECK(it2 != pnp_pageloads_.end());
+
+    const PnPPageLoad& pageload = it2->second;
+    CHECK_EQ(pageload.main_stream_id_, associated_stream_id);
+
+    if (pageload.cache_hits_.find(gurl) != pageload.cache_hits_.end()) {
+      MYDVLOG(2) << "___ reject push stream= " << stream_id
+                 << " url= " << gurl << " due to cache hit";
+      EnqueueResetStreamFrame(
+        stream_id,
+        request_priority,
+        RST_STREAM_CANCEL,
+        "reject push stream url= " + gurl.spec() + " due to cache hit");
+      return false;
+    }
+  }
+
   scoped_ptr<SpdyStream> stream(new SpdyStream(SPDY_PUSH_STREAM,
                                                GetWeakPtr(),
                                                gurl,
                                                request_priority,
                                                stream_initial_send_window_size_,
                                                stream_initial_recv_window_size_,
-                                               net_log_));
+                                               net_log_,
+                                               associated_stream_id));
   stream->set_stream_id(stream_id);
 
   // In spdy4/http2 PUSH_PROMISE arrives on associated stream.
diff --git a/net/spdy/spdy_session.h b/net/spdy/spdy_session.h
index c7f76bf..8d2ffcc 100644
--- a/net/spdy/spdy_session.h
+++ b/net/spdy/spdy_session.h
@@ -164,7 +164,8 @@ class NET_EXPORT_PRIVATE SpdyStreamRequest {
                    const GURL& url,
                    RequestPriority priority,
                    const BoundNetLog& net_log,
-                   const CompletionCallback& callback);
+                   const CompletionCallback& callback,
+		   const HttpRequestInfo* request_info=NULL);
 
   // Cancels any pending stream creation request. May be called
   // repeatedly.
@@ -191,6 +192,7 @@ class NET_EXPORT_PRIVATE SpdyStreamRequest {
   // Accessors called by |session_|.
   SpdyStreamType type() const { return type_; }
   const GURL& url() const { return url_; }
+  const HttpRequestInfo* request_info() const { return request_info_; }
   RequestPriority priority() const { return priority_; }
   const BoundNetLog& net_log() const { return net_log_; }
 
@@ -200,6 +202,7 @@ class NET_EXPORT_PRIVATE SpdyStreamRequest {
   base::WeakPtr<SpdySession> session_;
   base::WeakPtr<SpdyStream> stream_;
   GURL url_;
+  const HttpRequestInfo* request_info_;
   RequestPriority priority_;
   BoundNetLog net_log_;
   CompletionCallback callback_;
@@ -248,6 +251,7 @@ class NET_EXPORT SpdySession : public BufferedSpdyFramerVisitorInterface,
               size_t max_concurrent_streams_limit,
               TimeFunc time_func,
               const HostPortPair& trusted_spdy_proxy,
+              const bool pnp_enabled,
               NetLog* net_log);
 
   virtual ~SpdySession();
@@ -522,6 +526,9 @@ class NET_EXPORT SpdySession : public BufferedSpdyFramerVisitorInterface,
   void register_push_interest(const GURL& url, const base::Closure& cb);
   void remove_push_interest(const GURL& url);
 
+  void notify_cache_hit(const PnPPageLoadID& pageloadID, const GURL& url,
+			const bool disk_or_mem_cache);
+
  private:
   friend class base::RefCounted<SpdySession>;
   friend class SpdyStreamRequest;
@@ -549,6 +556,26 @@ class NET_EXPORT SpdySession : public BufferedSpdyFramerVisitorInterface,
   FRIEND_TEST_ALL_PREFIXES(SpdySessionTest,
                            CancelReservedStreamOnHeadersReceived);
 
+  class PnPPageLoad
+  {
+
+  public:
+    PnPPageLoad(const uint64 main_stream_instNum);
+    ~PnPPageLoad();
+
+    bool IsDone() const { return main_stream_done_ && active_push_streams_.empty(); }
+
+    // needs the instnum because the stream id is known late.
+    const uint64 main_stream_instNum_;
+    SpdyStreamId main_stream_id_;
+    bool main_stream_done_;
+
+    /* set of url for which the browser has noticed cache hits */
+    std::set<GURL> cache_hits_;
+    std::set<SpdyStreamId> active_push_streams_;
+  };
+
+
   typedef std::deque<base::WeakPtr<SpdyStreamRequest> >
       PendingStreamRequestQueue;
 
@@ -1169,6 +1196,15 @@ class NET_EXPORT SpdySession : public BufferedSpdyFramerVisitorInterface,
   // different from those of their associated streams.
   HostPortPair trusted_spdy_proxy_;
 
+  const bool pnp_enabled_;
+
+  typedef std::map<PnPPageLoadID, PnPPageLoad> ActivePageLoadMap;
+  ActivePageLoadMap pnp_pageloads_;
+  // used by ActivateCreatedStream to easily find the PnPPageLoad
+  std::map<uint64, PnPPageLoadID> mainstream_instNum_to_pageloadID_;
+  // used by DeleteStream to easily find the PnPPageLoad
+  std::map<SpdyStreamId, PnPPageLoadID> mainstream_id_to_pageloadID_;
+
   TimeFunc time_func_;
 
   // Used for posting asynchronous IO tasks.  We use this even though
diff --git a/net/spdy/spdy_session_pool.cc b/net/spdy/spdy_session_pool.cc
index 59df64d..2b769c6 100644
--- a/net/spdy/spdy_session_pool.cc
+++ b/net/spdy/spdy_session_pool.cc
@@ -42,6 +42,7 @@ SpdySessionPool::SpdySessionPool(
     size_t initial_max_concurrent_streams,
     size_t max_concurrent_streams_limit,
     SpdySessionPool::TimeFunc time_func,
+    const bool pnp_enabled,
     const std::string& trusted_spdy_proxy)
     : http_server_properties_(http_server_properties),
       transport_security_state_(transport_security_state),
@@ -63,7 +64,9 @@ SpdySessionPool::SpdySessionPool(
       max_concurrent_streams_limit_(max_concurrent_streams_limit),
       time_func_(time_func),
       trusted_spdy_proxy_(
-          HostPortPair::FromString(trusted_spdy_proxy)) {
+          HostPortPair::FromString(trusted_spdy_proxy)),
+      pnp_enabled_(pnp_enabled)
+{
   DCHECK(default_protocol_ >= kProtoSPDYMinimumVersion &&
          default_protocol_ <= kProtoSPDYMaximumVersion);
   NetworkChangeNotifier::AddIPAddressObserver(this);
@@ -114,6 +117,7 @@ base::WeakPtr<SpdySession> SpdySessionPool::CreateAvailableSessionFromSocket(
                       max_concurrent_streams_limit_,
                       time_func_,
                       trusted_spdy_proxy_,
+                      pnp_enabled_,
                       net_log.net_log()));
 
   new_session->InitializeWithSocket(
@@ -333,6 +337,57 @@ void SpdySessionPool::OnCACertChanged(const X509Certificate* cert) {
   CloseCurrentSessions(ERR_CERT_DATABASE_CHANGED);
 }
 
+void SpdySessionPool::add_pageload_id_to_session(const PnPPageLoadID& pageloadID,
+						 SpdySession* session)
+{
+  MYDVLOG(2) << "___ begin, plid= " << pageloadID.ToString() << " session= " << session;
+  MYDVLOG(2) << "___ current size= " << pageload_id_to_session_.size();
+  CHECK(pnp_enabled_);
+  PageLoadIDToSessionMap::const_iterator it = pageload_id_to_session_.find(pageloadID);
+  if (it != pageload_id_to_session_.end()) {
+    NOTREACHED();
+  }
+  pageload_id_to_session_[pageloadID] = session->GetWeakPtr();
+  MYDVLOG(2) << "___ new size= " << pageload_id_to_session_.size();
+}
+
+SpdySession* SpdySessionPool::lookup_session_by_page_load_id(const PnPPageLoadID& pageloadID)
+{
+  MYDVLOG(2) << "___ plid= " << pageloadID.ToString()
+	     << " current size= " << pageload_id_to_session_.size();
+  PageLoadIDToSessionMap::iterator it = pageload_id_to_session_.find(pageloadID);
+  if (it != pageload_id_to_session_.end()) {
+    MYDVLOG(2) << "___ found";
+    return it->second.get();
+  } else {
+    MYDVLOG(2) << "___ NOT found";
+    return NULL;
+  }
+}
+
+void SpdySessionPool::remove_pageload_id_to_session(const PnPPageLoadID& pageloadID)
+{
+  MYDVLOG(2) << "___ begin, plid= " << pageloadID.ToString();
+  MYDVLOG(2) << "___ current size= " << pageload_id_to_session_.size();
+  CHECK(pnp_enabled_);
+  PageLoadIDToSessionMap::iterator it = pageload_id_to_session_.find(pageloadID);
+  if (it == pageload_id_to_session_.end()) {
+    NOTREACHED();
+  }
+  pageload_id_to_session_.erase(it);
+  MYDVLOG(2) << "___ new size= " << pageload_id_to_session_.size();
+}
+
+void SpdySessionPool::notify_cache_hit(const PnPPageLoadID& pageloadID, const GURL& url,
+				       const bool disk_or_mem_cache)
+{
+  //  MYDVLOG(2)<< "___ begin, plid= "<< pageloadID.ToString() << " url= " <<url;
+  SpdySession* spdy_session = lookup_session_by_page_load_id(pageloadID);
+  CHECK(spdy_session);
+  spdy_session->notify_cache_hit(pageloadID, url, disk_or_mem_cache);
+  //  MYDVLOG(2) << "___ done";
+}
+
 bool SpdySessionPool::IsSessionAvailable(
     const base::WeakPtr<SpdySession>& session) const {
   for (AvailableSessionMap::const_iterator it = available_sessions_.begin();
diff --git a/net/spdy/spdy_session_pool.h b/net/spdy/spdy_session_pool.h
index 2fbb030..3dc4108 100644
--- a/net/spdy/spdy_session_pool.h
+++ b/net/spdy/spdy_session_pool.h
@@ -26,6 +26,8 @@
 #include "net/spdy/spdy_session_key.h"
 #include "net/ssl/ssl_config_service.h"
 
+#include "net/base/pnp_page_load_id.h"
+
 namespace net {
 
 class AddressList;
@@ -60,6 +62,7 @@ class NET_EXPORT SpdySessionPool
       size_t initial_max_concurrent_streams,
       size_t max_concurrent_streams_limit,
       SpdySessionPool::TimeFunc time_func,
+      const bool pnp_enabled,
       const std::string& trusted_spdy_proxy);
   virtual ~SpdySessionPool();
 
@@ -147,6 +150,16 @@ class NET_EXPORT SpdySessionPool
   virtual void OnCertAdded(const X509Certificate* cert) OVERRIDE;
   virtual void OnCACertChanged(const X509Certificate* cert) OVERRIDE;
 
+
+  void add_pageload_id_to_session(const PnPPageLoadID& pageloadID,
+				  SpdySession* session);
+  void remove_pageload_id_to_session(const PnPPageLoadID& pageloadID);
+  SpdySession* lookup_session_by_page_load_id(const PnPPageLoadID& pageloadID);
+
+  // "disk_or_mem_cache" = true for disk cache.
+  void notify_cache_hit(const PnPPageLoadID& pageloadID, const GURL& url,
+			const bool disk_or_mem_cache);
+
  private:
   friend class SpdySessionPoolPeer;  // For testing.
 
@@ -229,6 +242,10 @@ class NET_EXPORT SpdySessionPool
   // different from those of their associated streams.
   HostPortPair trusted_spdy_proxy_;
 
+  const bool pnp_enabled_;
+  typedef std::map<PnPPageLoadID, base::WeakPtr<SpdySession> > PageLoadIDToSessionMap;
+  PageLoadIDToSessionMap pageload_id_to_session_;
+
   DISALLOW_COPY_AND_ASSIGN(SpdySessionPool);
 };
 
diff --git a/net/spdy/spdy_stream.cc b/net/spdy/spdy_stream.cc
index 40ec654..16d9d9a 100644
--- a/net/spdy/spdy_stream.cc
+++ b/net/spdy/spdy_stream.cc
@@ -17,6 +17,8 @@
 
 namespace net {
 
+uint64 SpdyStream::next_instNum_ = 1;
+
 namespace {
 
 base::Value* NetLogSpdyStreamErrorCallback(SpdyStreamId stream_id,
@@ -83,9 +85,12 @@ SpdyStream::SpdyStream(SpdyStreamType type,
                        RequestPriority priority,
                        int32 initial_send_window_size,
                        int32 initial_recv_window_size,
-                       const BoundNetLog& net_log)
-    : type_(type),
+                       const BoundNetLog& net_log,
+		       const SpdyStreamId associated_stream_id)
+    : instNum(next_instNum_),
+      type_(type),
       stream_id_(0),
+      associated_stream_id_(associated_stream_id),
       url_(url),
       priority_(priority),
       send_stalled_by_flow_control_(false),
@@ -105,11 +110,15 @@ SpdyStream::SpdyStream(SpdyStreamType type,
       recv_bytes_(0),
       write_handler_guard_(false),
       weak_ptr_factory_(this) {
+  ++next_instNum_;
   CHECK(type_ == SPDY_BIDIRECTIONAL_STREAM ||
         type_ == SPDY_REQUEST_RESPONSE_STREAM ||
         type_ == SPDY_PUSH_STREAM);
   CHECK_GE(priority_, MINIMUM_PRIORITY);
   CHECK_LE(priority_, MAXIMUM_PRIORITY);
+  if (type_ == SPDY_PUSH_STREAM) {
+    CHECK_GT(associated_stream_id_, 0u);
+  }
 }
 
 SpdyStream::~SpdyStream() {
diff --git a/net/spdy/spdy_stream.h b/net/spdy/spdy_stream.h
index ad916bc..58bbeff 100644
--- a/net/spdy/spdy_stream.h
+++ b/net/spdy/spdy_stream.h
@@ -164,7 +164,8 @@ class NET_EXPORT_PRIVATE SpdyStream {
              RequestPriority priority,
              int32 initial_send_window_size,
              int32 initial_recv_window_size,
-             const BoundNetLog& net_log);
+             const BoundNetLog& net_log,
+	     const SpdyStreamId associated_stream_id=0);
 
   ~SpdyStream();
 
@@ -187,6 +188,11 @@ class NET_EXPORT_PRIVATE SpdyStream {
   SpdyStreamId stream_id() const { return stream_id_; }
   void set_stream_id(SpdyStreamId stream_id) { stream_id_ = stream_id; }
 
+  /* the stream id with which |this| stream is associated. is 0 if
+   * this stream is not associated with any other stream.
+   */
+  SpdyStreamId associated_stream_id() const { return associated_stream_id_; }
+
   const GURL& url() const { return url_; }
 
   RequestPriority priority() const { return priority_; }
@@ -438,7 +444,14 @@ class NET_EXPORT_PRIVATE SpdyStream {
 
   SpdyMajorVersion GetProtocolVersion() const;
 
+  /* monolithic unique SpdyStream instance number. not related to the
+   * protocol stream id.
+   */
+  const uint64 instNum;
+
  private:
+  static uint64 next_instNum_;
+
   class SynStreamBufferProducer;
   class HeaderBufferProducer;
 
@@ -495,6 +508,7 @@ class NET_EXPORT_PRIVATE SpdyStream {
   const SpdyStreamType type_;
 
   SpdyStreamId stream_id_;
+  const SpdyStreamId associated_stream_id_;
   const GURL url_;
   const RequestPriority priority_;
 
-- 
1.9.1


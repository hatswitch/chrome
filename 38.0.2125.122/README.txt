this directory contains patches against release 38.0.2125.122 of
chromium at commit:

-----
commit 480fc5d14778f9987917d73040c8b5f3604eaace
Author: chrome-tpm <chrome-tpm@google.com>
Date:   Wed Nov 5 13:16:12 2014 -0800
-----

for chromium side, and commit:

-----
commit 1493aa587a0f52182dcb7830a55528bf0b92af9f
Author: ed@opera.com <ed@opera.com>
Date:   Thu Oct 9 14:30:39 2014 +0000
-----

for the third_party/WebKit side, and commit:

commit d71adf709268b5bfd5d67faf97429b23c2948354
Author: ishell@chromium.org <ishell@chromium.org>
Date:   Wed Nov 5 09:10:08 2014 +0000

    Version 3.28.71.19 (merged r23127)
    ...

of v8.

there is also code that work with the patched chrome.

NOTE: the patches are adding some new source/header files, so you
might need to rerun 'gclient runhooks'/'./build/gyp_chromium' before
building with ninja.


OPTIONAL: might be better to disable various features before building.
for example, safe browsing feature might cause network requests that
clutter our logging. I am configuring like this:

./build/gyp_chromium -Goutput_dir=/mnt/data/buildchrome \
    -Dcomponent=shared_library -Ddisable_nacl=1 -Dsafe_browsing=0 \
    -Denable_google_now=0 -Denable_webrtc=0 \
    -Denable_themes=0 -Ddisable_ftp_support=1 -Ddisable_pnacl=1

!!! EXCEPT the build doesn't seem to like some of these options, e.g.,
    the final link stage complains of "error: undefined reference to
    'ThemeSource::ThemeSource(Profile*)'" and "undefined reference to
    'SafeBrowsingProtocolConfig::~SafeBrowsingProtocolConfig()'". Since
    rerunning gyp_chromium can mean rebuilding everything (can take 2
    hours), I don't want to trial-and-error on all of these, so I'm
    just going to keep all features enabled by default.

### JavaScript Promises

JavaScript `Promise`s
([link](http://www.html5rocks.com/en/tutorials/es6/promises/)) are better ways
to do event callbacks.

The user gives a callback function to a `Promise` object's `then()`
function. The `Promise` object might be **(1)** resolved already
(i.e., the value is ready), or it might be **(2)** resolved in the
future. Regardless of case **(1)** or **(2)**, when it's time to call
the user's callback function (e.g., _now_ in case **(1)**), it is not
called directly/synchronously; rather, the call is enqueued as a
**microtask** and will be called later during the next microtask
checkpoint
([link](https://html.spec.whatwg.org/multipage/webappapis.html#processing-model-8)).

For example, consider this example:
```
<html>
<body>
<script type="text/javascript">
function pausecomp(millis) {
  var date = new Date();
  var curDate = null;
  do { curDate = new Date(); }
  while(curDate-date < millis);
}

var p1 = Promise.resolve(12345);
</script>

<script type="text/javascript">

p1.then(function(result) {
  console.log(result);
});

pausecomp(3000);
</script>

</body>
</html>
```

Line `var p1 = Promise.resolve(12345);` means the promise `p1` is
immediately ready/resolved. Later, in the second script, when
`p1.then()` is called, even though `p1` is already resolved,
`console.log(result);` won't be called until after `pausecomp(3000);`
(i.e., busy wait for 3 seconds) has been called and the second script
has finished running.

Of course in practice promises are not as trivial as in the above
example---e.g., they can involve loading some network resource---then
we'd want to be able to model this correctly, i.e., the callback can
only be called after **both (a)** the promised network resource has
been loaded (this depends on network conditions/caching) **and (b)**
the second script has finished running.

However, the `Promise`s are implemented in multiple places: in WebKit
`third_party/WebKit/Source/bindings/core/v8/ScriptPromise.cpp`
etc. (used by `FontFaceSet`(*), for example), and natively in JavaScript
`src/v8/src/promise.js` (used by our simple example above). The latter
is challenging for us to instrument, and this remains a TODO.

(*) this is because webkit supports the [font face set load API](https://drafts.csswg.org/css-font-loading/#font-face-set-load)
#!/usr/bin/env python3

import time, sys, os
import argparse
from time import sleep
import shutil
import subprocess

g_verbose = False

def dlog(s):
    if g_verbose:
        print( s)
        pass
    pass

from urllib.parse import urlparse
def get_page_output_dirname(page_url):
    parsed = urlparse(page_url)
    dname = parsed.netloc + parsed.path.rstrip('/')
    return dname.replace('/', '_')

def load_page_file(file_path):
    f = open(file_path, 'r')

    # return list of (name, url) tuples
    retlist = []

    # sets to catch duplicates
    names = set()
    urls = set()
    i = 1
    for lineno, line in enumerate(f.readlines()):
        line = line.strip()
        if len(line) == 0 or line[0] == '#':
            # skip empty and comment lines
            continue
        parts = line.split(' ')
        numparts = len(parts)
        assert(numparts==1), 'bad line "{}" has {} parts'.format(line, numparts)
        url = parts[0]
        name = get_page_output_dirname(url)
        assert name not in names, 'duplicate name "{}"'.format(name)
        names.add(name)
        assert url not in urls, 'duplicate url "{}"'.format(url)
        urls.add(url)
        retlist.append((name, url, 10))
        i += 1
        pass


        # if numparts != 2 and numparts != 3:
        #     print 'ERROR: line %u in page file %s does not have 2 or 3 parts' % (
        #         lineno+1, file_path)
        #     sys.exit(1)
        #     pass
        # name = parts[0]
        # url = parts[1]
        # if name in names:
        #     print 'ERROR: page file %s has duplicate page name "%s"' % (
        #         file_path, name)
        #     sys.exit(1)
        #     pass
        # if url in urls:
        #     print 'ERROR: page file %s has duplicate page URL "%s"' % (
        #         file_path, url)
        #     sys.exit(1)
        #     pass
        # names.add(name)
        # urls.add(url)
        # if numparts == 3:
        #     load_wait = int(parts[2])
        #     pass
        # else:
        #     # by default, gives page 8 seconds to load
        #     load_wait = 8
        #     pass
        # retlist.append((name, url, load_wait))
        # pass
    f.close()

    return retlist

def terminate_renderer_proceses():
    import psutil

    for proc in psutil.process_iter():
        try:
            pinfo = proc.as_dict(attrs=['pid', 'name', 'cmdline'])
            if pinfo['name'] == 'chrome':
                if pinfo['cmdline'] and '--type=renderer' in pinfo['cmdline'][0].split():
                    pid = pinfo['pid']
                    proc.terminate()
                    dlog('terminated renderer process {}'.format(pid))
                    pass
                pass
            pass
        except Exception as e:
            print( 'EXCEPTION:', str(e))
            pass
        pass

    return

def do_one_page(page_name,
                url,
                chrome_path,
                chrome_is_debug_binary,
                exp_output_dir,
                load_wait,
                ):

    print( 'page name [%s], url [%s]' % (page_name, url))
    page_output_dir = exp_output_dir + '/' + page_name

    assert not os.path.exists(page_output_dir)

    os.mkdir(page_output_dir)

    file_devnull = open(os.devnull, 'w')

    print( '=============================')

    subprocess.call('killall -9 chrome',
                    shell=True, stdout=file_devnull, stderr=file_devnull)
    time.sleep(1)

    #
    # build up the cmd list
    #

    cmd = [
        chrome_path,
        '--incognito',
        '--enable-logging=stderr',
        '--v=0',
        '--vmodule=web_url_loader_impl=2,chrome_browser_main_posix=2,render_process_host_impl=2,http_stream_parser=2,http_network_transaction=2',
#        '--vmodule=chrome_browser_main_posix=0,render_process_host_impl=0,http_stream_parser=0,http_network_transaction=0',
        '--safebrowsing-disable-download-protection',
        '--disable-client-side-phishing-detection',
        '--disable-component-extensions-with-background-pages',
        '--disable-default-apps',
        '--disable-preconnect',
        '--blink-platform-log-channels=PageModeling',
        '--disable-background-networking',
        '--extract-page-model',
        '--disable-background-parser',
        '--use-spdy=off',
        '--no-first-run',
        '--mute-audio',
    ]

    # some directory options
    disk_cache_dir_path = './.chromediskcachedir'
    user_data_dir_path = './.chromedatadir'
    for optname, dpath in (
            ('user-data-dir', user_data_dir_path),
            ('disk-cache-dir', disk_cache_dir_path),
    ):
        if os.path.exists(dpath):
            # assume it's a dir
            shutil.rmtree(dpath)
            assert not os.path.exists(dpath)
            pass
        cmd.append('--%s=%s' % (optname, dpath))
        pass

    cmd.append(url)

    dlog('chrome cmd: [%s]' % (' '.join(cmd)))

    chrome_debug_log_file_path = user_data_dir_path + '/chrome_debug.log' if not chrome_is_debug_binary else "/home/me/chromium-src-for-15.04/src/out/Debug/chrome_debug.log"

    #
    # go browse!
    #
    output_file = open(page_output_dir + '/stderr.txt', 'w')
    chrome_process = subprocess.Popen(
        cmd, stdout=output_file, stderr=output_file)
    time.sleep(load_wait)

    # terminate renderer processes
    terminate_renderer_proceses()
    sleep(1)

    # now terminate all chrome processes
    chrome_process.terminate()
    subprocess.call('killall chrome',
                    shell=True, stdout=file_devnull, stderr=file_devnull)
    sleep(1)

    # now kill all chrome processes
    subprocess.call('killall -9 chrome',
                    shell=True, stdout=file_devnull, stderr=file_devnull)
    sleep(1)

    output_file.close()

    # dlog('saving some log files')
    # for filepath in (
    #         chrome_debug_log_file_path,
    # ):
    #     # copy instead of move
    #     shutil.copy(filepath, page_output_dir)
    #     pass

    # files_to_compress = [
    #     page_output_dir + '/chrome_debug.log',
    # ]
    # # use Popen to let it happen in background
    # subprocess.Popen(
    #     ['bzip2'] + files_to_compress,
    #     stdout=file_devnull, stderr=file_devnull)

    return


def run(page_file_path,
        output_dir,
        release_chrome_path = None,
        debug_chrome_path = None,
        ):

    if release_chrome_path:
        chrome_path = release_chrome_path
        chrome_is_debug_binary = False
        pass
    else:
        chrome_path = debug_chrome_path
        chrome_is_debug_binary = True
        pass

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
        pass
    else:
        print( 'ERROR: output dir %s already exists. quitting to prevent overwrite' % (output_dir))
        sys.exit(-1)
        pass

    page_name_to_url_and_wait = load_page_file(page_file_path)

    summary_file = open(output_dir + '/summary.txt', 'w')
    summary_file.write('command-line args:\n%s\n' % (' '.join(sys.argv)))
    summary_file.write('\nstart: %s\n' % (time.ctime()))
    summary_file.close()

    shutil.copy(page_file_path, output_dir)

    for page_name, url, load_wait in page_name_to_url_and_wait:
        print ('===================================')
        do_one_page(page_name, url, chrome_path, chrome_is_debug_binary, output_dir, load_wait)
        pass
    pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description=''
        )

    parser.add_argument("-v", "--verbose",
                        action="store_true", dest="verbose", default=False,
                        help="be verbose")

    parser.add_argument("--debug-chrome-path",
                        help='path to debug chrome')

    parser.add_argument("--release-chrome-path",
                        help='path to release chrome')

    parser.add_argument("page_file_path",
                        metavar='page-file',
                        help="page containing which pages to load, each line is '<name> <url> [<wait>]'")
    parser.add_argument("output_dir_path",
                        metavar='output-dir',
                        help='path to directory to save results')

    args = parser.parse_args()

    g_verbose = args.verbose

    run(page_file_path = args.page_file_path,
        debug_chrome_path = args.debug_chrome_path,
        release_chrome_path = args.release_chrome_path,
        output_dir = args.output_dir_path,
        )
    pass

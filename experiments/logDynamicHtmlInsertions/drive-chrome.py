#!/usr/bin/python

import time
import sys
import pprint

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
import threading
from Queue import Queue
from Queue import Empty as QueueEmptyException
from selenium.webdriver.chrome.options import Options as ChromeOptions
import argparse

g_verbose = False

def dlog(s):
    if g_verbose:
        print '%s' % (s)
        pass
    return

LOAD_OK = 0
LOAD_TIMEDOUT = 1
LOAD_OTHER_ERROR = 2

def load_page(driver, url):
    dlog('callin get() ' + url)
    try:
        driver.get(url)
        dlog('get() returned')
        pass
    except TimeoutException:
        print 'error: url= [%s] timedout' % (url)
        return LOAD_TIMEDOUT
    except:
        print 'error: url= [%s] other error' % (url), sys.exc_info()[0]
        return LOAD_OTHER_ERROR
    return LOAD_OK

def get_timestamp():
    return time.time()

def run(urls,
        load_timeout_sec=30.0,
        disable_javascript=False,
        load_test_page=False,
        chrome_driver_port=9515,
        chrome_path=None,
        chrome_options=[],
        ):

    options = ChromeOptions()

    for o in chrome_options:
        options.add_argument(o)
        pass

    for common_arg in (
        'no-first-run',
        'no-message-box',
        'performance-monitor-gathering',
        'no-default-browser-check',
        #'diagnostics', # this causes driver (e.g., 2.11 and 2.12) to not work (stuck without launching chrome)
        ):
        options.add_argument(common_arg)
        pass
        
    ## command line switch --disable-application-cache,
    ## --disk-cache-size, and --media-cache-size seem to work only
    ## partially. for example, loading cnn.com without these switches
    ## will result in ~2 MB in the disk cache dir. with these switches
    ## and cache sizes at 1 (byte), the disk cache dir still grows to
    ## ~3-400 KB. according to
    ## http://www.chromium.org/administrators/policy-list-3, these
    ## cache size values are only hints to chrome; chrome will use
    ## minimum of a few MB regardless of what values are specified for
    ## these command line switches.
    ##
    ## so, it seems we have to just restart the browser and delete the
    ## disk cache dir before every load. (it doesn't seem possible to
    ## use webdriver to clear the cache with shift-alt-delete, etc.)


    if chrome_path:
        options.binary_location = chrome_path
        pass

    capabilities = options.to_capabilities()

    if disable_javascript:
        capabilities['javascriptEnabled'] = False
        pass

    if g_verbose:
        pprint.pprint(capabilities)
        pass

    driver = webdriver.Remote("http://127.0.0.1:%u" % (chrome_driver_port), capabilities)

    # start a thread to load the page, and the main thread will wait
    # for that thread to finish loading the page and put a True into
    # the queue.

    if load_test_page:
        # first, load a small page to get the ssl handshake and spdy
        # session setup
        testurl = 'http://web.engr.illinois.edu/~nguyen59/tmp/blank.html'
        dlog('loading sanity test page')

        result = load_page(driver, testurl)
        if result == LOAD_OK:
            dlog('test page is loaded, but is it correct? ...')
            if driver.title != 'my blank page for sanity check':
                print '%s: error: test page has unexpected title: [%s]' % (
                    get_timestamp(), driver.title)
                return
            else:
                dlog('it is good')
                time.sleep(0.5)
                pass
            pass
        else:
            return

        pass

    dlog('start loading the url(s)')
    #time.sleep(10)
    for url in urls:
        start_time = get_timestamp()
        print '%s (%s): start loading url= [%s]' % (
            start_time, time.ctime(start_time), url)
        dlog('start_time: ' + str(start_time))

        driver.set_page_load_timeout(load_timeout_sec)
        result = load_page(driver, url)
        if result == LOAD_OK:
            done_time = get_timestamp()
            dlog('done_time: ' + str(done_time))
            page_load_time_s = (done_time - start_time)
            print '%s (%s): done loading page= [%s]. _totalpagetime= %.1f s' % (
                done_time, time.ctime(done_time), url, page_load_time_s)
            pass

        dlog('wait for a few seconds...')
        time.sleep(1)

        pass # end for

    driver.quit()

    return 0


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description='''

!!!! make sure chromedriver runs in a directory that does not contain
     files / directories that have name like chrome or chromium,
     because it would try to start those as the browser instead of
     system paths like /usr/bin/chrome

'''
        )

    parser.add_argument("-v", "--verbose",
                        action="store_true", dest="verbose", default=False,
                        help="be verbose")

    parser.add_argument('--timeout', type=int, dest='load_timeout_sec',
                        default=30,
                        help="page load timeout in seconds")

    parser.add_argument("--disable-javascript",
                        action="store_true", dest="disable_javascript", default=False,
                        help="disable javascript")

    parser.add_argument("--load-test-page",
                        action="store_true", dest="load_test_page", default=False,
                        help="load the test page first so the ssl/spdy session setup is done")

    parser.add_argument('--chrome-driver-port', type=int,
                        dest='chrome_driver_port',
                        default=9515,
                        help="port the chromedriver is listening on")
    parser.add_argument("--chrome-path", dest='chrome_path',
                        help='path to chrome')
    parser.add_argument("--chrome-option", action='append', dest='chrome_options',
                        help="other options to pass to chrome, e.g., \"enable-xyz\" or \"abc-mode=1234\"")

    parser.add_argument("url", nargs='+',
                        help="url(s) of load")

    args = parser.parse_args()

    g_verbose = args.verbose

    code = run(
        args.url,
        load_timeout_sec = args.load_timeout_sec,
        disable_javascript = args.disable_javascript,
        load_test_page = args.load_test_page,
        chrome_driver_port = args.chrome_driver_port,
        chrome_path = args.chrome_path,
        chrome_options = args.chrome_options,
        )
    sys.exit(code)

    pass

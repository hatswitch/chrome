#!/usr/bin/python

import time, sys, os
import argparse
from time import sleep
import shutil
import subprocess

g_verbose = False

def dlog(s):
    if g_verbose:
        print s
        pass
    pass

def killself():
    os.system('kill -9 %d' % (os.getpid()))
    pass

class Timeout(Exception):
    pass


def wait_for_process_with_timeout(process, timeout, pollInterval=3):
    totalWait = 0
    while totalWait < timeout:
        dlog('totalWait %u' % (totalWait))
        code = process.poll()
        dlog('poll returned: [%s]' % (str(code)))
        if code != None:
            return code
        dlog('sleeping %u seconds...' % (pollInterval))
        sleep(pollInterval)
        totalWait += pollInterval
        pass
    raise Timeout()

def load_page_file(file_path):
    f = open(file_path, 'r')

    # return list of (name, url) tuples
    retlist = []

    # sets to catch duplicates
    names = set()
    urls = set()

    for lineno, line in enumerate(f.readlines()):
        line = line.strip()
        if len(line) == 0 or line[0] == '#':
            # skip empty and comment lines
            continue
        parts = line.split(' ')
        numparts = len(parts)
        if numparts != 2 and numparts != 3:
            print 'ERROR: line %u in page file %s does not have 2 or 3 parts' % (
                lineno+1, file_path)
            sys.exit(1)
            pass
        name = parts[0]
        url = parts[1]
        if name in names:
            print 'ERROR: page file %s has duplicate page name "%s"' % (
                file_path, name)
            sys.exit(1)
            pass
        if url in urls:
            print 'ERROR: page file %s has duplicate page URL "%s"' % (
                file_path, url)
            sys.exit(1)
            pass
        names.add(name)
        urls.add(url)
        if numparts == 3:
            load_timeout = int(parts[2])
            pass
        else:
            load_timeout = -1
            pass
        retlist.append((name, url, load_timeout))
        pass
    f.close()

    return retlist

consecutive_chromedriver_start_fail_count = 0

def do_one_page(page_name,
                url,
                chromedriver_server_path,
                chromedriver_server_port,
                drive_chrome_script_path,
                chrome_path, # the browser
                chrome_debug_log_file_path,
                page_load_timeout,
                exp_output_dir,
                ):

    print 'page name [%s], url [%s]' % (page_name, url)
    page_output_dir = exp_output_dir + '/' + page_name

    assert not os.path.exists(page_output_dir)

    os.mkdir(page_output_dir)

    file_devnull = open(os.devnull, 'w')

    print '============================='

    subprocess.call('killall -9 chromedriver chrome google-chrome chromium',
                    shell=True, stdout=file_devnull, stderr=file_devnull)

    dlog('start chromedriver server...')

    assert os.path.isfile(chromedriver_server_path)
    cmd = (chromedriver_server_path,
           '--verbose',
           '--port=%u' % (chromedriver_server_port))
    dlog('popen cmd: [%s]' % (' '.join(cmd)))

    chromedriver_server_output = open(
        page_output_dir + '/chromedriver_output.txt', 'w')
        #os.devnull, 'w')
    chromedriver_server = subprocess.Popen(
        cmd, stdout=chromedriver_server_output, stderr=chromedriver_server_output,
        env={'DISPLAY': ':55',
             'CHROME_DEVEL_SANDBOX': '/usr/local/sbin/chrome-devel-sandbox',
         })

    global consecutive_chromedriver_start_fail_count
    retcode = chromedriver_server.poll()
    if retcode != None:
        f = open(page_output_dir + '/chromedriver_fails_to_start', 'w')
        f.write('status code: %s\n' % (str(retcode)))
        f.close()
        # skip to next load
        consecutive_chromedriver_start_fail_count += 1
        if consecutive_chromedriver_start_fail_count == 4:
            print 'ERROR: chromedriver fails to start %u times in a row. exiting...' % (
                consecutive_chromedriver_start_fail_count)
            sys.exit(1)
            pass
        return
    else:
        # reset
        consecutive_chromedriver_start_fail_count = 0
        pass

    ##
    ## use drive-chrome.py to start load
    ##

    # build up the cmd list
    assert os.path.isfile(drive_chrome_script_path)
    cmd = [drive_chrome_script_path,
           '--chrome-driver-port', str(chromedriver_server_port)]

    # some chrome options
    net_log_file_path = page_output_dir + '/chrome_netlog.txt'
    for o in (
            'incognito',
            #'enable-logging',
            'safebrowsing-disable-download-protection',
            'disable-component-extensions-with-background-pages',
            'disable-default-apps',
            'disable-preconnect',
            #'vmodule=*/web_contents_impl.cc=2,*/resource_dispatcher_host_impl.cc=2',
            'blink-platform-log-channels=ResourceLoading',
            #'v=2',
            'disable-background-networking',
            'disable-client-side-phishing-detection',
            #'log-net-log=%s' % (net_log_file_path),
            #'net-log-level=0',
    ):
        cmd.append('--chrome-option')
        cmd.append(o)
        pass

    # some directory options
    disk_cache_dir_path = './.chromediskcachedir'
    user_data_dir_path = './.chromedatadir'
    for optname, dpath in (
            ('user-data-dir', user_data_dir_path),
            ('disk-cache-dir', disk_cache_dir_path),
    ):
        if os.path.exists(dpath):
            # assume it's a dir
            shutil.rmtree(dpath)
            assert not os.path.exists(dpath)
            pass
        cmd.append('--chrome-option')
        cmd.append('%s=%s' % (optname, dpath))
        pass

    for arg in (
            '--timeout', str(page_load_timeout),
    ):
        cmd.append(arg)
        pass

    if chrome_path:
        assert os.path.exists(chrome_path)
        cmd.extend(['--chrome-path', chrome_path])
        pass

    cmd.append(url)

    # we need to cause webkit's Document destructor to run because
    # that's where we make chrome do the logging. so we ask it to
    # navigate to another page.

    # these two don't seem to cause the Document's destructor to run
    # for the first page
#    cmd.append('about:blank')
#    cmd.append('chrome://newtab/')

    cmd.append('http://web.engr.illinois.edu/~nguyen59/notfound.html')

    dlog('drive chrome cmd: [%s]' % (' '.join(cmd)))

    if chrome_debug_log_file_path is None:
        chrome_debug_log_file_path = user_data_dir_path + '/chrome_debug.log'
        pass

    # maybe use 3.3's subprocess.TimeoutExpired?
    output_file = open(page_output_dir + '/load_output.txt', 'w')
    timedout = False
    chromedriver_client = subprocess.Popen(
        cmd, stdout=output_file, stderr=output_file)
    try:
        wait_for_process_with_timeout(chromedriver_client, page_load_timeout+3)
        pass
    except Timeout:
        timedout = True
        print 'ERROR: page load timed out'
        pass

    code = chromedriver_client.poll()
    if code is None:
        dlog('drive chrome is still running -> kill')
        chromedriver_client.terminate()
        chromedriver_client.kill()
        pass

    output_file.close()

    dlog('stopping chromedriver_server')
    chromedriver_server.terminate()
    sleep(1)
    chromedriver_server.kill()
    assert chromedriver_server.poll() != None

    chromedriver_server_output.close()

    # wait a few seconds before next load

    subprocess.call('killall -9 chromedriver chrome google-chrome chromium',
                    shell=True, stdout=file_devnull, stderr=file_devnull)

    if not timedout:
        dlog('saving some log files')
        for filepath in (
                chrome_debug_log_file_path,
        ):
            # copy instead of move
            shutil.copy(filepath, page_output_dir)
            pass

        files_to_compress = [
            net_log_file_path,
            page_output_dir + '/chrome_debug.log',
            chromedriver_server_output.name,
        ]
        # use Popen to let it happen in background
        subprocess.Popen(
            ['bzip2'] + files_to_compress,
            stdout=file_devnull, stderr=file_devnull)

        pass
    else:
        f = open(page_output_dir + '/pageload_timedout', 'w')
        f.close()
        pass

    return


def run(chromedriver_server_path,
        chromedriver_server_port,
        drive_chrome_script_path,
        chrome_path, # the browser
        chrome_debug_log_file_path,
        page_load_timeout,
        page_file_path,
        output_dir,
        ):

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
        pass
    else:
        print 'ERROR: output dir %s already exists. quitting to prevent overwrite' % (output_dir)
        sys.exit(-1)
        pass

    page_name_to_url_and_timeout = load_page_file(page_file_path)

    summary_file = open(output_dir + '/summary.txt', 'w')
    summary_file.write('command-line args:\n%s\n' % (' '.join(sys.argv)))
    summary_file.write('\nstart: %s\n' % (time.ctime()))
    summary_file.close()

    shutil.copy(page_file_path, output_dir)

    for page_name, url, load_timeout in page_name_to_url_and_timeout:
        print ('===================================')
        if load_timeout == -1:
            load_timeout = page_load_timeout
            pass
        do_one_page(page_name, url,
                    chromedriver_server_path=chromedriver_server_path,
                    chromedriver_server_port=chromedriver_server_port,
                    drive_chrome_script_path=drive_chrome_script_path,
                    chrome_path=chrome_path,
                    chrome_debug_log_file_path=chrome_debug_log_file_path,
                    page_load_timeout=load_timeout,
                    exp_output_dir=output_dir)
        pass
    pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description=''
        )

    parser.add_argument("-v", "--verbose",
                        action="store_true", dest="verbose", default=False,
                        help="be verbose")

    parser.add_argument("--chrome-path",
                        help='path to chrome')
    parser.add_argument("--chrome-debug-log-file-path",
                        help="path to chrome's debug log file")
    parser.add_argument("--chrome-driver-port",
                        dest='chrome_driver_port', default=9515, type=int,
                        help='port to give chrome driver to listen on')
    parser.add_argument("--load-timeout", default=10, type=int,
                        help='timeout in seconds for each page load')

    parser.add_argument("chrome_driver_path",
                        metavar='chrome-driver',
                        help='path to chromedriver server')
    parser.add_argument("drive_chrome_script_path",
                        metavar='drive-chrome-script',
                        help='path to drive-chrome script')
    parser.add_argument("page_file_path",
                        metavar='page-file',
                        help="page containing which pages to load, each line is '<name> <url> [<timeout>]'")
    parser.add_argument("output_dir_path",
                        metavar='output-dir',
                        help='path to directory to save results')

    args = parser.parse_args()

    g_verbose = args.verbose

    run(chromedriver_server_path = args.chrome_driver_path,
        chromedriver_server_port = args.chrome_driver_port,
        drive_chrome_script_path = args.drive_chrome_script_path,
        page_file_path = args.page_file_path,
        page_load_timeout = args.load_timeout,
        chrome_path = args.chrome_path,
        chrome_debug_log_file_path = args.chrome_debug_log_file_path,
        output_dir = args.output_dir_path,
        )
    pass

python run.py \
    --chrome-path /home/me/chromium-src-for-15.04/src/out/Debug/chrome \
    --chrome-debug-log-file-path /home/me/chromium-src-for-15.04/src/out/Debug/chrome_debug.log \
    --chrome-driver-port 9515 \
    --load-timeout 10 \
    --verbose \
    /home/me/chromium-src-for-15.04/src/out/Debug/chromedriver \
    ./drive-chrome.py \
    pages.txt \
    outputdir
